// repository views and handlers

package web

import (
	//"regexp"
	"errors"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"

	"codeberg.org/snaums/mvoCI/core"

	//"golang.org/x/crypto/bcrypt"
	//"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	//"github.com/labstack/echo/middleware"
)

const SSH_CONFIG_FILE string = "$HOME/.ssh/config"

// /keys/add
// View: Add a key to the database
func keysAddHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"integration")
	}

	return ctx.Render(http.StatusOK, "keys_edit", echo.Map{
		"navPage": "integration",
		"user":    user,
		"mode":    "add",
	})
}

func keysModifyHandler(ctx echo.Context, edit bool) error {
	host := ctx.FormValue("key_host")
	hostname := ctx.FormValue("key_hostname")
	username := ctx.FormValue("key_user")
	proxyjmp := ctx.FormValue("key_proxyjump")
	port := ctx.FormValue("key_port")

	key_file := ctx.FormValue("key_file")
	key_input := ctx.FormValue("key_private_key")
	key_upload, upload_err := ctx.FormFile("key_file_upload")

	if host == "" || (key_file == "" && key_input == "" && (key_upload == nil || upload_err != nil)) {
		return errors.New("Host must be filled")
	}

	if strings.ContainsAny(host, "$") || host[0] == '/' {
		return errors.New("Host must not contain $, and may not start with '/'")
	}

	if (key_file != "" && key_input != "") ||
		(key_file != "" && key_upload != nil) ||
		(key_input != "" && key_upload != nil) {
		return errors.New("Only one of the key-fields must be filled")
	}

	var err error
	var portnum = -1
	if port != "" {
		portnum, err = strconv.Atoi(port)
		if err != nil {
			return err
		}
	}

	var identityFile = os.ExpandEnv("$HOME/.ssh/" + host)

	if key_file != "" {
		key_file = strings.TrimSpace(key_file)
		if strings.Contains(key_file, "..") || key_file[0] == '/' {
			return errors.New("Invalid path")
		}
		identityFile = os.ExpandEnv("$HOME/.ssh/" + key_file)
	} else if key_input != "" {
		err = os.WriteFile(os.ExpandEnv(identityFile), []byte(key_input), 0600)
		if err != nil {
			return err
		}
	} else if key_upload != nil {
		if upload_err != nil {
			return upload_err
		}

		src, err := key_upload.Open()
		if err != nil {
			return err
		}
		defer src.Close()

		dest, err := os.Create(os.ExpandEnv(identityFile))
		if err != nil {
			return err
		}
		defer dest.Close()
		err = dest.Chmod(0600)
		if err != nil {
			core.Console.Warn("Unable to set appropriate permission bits for key")
			return err
		}

		if _, err = io.Copy(dest, src); err != nil {
			return err
		}
	} else {
		return errors.New("Exactly one key-field must be filled")
	}

	var k core.SshKey
	core.DB.Where("host = ?", host).First(&k)
	if edit {
		if k.Host == "" {
			return errors.New("The key with host " + host + " does not exists")
		}

		k.Hostname = hostname
		k.User = username
		k.ProxyJump = proxyjmp
		k.Port = portnum
		k.IdentityFile = identityFile

		core.DB.Save(&k)
	} else {
		if k.Host != "" {
			return errors.New("The key with host " + host + " already exists")
		}

		k = core.SshKey{
			Host:         host,
			Hostname:     hostname,
			User:         username,
			ProxyJump:    proxyjmp,
			Port:         portnum,
			IdentityFile: identityFile,
		}

		core.DB.Save(&k)
	}

	err = keysUpdateConfigFile(SSH_CONFIG_FILE)
	if err != nil {
		return err
	}

	return nil
}

func keysEditPostHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"integration")
	}

	err := keysModifyHandler(ctx, true)
	if err != nil {
		var host = ctx.Param("host")
		if host == "" {
			return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"keys?error="+err.Error())
		} else {
			return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"keys/edit/"+host+"?error="+err.Error())
		}
	}

	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"keys?success=Key modified")
}

func keysAddPostHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"integration")
	}

	err := keysModifyHandler(ctx, false)
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"keys/add?error="+err.Error())
	}

	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"keys?success=Key added")
}

func keysDeleteHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"integration")
	}

	var host = ctx.Param("host")
	if host == "" {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"keys?error=SSH key Identifier invalid")
	}

	core.DB.Where("host = ?", host).Delete(&core.SshKey{})
	err := keysUpdateConfigFile(SSH_CONFIG_FILE)
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"keys?error="+err.Error())
	}
	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"keys?success=Delete of key "+host+" successful")
}

func keysUpdateConfigFile(filename string) error {
	var keys []core.SshKey
	core.DB.Order("host").Find(&keys)

	content := core.SshKeysToString(keys)
	_, err := os.Stat(filename + ".old.mvoCI")
	var src, dest *os.File
	if os.IsNotExist(err) {
		src, err = os.Open(filename)
		if err == nil {
			defer src.Close()
			dest, err = os.Create(filename + ".old.mvoCI")
			if err == nil {
				defer dest.Close()
				_, err = io.Copy(dest, src)
			}
		}

		if err != nil {
			core.Console.Warn("Backup copy of SSH-config could not be created: " + err.Error())
		}
	}

	err = os.WriteFile(os.ExpandEnv(filename), []byte(content), 0600)
	if err != nil {
		core.Console.Warn("Unable to write key config file:", err)
		return err
	}

	return nil
}

func keysEditHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"integration")
	}

	var host = ctx.Param("host")
	if host == "" {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"keys")
	}

	var k core.SshKey
	core.DB.Where("host = ?", host).First(&k)

	return ctx.Render(http.StatusOK, "keys_edit", echo.Map{
		"navPage": "integration",
		"user":    user,
		"mode":    "edit",
		"key":     k,
		"getFilename": func(url string) string {
			return strings.ReplaceAll(url, os.ExpandEnv("$HOME/.ssh/"), "")
		},
	})
}

// /keys
// list of SSH Keys
func keysOverviewHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"integration")
	}

	var keys []core.SshKey
	core.DB.Order("host").Find(&keys)

	return ctx.Render(http.StatusOK, "keys", echo.Map{
		"navPage": "integration",
		"user":    user,
		"keys":    keys,
		"getFilename": func(url string) string {
			return strings.ReplaceAll(url, os.ExpandEnv("$HOME/.ssh/"), "")
		},
	})
}
