// handlers for the /api/v1/repo endpoints

package web

import (
	"net/http"

	"errors"
	"strconv"

	"codeberg.org/snaums/mvoCI/build"
	"codeberg.org/snaums/mvoCI/core"

	"github.com/labstack/echo/v4"
)

// API: api call handler for listing and searching over repositories
// @Summary     List/Search accessible repositories
// @Description List all queryable repositories, if name is given it is used as search-word
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} []core.Repository  "List of repositories"
// @Param       name            query   string  false   "Search string for the Repository name"
// @Param       order_by        query   string  false   "Order the repositories by: created_at, updated_at, name, public"
// @Param       length          query   integer false   "Limit the number of returned objects"
// @Param       start_at        query   integer false   "Skip the first n objects"
// @Security    securitydefinitions.apikey
// @Tags        repo, list, search, query
// @Router      /repo/list [get]
func apiRepoListHandler(endpoint string, user core.User, ctx echo.Context) error {
	var allowedOrders = []string{"name", "created_at", "updated_at", "public"}
	name, orderby, limit, startAt := apiListHelper(ctx, allowedOrders)

	core.Console.Warn("Hello WORLD")

	var r []core.Repository
	if user.Superuser {
		if name == "" {
			core.DB.Select("id, name, user_id, clone_url, public").Order(orderby).Offset(startAt).Limit(limit).Find(&r)
		} else {
			core.DB.Select("id, name, user_id, clone_url, public").Where("name LIKE ?", name).Order(orderby).Offset(startAt).Limit(limit).Find(&r)
		}
	} else {
		if name == "" {
			core.DB.Select("id, name, user_id, clone_url, public").Where("user_id = ?", user.ID).Order(orderby).Offset(startAt).Limit(limit).Find(&r)
		} else {
			core.DB.Select("id, name, user_id, clone_url, public").Where("name LIKE ? AND user_id = ?", name, user.ID).Order(orderby).Offset(startAt).Limit(limit).Find(&r)
		}
	}

	ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	return ctx.JSON(http.StatusOK, r)
}

// API: return a repository object by its ID
// @Summary     Query details about a given Repository
// @Description Given a Repository-ID, return a fully populated object of the repository
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} core.Repository  "The requested repository object"
// @Param       id        query   integer true   "The repository-ID to be queried"
// @Failure     404     {string} string "The requested repository was not found or is not accessible"
// @Failure     400     {string} string "The ID parameter is missing"
// @Security    securitydefinitions.apikey
// @Tags        repository, query
// @Router      /repo/detail [get]
func apiRepoDetailHandler(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil || id <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "repo", apiError("The ID is missing"))
	}

	var r core.Repository
	core.DB.Where("id = ?", id).First(&r)
	if r.ID <= 0 {
		return apiReturn(ctx, http.StatusNotFound, "repo", apiError("The repository was not found"))
	}
	acl := core.AclCheck(user, r)
	if acl >= core.AclView {
		return apiReturn(ctx, http.StatusOK, "repo/"+strconv.Itoa(id), r)
	}
	return apiReturn(ctx, http.StatusForbidden, "repo", apiError("Operation not permitted"))
}

// API: Start a build using the API with a given branch and hash
// @Summary     Build a given repository
// @Description Given a Repository-ID, start a build of the repository; returns the created build objects and returns immediately
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} core.Build  "The created build object"
// @Param       id        query   integer true   "The repository-ID to be build"
// @Param       hash      query   string  false  "The commit to be build"
// @Param       branch    query   string  false  "The branch to be build"
// @Failure     404     {string} string "The requested repository was not found or is not accessible"
// @Failure     400     {string} string "The ID parameter is missing"
// @Security    securitydefinitions.apikey
// @Tags        repository, query
// @Router      /repo/build [get]
func apiRepoBuildHandler(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil || id <= 0 {
		return HTTPError(http.StatusBadRequest, ctx)
	}
	var r core.Repository
	core.DB.Where("id = ?", id).First(&r)
	if r.ID > 0 {
		if r.UserID == user.ID || user.Superuser {
			var hash = ctx.FormValue("hash")
			var branch = ctx.FormValue("branch")
			if branch == "" {
				branch = r.DefaultBranch
			}

			b := build.BuildSpecific2(r, branch, hash, "", "", "", "API Build", map[string]string{})
			ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
			var bx core.Build
			core.DB.Where("id = ?", b).First(&bx)
			return ctx.JSON(http.StatusOK, bx)
		} else {
			return HTTPError(http.StatusForbidden, ctx)
		}
	}
	return HTTPError(http.StatusNotFound, ctx)
}

// POST /api/v1/repo/modify/:id
// @Summary     Modify an existing Repository
// @Description Modify properties of a repository
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} core.Repository
// @Param       id               query   int     true  "the ID of the repository to modify"
// @Param       name             query   string  false "name of the new Repository"
// @Param       clone_url        query   string  false "URL of the new repository (where it can be cloned from)"
// @Param       default_branch   query   string  false "Default branch"
// @Param       build_script     query   string  false "Script for building the repository"
// @Param       public_enable    query   bool  false   "Is this repository public? (default: no)"
// @Param       keep_builds      query   bool  false   "Keep build artifacts? (default: no)"
// @Param       webhook_enable   query   bool  false   "Enable webhook builds? (default: no)"
// @Failure     400     {string}    string  Required parameters are missing
// @Failure     404     {string}    string  The Repository was not found
// @Security    securitydefinitions.apikey
// @Tags        repo, modify
// @Router      /repo/modify [post]
func apiRepoModify(endpoint string, user core.User, ctx echo.Context) error {
	err, rid := repoPostChecker(ctx, user, true)
	if err != nil {
		// the edit operation did not work, no trustworthy id in rid
		id, err2 := apiGetID(ctx)
		if err2 == nil {
			var r core.Repository
			core.DB.Select("id").Where("id = ?", strconv.Itoa(id)).First(&r)
			if r.ID > 0 {
				return apiReturn(ctx, http.StatusBadRequest, "repo/edit/"+strconv.Itoa(id), apiError(err.Error()))
			} else {
				return apiReturn(ctx, http.StatusBadRequest, "repo", apiError("No valid ID found"))
			}
		}
	}
	strid := strconv.FormatUint(uint64(rid), 10)
	return apiReturn(ctx, http.StatusFound, "repo/view/"+strid, apiSuccess("repo/modify", "Repo modified correctly "+strid))
}

// POST /api/v1/repo/create
// @Summary     Create a Repository
// @Description This api call creates a repository
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} core.Repository
// @Param       name             query   string  true   "name of the new Repository"
// @Param       clone_url        query   string  true   "URL of the new repository (where it can be cloned from)"
// @Param       default_branch   query   string  true   "Default branch"
// @Param       build_script     query   string  true   "Script for building the repository"
// @Param       public_enable    query   bool  false   "Is this repository public? (default: no)"
// @Param       keep_builds      query   bool  false   "Keep build artifacts? (default: no)"
// @Param       webhook_enable   query   bool  false   "Enable webhook builds? (default: no)"
// @Failure     400     {string}    string  Required parameters are missing
// @Security    securitydefinitions.apikey
// @Tags        repo, create
// @Router      /repo/create [post]
func apiRepoCreate(endpoint string, user core.User, ctx echo.Context) error {
	err, rid := repoPostChecker(ctx, user, false)
	if err != nil {
		return apiReturn(ctx, http.StatusBadRequest, "repo/add", apiError(err.Error()))
	}
	strid := strconv.FormatUint(uint64(rid), 10)
	return apiReturn(ctx, http.StatusFound, "repo/view/"+strid, apiSuccess("repo/create", "Created repository "+strid))
}

// GET /api/v1/repo/delete/:id
// @Summary     Delete a Repository
// @Description This api call deletes a repository
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} core.Repository
// @Param       id   query   int  true   "ID of the repository to delete"
// @Failure     400     {string}    string  Required parameters are missing
// @Security    securitydefinitions.apikey
// @Tags        repo, delete
// @Router      /repo/delete [post]
func apiRepoDelete(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil {
		// todo: error propagation
		return apiReturn(ctx, http.StatusFound, "repo", apiError("No valid ID found"))
	}

	var r core.Repository
	var builds []core.Build
	core.DB.Where("id = ?", id).First(&r)
	if r.ID <= 0 {
		return apiReturn(ctx, http.StatusFound, "repo", apiError("Repository not found"))
	}

	// auth check
	acl := core.AclCheck(user, r)
	if acl < core.AclDelete {
		return apiReturn(ctx, http.StatusFound, "repo", apiError("Permission Error"))
	}

	// delete all builds
	core.DB.Select("id").Where("repository_id = ?", id).Find(&builds)
	for _, b := range builds {
		buildDelete(b.ID)
	}
	// delete the repository
	core.DB.Delete(&r)

	// Todo: delete all acls
	return apiReturn(ctx, http.StatusFound, "repo", apiSuccess("repo/delete", "Deleted repository "+r.Name+" successfully"))
}

// Check user inputs from adding or editing repository information views.
// Then commit them to the database.
func repoPostChecker(ctx echo.Context, user core.User, edit bool) (error, uint) {
	var name string
	var cloneUrl string
	var defaultBranch string
	var buildScript string
	wh := false
	keepBuilds := false
	public := false
	email := false
	nightly := false
	nightlyWhenChanged := false

	var id int
	var err error
	id = -1
	if edit {
		id, err = apiGetID(ctx)
		if err != nil {
			return errors.New("Invalid ID"), 0
		}
	}

	name, err = apiGetParam(ctx, "repo_name")
	if err != nil {
		return err, 0
	}
	cloneUrl, err = apiGetParam(ctx, "repo_clone_url")
	if err != nil {
		return err, 0
	}
	defaultBranch, err = apiGetParam(ctx, "repo_default_branch")
	if err != nil {
		return err, 0
	}
	buildScript, err = apiGetParam(ctx, "repo_push_build_script")
	if err != nil {
		return err, 0
	}

	public, _ = apiGetCheckBox(ctx, "repo_public_enable")
	keepBuilds, _ = apiGetCheckBox(ctx, "repo_keepbuilds_enable")
	wh, _ = apiGetCheckBox(ctx, "repo_webhook_enable")
	email, _ = apiGetCheckBox(ctx, "repo_email_enable")
	nightly, _ = apiGetCheckBox(ctx, "repo_nightly_enable")
	nightlyWhenChanged, _ = apiGetCheckBox(ctx, "repo_nightly_when_changed_enable")

	if len(defaultBranch) <= 0 {
		defaultBranch = "main"
	}

	if len(name) <= 0 || len(cloneUrl) <= 0 {
		return errors.New("Did you input anything?"), 0
	}

	var secret string
	var r core.Repository
	if !edit {
		var x int64 = 1
		for x > 0 {
			secret = authSecretGenerator()
			core.DB.Model(&core.Repository{}).Where("secret = ?", secret).Count(&x)
		}

		r = core.Repository{
			Name:               name,
			CloneUrl:           cloneUrl,
			DefaultBranch:      defaultBranch,
			Secret:             secret,
			BuildScript:        buildScript,
			WebHookEnable:      wh,
			KeepBuilds:         keepBuilds,
			Public:             public,
			SendMail:           email,
			Nightly:            nightly,
			NightlyWhenChanged: nightlyWhenChanged,
			UserID:             user.ID,
		}

		core.DB.Create(&r)
		if r.ID <= 0 {
			return errors.New("Could not create the repository"), 0
		}
	} else {
		secret, err = apiGetParam(ctx, "repo_secret")
		if err != nil {
			return err, 0
		}
		core.DB.Where("id = ?", id).First(&r)
		// auth check
		acl := core.AclCheck(user, r)
		if acl < core.AclEdit {
			return errors.New("Can't view that repository"), 0
		}

		if r.ID <= 0 {
			return errors.New("Invalid ID"), 0
		}

		r.WebHookEnable = wh
		r.Name = name
		r.CloneUrl = cloneUrl
		r.DefaultBranch = defaultBranch
		r.Secret = secret
		r.BuildScript = buildScript
		r.KeepBuilds = keepBuilds
		r.Public = public
		r.SendMail = email
		r.Nightly = nightly
		r.NightlyWhenChanged = nightlyWhenChanged

		err = core.DB.Save(&r).Error
		if err != nil {
			return err, r.ID
		}
	}
	return nil, r.ID
}
