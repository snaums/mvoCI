// helper functions for the web-part of the authentication procedure

package web

import (
	"crypto/rand"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"codeberg.org/snaums/mvoCI/core"
	"github.com/labstack/echo/v4"
	// "golang.org/x/crypto/bcrypt"
)

// name of the cookie to be set at the users end
const sessionCookie string = "mvoCI_session"

// test the crupto source at start-up
func init() {
	// Assert that a cryptographically secure PRNG is available.
	// Panic otherwise.
	buf := make([]byte, 1)

	_, err := io.ReadFull(rand.Reader, buf)
	if err != nil {
		panic(fmt.Sprintf("crypto/rand is unavailable: Read() failed with %#v", err))
	}
}

// remove a cookie from the end users browser
func RemoveCookie(ctx echo.Context, name string) {
	if CookieExists(ctx, name) {
		WriteCookie(ctx, name, "", time.Unix(0, 0))
	}
}

// set a cookie at the end users browser
func WriteCookie(ctx echo.Context, name string, value string, expires time.Time) {
	var cookie *http.Cookie
	if CookieExists(ctx, name) {
		cookie = CollectCookie(ctx, name)
	} else {
		cookie = new(http.Cookie)
		cookie.Name = name
	}
	cookie.Value = value
	cookie.Expires = expires
	ctx.SetCookie(cookie)
}

// return whether a cookie exists
func CookieExists(ctx echo.Context, name string) bool {
	cookie, err := ctx.Cookie(name)
	if err != nil || cookie == nil {
		return false
	}
	return true
}

// return the http.Cookie for a name or nil if it doesn't exist
func CollectCookie(ctx echo.Context, name string) *http.Cookie {
	cookie, err := ctx.Cookie(name)
	if err != nil || cookie == nil {
		return nil
	}

	return cookie
}

// return the content of a cookie
func ReadCookie(ctx echo.Context, name string) (string, error) {
	cookie, err := ctx.Cookie(name)
	if err != nil || cookie == nil {
		return "", errors.New("No Cookie found")
	}

	return cookie.Value, nil
}

// check that the user is fully logged in using her cookie
func UserFromSession(u *core.User, ctx echo.Context) bool {
	return userFromSession(u, false, false, ctx)
}

// check that the user token is incomplete and exists
func UserFromIncompleteSession(u *core.User, ctx echo.Context) bool {
	return userFromSession(u, false, true, ctx)
}

// check that the API token exists
func ApiTokenFromSession(u *core.User, ctx echo.Context) bool {
	return userFromSession(u, true, false, ctx)
}

// infer the next login step from the loginToken
func loginStepFromToken(ctx echo.Context) (string, string) {
	hash, err := ReadCookie(ctx, sessionCookie)
	if err != nil {
		return "", ""
	}

	var count int64
	l := core.LoginToken{}
	core.DB.Model(&core.LoginToken{}).Where("secret = ? AND expires_at > ?", hash, time.Now()).Count(&count).First(&l)

	if hash != "" && count == 1 {
		return l.Step, l.StepExtra
	}

	return "", ""
}

// check if a user is logged in given the cookie, according to the api and whether its incomplete settings; returns true and the user-object
func userFromSession(u *core.User, api bool, incomplete bool, ctx echo.Context) bool {
	hash, err := ReadCookie(ctx, sessionCookie)
	if err != nil {
		*u = core.User{}
		return false
	}

	if hash == "" {
		*u = core.User{}
		return false
	}
	var count int64
	l := core.LoginToken{}
	if api {
		// users _and_ API-tokens can call the API
		core.DB.Preload("User").Model(&core.LoginToken{}).Where("secret = ? AND expires_at > ?", hash, time.Now()).Count(&count).First(&l)
	} else {
		if incomplete {
			core.DB.Preload("User").Model(&core.LoginToken{}).Where("secret = ? AND expires_at > ? AND type = 'login'", hash, time.Now()).Count(&count).First(&l)
		} else {
			core.DB.Preload("User").Model(&core.LoginToken{}).Where("secret = ? AND expires_at > ? AND type = 'login' AND step = ?", hash, time.Now(), "fin").Count(&count).First(&l)
		}
	}
	if count == 1 {
		*u = l.User
		return true
	}
	*u = core.User{}
	return false
}

// generate a secret
func authSecretGenerator() string {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return "AN ERROR OCCURED, PLEASE REPORT!"
	}
	hs := hex.EncodeToString(b)
	result := hs[0:4] + "-" + hs[4:8] + "-" + hs[8:12] + "-" + hs[12:16] + "-" + hs[16:20] + "-" + hs[20:24] + "-" + hs[24:28] + "-" + hs[28:32]
	return result
}

// try to generate a collision free login token
func GenerateLoginToken(try int) string {
	r, err := GenerateRandomString(64)
	if err != nil {
		return ""
	}
	var cnt int64
	core.DB.Model(&core.LoginToken{}).Where("secret = ?", r).Count(&cnt)
	if cnt > 0 {
		if try == 0 {
			return ""
		}
		return GenerateLoginToken(try - 1)
	}
	return r
}

// update the loginToken to the nextStep, set its extra, generate a new tokenstring and write it to the user
func ChangeLoginToken(ctx echo.Context, u core.User, nextStep string, stepExtra string) bool {
	hash, err := ReadCookie(ctx, sessionCookie)
	if err != nil {
		return false
	}
	if hash == "" {
		return false
	}

	var l core.LoginToken
	var count int64
	core.DB.Model(&core.LoginToken{}).Where("secret = ? AND expires_at > ? AND type = 'login'", hash, time.Now()).Count(&count).First(&l)
	if count != 1 {
		return false
	}

	if l.UserID != u.ID {
		return false
	}

	secret := GenerateLoginToken(10)
	if secret == "" {
		return false
	}
	l.Secret = secret
	if nextStep == "fin" {
		l.ExpiresAt = time.Now().Add(24 * time.Hour)
	} else {
		l.ExpiresAt = time.Now().Add(20 * time.Minute)
	}
	l.User = u
	l.Step = nextStep
	l.StepExtra = stepExtra

	core.DB.Save(&l)
	WriteCookie(ctx, sessionCookie, l.Secret, l.ExpiresAt)
	return true
}

// create a new login token for a given user with nextstep and auth extra
func NewLoginToken(l *core.LoginToken, ctx echo.Context, u core.User, nextStep string, stepExtra string) bool {
	secret := GenerateLoginToken(10)
	if secret == "" {
		return false
	}
	l.Secret = secret
	l.Name = "Login"
	l.Type = "login"
	if nextStep == "fin" {
		l.ExpiresAt = time.Now().Add(24 * time.Hour)
	} else {
		l.ExpiresAt = time.Now().Add(20 * time.Minute)
	}
	l.User = u
	l.Step = nextStep
	l.StepExtra = stepExtra

	core.DB.Create(&l)
	WriteCookie(ctx, sessionCookie, l.Secret, l.ExpiresAt)
	return true
}

// generate a new api token from the users settings page
func NewAPIToken(l *core.LoginToken, ctx echo.Context, u core.User, name string) bool {
	secret := GenerateLoginToken(10)
	if secret == "" {
		return false
	}
	l.Secret = secret
	l.Name = name
	l.Type = "api"
	l.ExpiresAt = time.Now().Add(24 * 356 * time.Hour)
	l.User = u

	core.DB.Create(&l)
	return true
}

// remove a login token
func RemoveLoginToken(ctx echo.Context) {
	hash, err := ReadCookie(ctx, sessionCookie)
	if err != nil {
		core.DB.Where("secret = ?", hash).Delete(&core.LoginToken{})
	}
	RemoveCookie(ctx, sessionCookie)
}

// GenerateRandomBytes returns securely generated random bytes.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

// GenerateRandomString returns a securely generated random string.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomString(n int) (string, error) {
	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-"
	bytes, err := GenerateRandomBytes(n)
	if err != nil {
		return "", err
	}
	for i, b := range bytes {
		bytes[i] = letters[b%byte(len(letters))]
	}
	return string(bytes), nil
}
