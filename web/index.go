// dashboard, index, login

package web

import (
	"errors"
	"net/http"
	"time"

	"codeberg.org/snaums/mvoCI/auth"
	"codeberg.org/snaums/mvoCI/core"

	//"golang.org/x/crypto/bcrypt"
	//"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	//"github.com/labstack/echo/middleware"
)

// /dashboard
// renders the dashboard with all current builds listed
func dashboardHandler(ctx echo.Context, user core.User) error {
	var b []core.Build
	var bq []core.Build
	var repo_ids []uint

	mine := ctx.QueryParam("mine")
	queryRecent := core.DB.Select("builds.id", "finished_at", "started_at", "status", "commit_sha", "branch", "repository_id", "commit_message", "zip", "num_total", "num_success", "num_failed", "duration").Joins("Repository")
	queryQueue := core.DB.Select("builds.id", "finished_at", "started_at", "status", "commit_sha", "branch", "repository_id", "commit_message", "zip").Joins("Repository")
	if mine != "" {
		// recent builds
		queryRecent.Where("repository.user_id = ? AND parent_id = 0", user.ID)
		// build queue
		queryQueue.Where("(status='started' OR status='enqueued') AND repository.user_id = ?", user.ID)
	} else {
		if user.Superuser {
			// no where clause needed on recent builds
			// build queue
			queryQueue.Where("(status='started' OR status='enqueued')")
		} else {
			repo_ids = accessibleRepoIds(user)
			// recent builds
			queryRecent.Where("(repository_id IN (?) OR repository.user_id = ?) AND parent_id = 0", repo_ids, user.ID)
			// build queue
			queryQueue.Where("(status='started' OR status='enqueued') AND (repository_id IN (?) OR repository.user_id = ?)", repo_ids, user.ID)
		}
	}

	queryRecent.Order("builds.created_at desc").Limit(10).Find(&b)
	queryQueue.Order("status desc, builds.created_at asc").Limit(10).Find(&bq)

	return ctx.Render(http.StatusOK, "dashboard", echo.Map{
		"builds":                 b,
		"navPage":                "dashboard",
		"user":                   user,
		"queue":                  bq,
		"create_linear_gradient": repoTestCreateGradient,
	})
}

// redirects either to the dashboard, login-form or installation pages if enabled
// /
func indexHandler(ctx echo.Context) error {
	if core.Cfg.Install {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"install/1")
	}
	u := core.User{}
	if UserFromSession(&u, ctx) {
		return dashboardHandler(ctx, u)
	}
	if core.Cfg.LandingEnable {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"landing")
	}
	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"login")
}

// destroys the loginToken of the current users, i.e. logging her out
// /logout
func doLogoutHandler(ctx echo.Context) error {
	RemoveLoginToken(ctx)
	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"login")
}

// check the Credentials of a user and create a LoginToken for her if they are
// correct.
func doLoginHandler(ctx echo.Context) error {
	var cnt int64
	var err error
	var u core.User
	var l core.LoginToken
	var prov core.AuthProvider
	var pr core.AuthProvider

	step, stepExtra := loginStepFromToken(ctx)

	extra := ctx.FormValue("auth_extra")
	given := ctx.FormValue("auth_secret")
	switch step {
	case "fin":
		// this should not happen
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"login")
	case "fail":
		core.DB.Model(&core.User{}).Where("name = ?", extra).First(&u)
		if u.ID > 0 {
			u.FailedLoginAttempt()
		}
		RemoveLoginToken(ctx)
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"login")
	case "":
		// Verify Main
		core.DB.Model(&core.User{}).Where("name = ?", extra).Count(&cnt).First(&u)
		u.StepLoginAttempts()

		// TODO native should be some sort of default
		core.DB.Where("user_id = ? AND type = ?", u.ID, "native").Select("extra").First(&pr)
		err = auth.VerifyMain(u, given, extra, pr.Extra)
		NextStep := auth.FollowUp(u, step)
		core.DB.Where("user_id = ? AND type = ?", u.ID, NextStep).Select("extra").First(&prov)
		NextExtra := auth.SeedStep(NextStep, prov.Extra)

		if u.BannedUntil.After(time.Now()) {
			return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"login?error=Incorrect login")
		}
		if err == nil {
			// we check the count of users with this name here to mitigate timing attacks
			if cnt == 1 {
				if NextStep == "fin" {
					u.SuccessfullLogin()
				}

				if NewLoginToken(&l, ctx, u, NextStep, NextExtra) {
					return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"login")
				}
			}
			err = errors.New("Invalid Auth Operation")
		}

		if u.FailedLoginAttempt() {
			RemoveLoginToken(ctx)
		}
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"login?error="+err.Error())
	default:
		if !UserFromIncompleteSession(&u, ctx) {
			RemoveLoginToken(ctx)
			return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"login")
		}

		u.StepLoginAttempts()

		core.DB.Where("user_id = ? AND type = ?", u.ID, step).Select("extra").First(&pr)
		err = auth.VerifyExtra(u, step, stepExtra, given, extra, pr.Extra)

		NextStep := auth.FollowUp(u, step)
		core.DB.Where("user_id = ? AND type = ?", u.ID, NextStep).Select("extra").First(&prov)
		NextExtra := auth.SeedStep(NextStep, prov.Extra)

		if u.BannedUntil.After(time.Now()) {
			RemoveLoginToken(ctx)
			return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"login?error=Incorrect login")
		}

		if err == nil {
			if u.Name != "" {
				if NextStep == "fin" {
					u.SuccessfullLogin()
				}
				if ChangeLoginToken(ctx, u, NextStep, NextExtra) {
					return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"login")
				}
			}
			err = errors.New("Invalid Auth Operation")
		}

		if u.FailedLoginAttempt() {
			RemoveLoginToken(ctx)
		}
		if u.Name != "" {
			core.DB.Where("user_id = ? AND type = ?", u.ID, step).Select("extra").First(&prov)
			newSeed := auth.SeedStep(step, prov.Extra)
			if ChangeLoginToken(ctx, u, step, newSeed) {
				err = errors.New("Invalid Auth Operation")
			}
		}
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"login?error="+err.Error())
	}
}

// redirects to the login form
func loginHandler(ctx echo.Context) error {
	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"login")
}

// Renders first step login form
// /login
func loginHandler_(ctx echo.Context) error {
	if core.Cfg.Install {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"install/1")
	}
	u := core.User{}
	if UserFromSession(&u, ctx) {
		return dashboardHandler(ctx, u)
	}

	step, stepExtra := loginStepFromToken(ctx)
	view := auth.LoginView(step)
	if step == "fail" || view == "" {
		RemoveLoginToken(ctx)
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"login")
	} else if step == "fin" {
		// this should not occur, it should get redirected to the dashboard handler
		return HTTPError(http.StatusInternalServerError, ctx)
	}

	return ctx.Render(http.StatusOK, view, echo.Map{
		"navPage":   "login",
		"authExtra": stepExtra,
	})
}

// /impress
func impressHandler(ctx echo.Context) error {
	u := core.User{}
	var user *core.User
	if UserFromSession(&u, ctx) {
		user = &u
	}
	return ctx.Render(http.StatusOK, "impress", echo.Map{
		"user":    user,
		"navPage": "",
	})
}

// /landing
func landingHandler(ctx echo.Context) error {
	u := core.User{}
	var user *core.User
	if UserFromSession(&u, ctx) {
		user = &u
	}
	return ctx.Render(http.StatusOK, "landing", echo.Map{
		"user":    user,
		"navPage": "",
	})
}

// /integration
func integrationHandler(ctx echo.Context) error {
	u := core.User{}
	if !UserFromSession(&u, ctx) {
		return loginHandler(ctx)
	}
	var webhookURL = ctx.Scheme() + "://" + ctx.Request().Host + core.Cfg.HttpSubUrl + "push/hook"
	return ctx.Render(http.StatusOK, "integration", echo.Map{
		"user":       u,
		"navPage":    "integration",
		"webhookURL": webhookURL,
	})
}
