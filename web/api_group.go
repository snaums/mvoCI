// API handlers for group management

package web

import (
	"net/http"
	"strconv"

	"codeberg.org/snaums/mvoCI/core"

	"github.com/labstack/echo/v4"
)

// API: add a user to a group (you are member of or you are superuser)
// @Summary     Add a user to a group
// @Description Add a user to a group, given the UserID and the name of the group or create the group if it doesn't exist. The group name Everyone is special. This group contains all users and cannot be modified.
// @Accept      mpfd
// @Produce     json
// @Success     200 {string} string "Group created or user added to group"
// @Param       id        query   integer true   "The user-ID to be added"
// @Param       name      query   string  true   "The name of the group"
// @Failure     404     {string} string "The requested user was not found"
// @Failure     400     {string} string "The user-ID or name parameter is missing"
// @Failure     500     {string} string "The group cannot be created/modified"
// @Security    securitydefinitions.apikey
// @Tags        group, add, create
// @Router      /group/add [post]
func apiGroupAddMember(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)

	if err != nil || id <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "user", apiError("ID not present"))
	}

	var redirectUrl = "user/edit/" + strconv.Itoa(int(id))
	var u core.User
	core.DB.Where("id=?", id).First(&u)
	if u.ID != uint(id) {
		return apiReturn(ctx, http.StatusNotFound, redirectUrl, apiError("User not Found"))
	}

	name, err := apiGetParam(ctx, "name")
	if err != nil || name == "" {
		return apiReturn(ctx, http.StatusBadRequest, redirectUrl, apiError("Invalid Name"))
	}
	if name == "Everyone" {
		return apiReturn(ctx, http.StatusBadRequest, redirectUrl, apiError("The group Everyone cannot be modified"))
	}

	var grp core.Group
	var users []uint
	core.DB.Where("name = ?", name).First(&grp)
	if grp.ID > 0 {
		core.DB.Table("user_groups").Select("user_id").Where("group_id=?", grp.ID).Find(&users)
		var amIMember = false
		for _, v := range users {
			if v == u.ID {
				return apiReturn(ctx, http.StatusBadRequest, redirectUrl, apiError("The user is already a member of that group"))
			}
			if v == user.ID {
				amIMember = true
			}
		}

		// I can only add members to groups I am a member of
		if !user.Superuser && !amIMember {
			return apiReturn(ctx, http.StatusBadRequest, redirectUrl, apiError("You can only add members to group, you are a member of"))
		}

		grp.User = append(grp.User, u)
		core.DB.Save(&grp)
		if grp.ID > 0 {
			return apiReturn(ctx, http.StatusOK, redirectUrl, apiSuccess("group/add", "Added "+u.Name+" to group "+name))
		}
	} else {
		if user.ID != u.ID && !user.Superuser {
			return apiReturn(ctx, http.StatusForbidden, "user", apiError("Permission Error"))
		}

		grp = core.Group{}
		grp.Name = name
		grp.User = []core.User{u}
		core.DB.Save(&grp)
		if grp.ID > 0 {
			return apiReturn(ctx, http.StatusOK, redirectUrl, apiSuccess("group/add", "Created new group "+name))
		}
	}

	return apiReturn(ctx, http.StatusInternalServerError, redirectUrl, apiError("Cannot store new group"))
}

// API: create a new user group and add yourself to the group
// @Summary     Create a new group and add yourself
// @Description Create a new group and add yourself as first member. The group name Everyone is special. This group contains all users and cannot be modified.
// @Accept      mpfd
// @Produce     json
// @Success     200 {string} string "Group created"
// @Param       name      query   string  true   "The name of the new group"
// @Failure     400     {string} string "The name parameter is missing"
// @Failure     404     {string} string "The requested user was not found"
// @Failure     409     {string} string "A group with the same name already exists"
// @Failure     500     {string} string "The group cannot be created/modified"
// @Security    securitydefinitions.apikey
// @Tags        group, create
// @Router      /group/create [post]
func apiGroupCreate(endpoint string, user core.User, ctx echo.Context) error {
	var redirectUrl = "user"
	name, err := apiGetParam(ctx, "name")
	if err != nil || name == "" {
		return apiReturn(ctx, http.StatusBadRequest, redirectUrl, apiError("Invalid Name"))
	}
	if name == "Everyone" {
		return apiReturn(ctx, http.StatusBadRequest, redirectUrl, apiError("The group Everyone cannot be modified"))
	}

	var grp core.Group
	core.DB.Where("name = ?", name).First(&grp)
	if grp.ID > 0 {
		return apiReturn(ctx, http.StatusConflict, redirectUrl, apiError("The group already exists"))
	}

	grp = core.Group{}
	grp.Name = name
	grp.User = []core.User{user}
	core.DB.Save(&grp)
	if grp.ID > 0 {
		return apiReturn(ctx, http.StatusOK, redirectUrl, apiSuccess("group/created", "Created new group "+name))
	}

	return apiReturn(ctx, http.StatusInternalServerError, redirectUrl, apiError("Cannot store new group"))
}

// API: list members of a group
// @Summary     List members of a group
// @Description List members of a group. Everyone can only be listed by Superusers. You can only access groups you are in, if you are not a Superuser.
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} []apiUser "List of users"
// @Param       name      query   string  true   "The name of the group"
// @Failure     400     {string} string "The name parameter is missing"
// @Failure     403     {string} string "You tried to access Everyone; only Superusers can do so"
// @Failure     403     {string} string "You are not in the group; only groupmembers can list other members"
// @Failure     404     {string} string "The requested group was not found"
// @Security    securitydefinitions.apikey
// @Tags        group, list
// @Router      /group/members [post]
func apiGroupMembers(endpoint string, user core.User, ctx echo.Context) error {
	var grp core.Group
	id, err := apiGetID(ctx)

	if err != nil || id <= 0 {
		// by name
		name, err := apiGetParam(ctx, "name")
		if err != nil || name == "" {
			return apiReturn(ctx, http.StatusBadRequest, "user", apiError("Invalid Name"))
		}
		if name == "Everyone" && !user.Superuser {
			return apiReturn(ctx, http.StatusForbidden, "user", apiError("Only superusers can query 'Everyone'"))
		}

		core.DB.Model(&core.Group{}).Preload("User").Where("name = ?", name).First(&grp)
	} else {
		// by ID
		core.DB.Model(&core.Group{}).Preload("User").Where("id = ?", id).First(&grp)
		if grp.Name == "Everyone" && !user.Superuser {
			return apiReturn(ctx, http.StatusForbidden, "user", apiError("Only superusers can query 'Everyone'"))
		}
	}

	if grp.ID > 0 {
		users := grp.User
		var amIMember = false
		for _, v := range users {
			if v.ID == user.ID {
				amIMember = true
				break
			}
		}

		// must be member or superuser to list members of groups
		if !user.Superuser && !amIMember {
			return apiReturn(ctx, http.StatusForbidden, "user", apiError("Operation not permitted"))
		}

		apiUsers := apiUserCreateList(users)
		return apiReturn(ctx, http.StatusOK, "user", apiUsers)
	} else {
		return apiReturn(ctx, http.StatusNotFound, "user", apiError("Group not found"))
	}
}

// group object for use in API calls
type apiGroup struct {
	ID   uint   // the ID of the group
	Name string // the name of the group
}

func apiGroupResult(grps []core.Group) []apiGroup {
	var result []apiGroup
	for _, v := range grps {
		result = append(result, apiGroup{
			ID:   v.ID,
			Name: v.Name,
		})
	}
	return result
}

// API: query information about a group
// @Summary     Query name or ID of a group
// @Description Query name or ID of a group. You can only query groups you are member of, if you are no superuser.
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} apiGroup "Group-Object"
// @Param       name      query   string  false   "The name of the group (one parameter must be present)"
// @Param       id        query   string  false   "The ID of the group (one parameter must be present)"
// @Failure     400     {string} string "The name or ID parameter is missing"
// @Failure     404     {string} string "You are not in the group; only groupmembers or superusers can query a group, or the group does not exist"
// @Security    securitydefinitions.apikey
// @Tags        group, query
// @Router      /group [get]
func apiGroupQuery(endpoint string, user core.User, ctx echo.Context) error {
	var grp core.Group
	id, err := apiGetID(ctx)
	if id <= 0 || err != nil {
		// by name
		name, err := apiGetParam(ctx, "name")
		if name == "" || err != nil {
			return apiReturn(ctx, http.StatusBadGateway, "user", apiError("Name and ID parameters missing"))
		}

		core.DB.Preload("User").Where("name = ?", name).First(&grp)
	} else {
		// by ID
		core.DB.Preload("User").Where("id = ?", id).First(&grp)
	}

	if grp.ID > 0 && (user.Superuser || grp.AmIMember(user)) {
		result := apiGroupResult([]core.Group{grp})
		return apiReturn(ctx, http.StatusOK, "user", result)
	}

	return apiReturn(ctx, http.StatusNotFound, "user", apiError("The group was not found or the operation was not permitted"))

}

// API: returns a list of groups based on search criteria
// @Summary     List/Search groups
// @Description returns a list of groups based on search criteria and orders
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} []apiGroup "list of groups"
// @Param       name            query   string  false   "Search string into group name"
// @Param       order_by        query   string  false   "Order the user objects by: created_at, Name"
// @Param       length          query   integer false   "Limit the number of returned objects"
// @Param       start_at        query   integer false   "Skip the first n objects"
// @Security    securitydefinitions.apikey
// @Tags        group, list
// @Router      /group/list [get]
func apiGroupList(endpoint string, user core.User, ctx echo.Context) error {
	var allowedOrders = []string{"created_at", "name", "email"}
	name, orderby, limit, startAt := apiListHelper(ctx, allowedOrders)
	var groups []core.Group
	if name == "" {
		if user.Superuser {
			// TODO add filter for private users only a superuser can see
			core.DB.Select("name", "id").Order(orderby).Offset(startAt).Limit(limit).Find(&groups)

		} else {
			core.DB.Joins("user", core.DB.Where("user_id = ?", user.ID)).Select("name", "groups.id").Order(orderby).Offset(startAt).Limit(limit).Find(&groups)
		}
	} else {
		if user.Superuser {
			// TODO add filter for private users only a superuser can see
			core.DB.Select("name", "id").Where("name LIKE ?", name).Order(orderby).Offset(startAt).Limit(limit).Find(&groups)

		} else {
			core.DB.Joins("user", core.DB.Where("user_id = ?", user.ID)).Select("name", "groups.id").Where("name LIKE ?", name).Order(orderby).Offset(startAt).Limit(limit).Find(&groups)
		}
	}

	result := apiGroupResult(groups)
	return apiReturn(ctx, http.StatusOK, "", result)
}

// API: list members of a group
// @Summary     List members of a group
// @Description List members of a group. Everyone can only be listed by Superusers. You can only access groups you are in, if you are not a Superuser.
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} []apiUser "List of users"
// @Param       name      query   string  true   "The name of the group"
// @Failure     400     {string} string "The name parameter is missing"
// @Failure     403     {string} string "You tried to access Everyone; only Superusers can do so"
// @Failure     403     {string} string "You are not in the group; only groupmembers can list other members"
// @Failure     404     {string} string "The requested group was not found"
// @Security    securitydefinitions.apikey
// @Tags        group, list
// @Router      /group/members [post]
func apiGroupLeave(endpoint string, user core.User, ctx echo.Context) error {
	var grp core.Group
	id, err := apiGetID(ctx)

	if err != nil || id <= 0 {
		// by name
		name, err := apiGetParam(ctx, "name")
		if err != nil || name == "" {
			return apiReturn(ctx, http.StatusBadRequest, "user", apiError("Invalid Name"))
		}
		if name == "Everyone" && !user.Superuser {
			return apiReturn(ctx, http.StatusForbidden, "user", apiError("Only superusers can query 'Everyone'"))
		}

		core.DB.Model(&core.Group{}).Preload("User").Where("name = ?", name).First(&grp)
	} else {
		// by ID
		core.DB.Model(&core.Group{}).Preload("User").Where("id = ?", id).First(&grp)
		if grp.Name == "Everyone" && !user.Superuser {
			return apiReturn(ctx, http.StatusForbidden, "user", apiError("Only superusers can query 'Everyone'"))
		}
	}

	if grp.ID > 0 {
		// must be member or superuser to list members of groups
		if !user.Superuser && !grp.AmIMember(user) {
			return apiReturn(ctx, http.StatusForbidden, "user", apiError("Operation not permitted"))
		}

		apiUsers := apiUserCreateList(grp.User)
		return apiReturn(ctx, http.StatusOK, "user", apiUsers)
	} else {
		return apiReturn(ctx, http.StatusNotFound, "user", apiError("Group not found"))
	}
}
