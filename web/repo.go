// repository views and handlers
package web

import (
	"fmt"
	"strconv"
	"strings"

	"net/http"

	"codeberg.org/snaums/mvoCI/build"
	"codeberg.org/snaums/mvoCI/core"

	"github.com/labstack/echo/v4"
)

func aclString(id int) string {
	return core.AclToString(id)
}

// type of repo and its state for the view render functions
type repo_state struct {
	R     core.Repository // repo
	State string          // its last build state
}

func accessibleRepoIds(user core.User) []uint {
	var r []uint
	core.DB.Table("acls").Select("repository_id").Where("access_level > 0 AND (user_id = ? OR group_id IN (?))", user.ID,
		core.DB.Table("user_groups").Select("group_id").Where("user_id = ?", user.ID)).Find(&r)
	return r
}

// /repo
// list of repositories
func repoHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}
	var r []core.Repository
	var mine = ctx.QueryParam("mine")
	if mine == "" {
		if user.Superuser {
			core.DB.Order("name asc").Find(&r)
		} else {
			ids := accessibleRepoIds(user)
			core.DB.Where("user_id = ? OR id IN (?)", user.ID, ids).Order("name asc").Find(&r)
		}
	} else {
		core.DB.Where("user_id = ?", user.ID).Order("name asc").Find(&r)
	}

	var rx []repo_state
	for _, v := range r {
		var bx core.Build
		core.DB.Where("repository_id = ?", v.ID).Order("started_at DESC").Limit(1).First(&bx)
		rx = append(rx, repo_state{R: v, State: strings.ToLower(bx.Status)})
	}

	return ctx.Render(http.StatusOK, "repo", echo.Map{
		"navPage":      "repo",
		"user":         user,
		"repositories": rx,
		"u":            nil,
	})
}

// /repo/state/:id
// return a corresponding image for README files of projects
// only when the repo is public
const STATE_PASSED string = "static/img/build_passed.png"
const STATE_MISSED string = "static/img/build_miss.png"
const STATE_FAILED string = "static/img/build_failed.png"

func repoStateHandler(ctx echo.Context) error {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err == nil {
		user := core.User{}
		if !UserFromSession(&user, ctx) {
			return ctx.File(repoStateID(nil, id))
		}
		return ctx.File(repoStateID(&user, id))
	}
	return ctx.File(STATE_MISSED)
}

func repoStateID(user *core.User, id int) string {
	var r core.Repository
	core.DB.Select("id,public,user_id").Where("id = ?", id).First(&r)
	if r.ID != 0 {
		return repoState(user, r)
	} else {
		return STATE_MISSED
	}
}

func repoState(user *core.User, r core.Repository) string {
	var acl = core.AclForbidden
	if user != nil {
		acl = core.AclCheck(*user, r)
	}

	if r.Public || acl >= core.AclView {
		var b core.Build
		core.DB.Select("id, status").Where("repository_id = ? AND status IN ('finished', 'failed')", r.ID).Order("finished_at DESC").Limit(1).First(&b)
		if b.ID > 0 {
			switch b.Status {
			case "finished":
				return STATE_PASSED
			case "failed":
				return STATE_FAILED
			}
		}
	}

	return STATE_MISSED
}

// /repo/clone/:id
func repoCloneHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}
	id, _ := strconv.Atoi(ctx.Param("id"))
	var r core.Repository
	core.DB.Where("id = ?", strconv.Itoa(id)).First(&r)
	if r.ID == 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo")
	}

	acl := core.AclCheck(user, r)
	if acl < core.AclExecute {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(id))
	}

	return ctx.Render(http.StatusOK, "repo_edit", echo.Map{
		"mode":    "clone",
		"navPage": "repo",
		"user":    user,
		"repo":    r,
		"errors":  []string{},
		"success": []string{},
	})
}

// /repo/edit/:id
func repoEditHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}
	id, _ := strconv.Atoi(ctx.Param("id"))
	var r core.Repository
	core.DB.Where("id = ?", strconv.Itoa(id)).First(&r)
	if r.ID <= 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo")
	}

	acl := core.AclCheck(user, r)
	if acl < core.AclEdit {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(id))
	}

	return ctx.Render(http.StatusOK, "repo_edit", echo.Map{
		"mode":    "edit",
		"navPage": "repo",
		"user":    user,
		"repo":    r,
		"errors":  []string{},
		"success": []string{},
		"acl":     acl,
	})
}

// /repo/build/:id
// start a simple build for the repo
func repoBuildHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	var r core.Repository
	id, _ := strconv.Atoi(ctx.Param("id"))
	core.DB.Where("id = ?", id).First(&r)
	if r.ID > 0 {
		acl := core.AclCheck(user, r)
		if acl < core.AclExecute {
			return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(id)+"?error=Permission Error")
		}

		b := build.BuildNow(r)
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"build/"+strconv.Itoa(int(b)))
	}
	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(id))
}

// /repo/add
func repoAddHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	return ctx.Render(http.StatusOK, "repo_edit", echo.Map{
		"mode":    "add",
		"navPage": "repo",
		"user":    user,
		"repo":    core.Repository{},
		"errors":  []string{},
		"success": []string{},
	})
}

func repoMembersHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	id, _ := strconv.Atoi(ctx.Param("id"))
	var r core.Repository
	core.DB.Where("id = ?", strconv.Itoa(id)).First(&r)
	if r.ID <= 0 {
		return HTTPError(404, ctx)
	}

	acl := core.AclCheck(user, r)
	if acl < core.AclView {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=Permission Error")
	}

	var superusers []core.User
	core.DB.Select("name, id, superuser").Where("superuser = true OR id = ?", r.UserID).Order("name ASC").Find(&superusers)

	var myGroups []core.Group
	core.DB.Where("id IN (?)", core.DB.Table("user_groups").Select("group_id").Where("user_id = ?", user.ID)).Order("name ASC").Find(&myGroups)

	// TODO ugly
	var users []core.User
	core.DB.Select("id", "name").Find(&users)

	var aclGroup []core.Acl
	core.DB.Joins("Group").Where("repository_id = ? AND group_id > 0", r.ID).Order("name ASC").Find(&aclGroup)
	var aclUser []core.Acl
	core.DB.Joins("User").Where("repository_id = ? AND user_id > 0", r.ID).Order("name ASC").Find(&aclUser)

	return ctx.Render(http.StatusOK, "repo_members_edit", echo.Map{
		"navPage":    "repo",
		"user":       user,
		"users":      users,
		"repo":       r,
		"acl":        acl,
		"aclGroup":   aclGroup,
		"aclUser":    aclUser,
		"aclLevel":   core.AclStrings,
		"superusers": superusers,
		"myGroups":   myGroups,
		"errors":     []string{},
		"success":    []string{},
	})
}

func repoMembersPostHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	id, _ := strconv.Atoi(ctx.Param("id"))
	var r core.Repository
	core.DB.Where("id = ?", id).First(&r)
	if r.ID <= 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=Invalid repo")
	}

	acl := core.AclCheck(user, r)
	if acl < core.AclComplete {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=Permission Error")
	}

	aclLevel, err := strconv.Atoi(ctx.FormValue("aclLevel"))
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=Invalid privilege level")
	}
	if aclLevel < int(core.AclForbidden) && aclLevel > int(core.AclComplete) {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=Invalid privileeg level")
	}

	usr_id := ctx.FormValue("aclUserId")
	grp_id := ctx.FormValue("aclGroupId")
	acl_id := ctx.FormValue("aclId")
	fmt.Printf("%+v", ctx.Request().Form)
	fmt.Println(usr_id, grp_id, acl_id)
	if grp_id == "" && acl_id == "" && usr_id == "" {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=Group not found")
	}

	var newacl core.Acl
	if acl_id != "" {
		aclid, err := strconv.Atoi(acl_id)
		if err != nil {
			return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=Membership not found")
		}

		core.DB.Model(&core.Acl{}).Where("id=? AND repository_id = ?", aclid, r.ID).Update("access_level", aclLevel)
	} else {
		if grp_id != "" {
			// groups
			group_id, err := strconv.Atoi(grp_id)
			if err != nil {
				return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=Group not found")
			}

			var group core.Group
			core.DB.Where("id = ?", group_id).First(&group)
			if group.ID <= 0 {
				return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=Group not found")
			}

			core.DB.Where("group_id=? AND repository_id = ?", group_id, r.ID).First(&newacl)
			if newacl.ID > 0 {
				core.DB.Model(&core.Acl{}).Where("id = ?", newacl.ID).Update("access_level", aclLevel)
			} else {
				newacl.AccessLevel = core.AclLevel(aclLevel)
				newacl.RepositoryID = r.ID
				newacl.Group = &group
				core.DB.Save(&newacl)
			}
		} else {
			// user
			user_id, err := strconv.Atoi(usr_id)
			if err != nil {
				return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=User not found")
			}

			var user core.User
			core.DB.Where("id = ?", user_id).First(&user)
			if user.ID <= 0 {
				return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=User not found")
			}

			core.DB.Where("user_id=? AND repository_id = ?", user_id, r.ID).First(&newacl)
			if newacl.ID > 0 {
				core.DB.Model(&core.Acl{}).Where("id = ?", newacl.ID).Update("access_level", aclLevel)
			} else {
				newacl.AccessLevel = core.AclLevel(aclLevel)
				newacl.RepositoryID = r.ID
				newacl.User = &user
				core.DB.Save(&newacl)
			}
		}
	}

	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?success=Membership successfully added")
}

func repoMembersDeleteHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	id, _ := strconv.Atoi(ctx.Param("id"))
	var r core.Repository
	core.DB.Where("id = ?", id).First(&r)
	if r.ID <= 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=Invalid repo")
	}

	acl := core.AclCheck(user, r)
	if acl < core.AclComplete {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=Permission Error")
	}

	acl_id, err := strconv.Atoi(ctx.Param("acl_id"))
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=Invalid ACL")
	}

	var acls core.Acl
	acls.ID = uint(acl_id)
	result := core.DB.Where("repository_id = ?", r.ID).Delete(&acls)
	if result.RowsAffected == 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?error=Invalid ACL")
	}

	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/members/"+strconv.Itoa(id)+"?success=Deleted ACL")
}

// /repo/:id or /repo/view/:id
// shows the list of builds for a repo
func repoViewHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	id, _ := strconv.Atoi(ctx.Param("id"))
	pg, _ := strconv.Atoi(ctx.Param("page"))
	var page = int64(pg)
	if page <= 0 {
		page = 1
	}
	offset := (page - 1) * 10
	var r core.Repository
	var u core.User
	core.DB.Where("id = ?", id).First(&r)
	if r.ID <= 0 {
		return HTTPError(404, ctx)
	}
	// auth check
	acl := core.AclCheck(user, r)
	if acl < core.AclView {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo?error=Can't view that repository")
	}

	core.DB.Select("name, id").Where("id = ?", r.UserID).First(&u)
	var cnt int64
	core.DB.Model(core.Build{}).Where("repository_id = ? AND parent_id = 0", r.ID).Count(&cnt)
	var pageMax int64 = (cnt / 10) + 1
	if offset >= cnt {
		offset = (cnt / 10) * 10
		page = (offset / 10) + 1
	}

	cnt = 0
	var b []core.Build
	var bx core.Build
	core.DB.Where("repository_id = ? AND parent_id = 0", r.ID).Order("started_at DESC").Offset(int((page - 1) * 10)).Limit(10).Find(&b)
	core.DB.Select("id").Where("repository_id = ? AND status NOT IN ('finished', 'failed')", r.ID).First(&bx)

	// TODO pagination surely can be done simpler
	var pageRange []int64
	if pageMax > 9 {
		pageRange = make([]int64, 11)
		pageRange[0] = 1
		pageRange[1] = 2
		pageRange[2] = 3
		if page == 4 || page == 5 {
			pageRange[3] = 4
			pageRange[4] = 5
			pageRange[5] = 6
			pageRange[6] = 7
			pageRange[7] = -1 // => ...
		} else if page == pageMax-4 || page == pageMax-3 {
			pageRange[3] = -1
			pageRange[4] = pageMax - 6
			pageRange[5] = pageMax - 5
			pageRange[6] = pageMax - 4
			pageRange[7] = pageMax - 3
		} else {
			pageRange[3] = -1
			if page < pageMax-4 && page > 5 {
				pageRange[4] = page - 1
				pageRange[5] = page
				pageRange[6] = page + 1
			} else {
				pageRange[4] = pageMax / 2
				pageRange[5] = pageMax/2 + 1
				pageRange[6] = pageMax/2 + 2
			}
			pageRange[7] = -1
		}
		pageRange[8] = pageMax - 2
		pageRange[9] = pageMax - 1
		pageRange[10] = pageMax
	} else {
		pageRange = make([]int64, pageMax)
		var i int64 = 1
		for i <= pageMax {
			pageRange[i-1] = i
			i++
		}
	}

	var webhookURL = ctx.Scheme() + "://" + ctx.Request().Host + core.Cfg.HttpSubUrl + "repo/hook/" + strconv.Itoa(int(r.ID)) + "/{api}"
	var stateURL = core.Cfg.HttpSubUrl + "repo/state/" + strconv.FormatUint(uint64(r.ID), 10)
	var stateImg = repoState(&user, r)
	var stateString string
	switch stateImg {
	case STATE_MISSED:
		stateString = "An error occurred"
	case STATE_FAILED:
		stateString = "Repository build failed"
	case STATE_PASSED:
		stateString = "Repository build succeeded"
	}
	return ctx.Render(http.StatusOK, "repo_view", echo.Map{
		"navPage":                "repo",
		"acl":                    acl,
		"user":                   user,
		"u":                      u,
		"repo":                   r,
		"stateUrl":               stateURL,
		"stateString":            stateString,
		"builds":                 b,
		"unfinishedBuild":        bx.ID,
		"page":                   page,
		"pageMax":                pageMax,
		"pageRange":              pageRange,
		"webhookUrl":             webhookURL,
		"render_test_state":      repoTestRenderResult,
		"create_linear_gradient": repoTestCreateGradient,
	})
}
