package web

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"time"

	"codeberg.org/snaums/mvoCI/core"
	"github.com/labstack/echo/v4"
)

func repoTestDetailHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	id, _ := strconv.Atoi(ctx.Param("id"))
	run_id, _ := strconv.Atoi(ctx.Param("run_id"))
	test_id, _ := strconv.Atoi(ctx.Param("test_id"))

	var repo core.Repository
	core.DB.Select("id, name, user_id").Where("id = ?", id).First(&repo)
	if repo.ID == 0 {
		return ctx.Redirect(http.StatusFound, "/")
	}

	acl := core.AclCheck(user, repo)
	if acl < core.AclView {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/"+strconv.Itoa(id)+"?error=Permission Error")
	}

	var run core.Build
	core.DB.Where("id = ? AND repository_id = ?", run_id, id).First(&run)
	if run.ID == 0 {
		return ctx.Redirect(http.StatusFound, "/repo/"+strconv.Itoa(int(id)))
	}

	var t core.Test
	core.DB.Where("id = ?", test_id).First(&t)
	if t.BuildID != run.ID || t.ID == 0 {
		return ctx.Redirect(http.StatusFound, "/repo/tests/"+strconv.Itoa(int(id))+"/run/"+strconv.Itoa(int(run_id)))
	}

	return ctx.Render(http.StatusOK, "repo_test_detail", echo.Map{
		"navPage":         "repo",
		"user":            user,
		"repo":            repo,
		"run":             run,
		"test":            t,
		"render_duration": repoTestRenderDuration,
	})
}

func repoTestRenderDuration(d time.Duration) string {
	return d.Truncate(time.Millisecond).String()
}

func repoTestRenderResult(t core.Build) string {
	var succ float32 = float32(t.NumSuccess) / float32(t.NumTotal)
	var fail float32 = float32(t.NumFailed) / float32(t.NumTotal)

	if fail < 0.1 && succ > 0.8 {
		return core.TestResultOk
	} else if fail < 0.1 {
		return core.TestResultError
	} else {
		return core.TestResultFail
	}
}

func repoTestRenderRate(t core.Build) string {
	var succ float32 = (float32(t.NumSuccess) / float32(t.NumTotal)) * 100.0
	var fail float32 = (float32(t.NumFailed) / float32(t.NumTotal)) * 100.0

	if fail > 0.1 {
		return fmt.Sprintf("%3.0f %% failed", fail)
	} else {
		return fmt.Sprintf("%3.0f %% passed", succ)
	}
}

func repoTestCreateGradient(b core.Build) template.CSS {
	if b.NumTotal == 0 {
		return "transparent"
	}

	var okEnd = uint64((b.NumSuccess * 100) / b.NumTotal)
	var ok = strconv.FormatUint(okEnd, 10)
	var skipEnd = uint64(100 - ((b.NumFailed * 100) / b.NumTotal))
	var skip = strconv.FormatUint(skipEnd, 10)
	return template.CSS("linear-gradient(to right, var(--state-ok) " + ok + "%, var(--state-skip) " + ok + "%, var(--state-skip) " + skip + "%, var(--state-failed) " + skip + "%)")
}

func repoTestRunHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	id, _ := strconv.Atoi(ctx.Param("id"))
	run_id, _ := strconv.Atoi(ctx.Param("run_id"))
	filtermode := ctx.Param("filtermode")
	switch filtermode {
	case core.TestResultOk, core.TestResultError, core.TestResultFail, core.TestResultSkip, core.TestResultUnknown:
	default:
		filtermode = ""
	}

	var repo core.Repository
	core.DB.Select("id, name, user_id").Where("id = ?", id).First(&repo)
	if repo.ID == 0 {
		return ctx.Redirect(http.StatusFound, "/")
	}

	acl := core.AclCheck(user, repo)
	if acl < core.AclView {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/"+strconv.Itoa(id)+"?error=Permission Error")
	}

	var run core.Build
	if filtermode == "" {
		core.DB.Where("id = ? AND repository_id = ?", run_id, id).Preload("Tests").First(&run)
	} else {
		var t []*core.Test
		core.DB.Where("id = ? AND repository_id = ?", run_id, id).First(&run)
		core.DB.Where("build_id = ? AND result = ?", run.ID, filtermode).Find(&t)
		run.Tests = t
	}
	if run.ID == 0 {
		return ctx.Redirect(http.StatusFound, "/repo/"+strconv.Itoa(int(id)))
	}

	return ctx.Render(http.StatusOK, "repo_test_run", echo.Map{
		"navPage":                "repo",
		"user":                   user,
		"repo":                   repo,
		"run":                    run,
		"render_duration":        repoTestRenderDuration,
		"filtermodes":            []string{core.TestResultOk, core.TestResultSkip, core.TestResultError, core.TestResultFail},
		"filtermode":             filtermode,
		"create_linear_gradient": repoTestCreateGradient,
	})
}

func repoTestsHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	id, _ := strconv.Atoi(ctx.Param("id"))

	var repo core.Repository
	core.DB.Select("id, name, user_id").Where("id = ?", id).First(&repo)

	if repo.ID == 0 {
		return ctx.Redirect(http.StatusFound, "/")
	}

	acl := core.AclCheck(user, repo)
	if acl < core.AclView {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/"+strconv.Itoa(id)+"?error=Permission Error")
	}

	var testruns []core.Build
	core.DB.Where("repository_id = ? AND num_total > 0", id).Order("started_at desc").Find(&testruns)

	return ctx.Render(http.StatusOK, "repo_tests", echo.Map{
		"navPage":                "repo",
		"user":                   user,
		"repo":                   repo,
		"testruns":               testruns,
		"render_result":          repoTestRenderResult,
		"render_rate":            repoTestRenderRate,
		"create_linear_gradient": repoTestCreateGradient,
	})
}
