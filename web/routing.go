// the package web contains the web-related stuff, i.e. all handlers
// the routing "table" and loads of functions for rendering the views and
// interacting with the database
package web

import (
	"fmt"
	"html/template"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"codeberg.org/snaums/mvoCI/core"

	"github.com/foolin/goview"
	"github.com/foolin/goview/supports/echoview-v4"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// structure for globally known values inside the web-package
type Server struct {
	echo *echo.Echo // global reference to the echo object
}

var s *Server

// sends an information back, that there was no database object
func dbErrorHandler(ctx echo.Context) error {
	return ctx.String(http.StatusOK, "Database connection refused")
}

// the routing "table"
func (s *Server) Routes() {
	if core.DB != nil || (core.DB == nil && core.Cfg.Install) {
		s.echo.GET("/", indexHandler)
		s.echo.GET("/login", loginHandler_)
		s.echo.GET("/impress", impressHandler)
		s.echo.GET("/dashboard", indexHandler)

		if !core.Cfg.Install {
			s.echo.POST("/login", doLoginHandler)
			s.echo.GET("/install/:id", indexHandler)
			s.echo.GET("/install/", indexHandler)
			s.echo.GET("/install", indexHandler)
			s.echo.GET("/landing", landingHandler)
			s.echo.GET("/logout", doLogoutHandler)
			s.echo.GET("/integration", integrationHandler)
			// USER
			s.echo.GET("/user", userHandler)
			s.echo.GET("/user/:id", userEditHandler)
			s.echo.GET("/user/edit/:id", userEditHandler)
			s.echo.GET("/user/add", userAddHandler)
			//s.echo.POST ( "/user/post/add",      userAddPostHandler )
			//s.echo.POST ( "/user/post/edit/:id", userEditPostHandler )
			//s.echo.GET  ( "/user/delete/:id",    userDeleteHandler )
			s.echo.GET("/user/repo/:id", userRepoHandler)
			s.echo.GET("/user/group/:id", userGroupHandler)
			//s.echo.POST ( "/user/group/add/:id", apiGroupAddMember )
			s.echo.GET("/user/:id/group/leave/:group_id", userLeaveGroupHandler)

			s.echo.POST("/user/:id/token/add", userTokenAdd)
			s.echo.GET("/user/:id/token/invalidate/:tid", userTokenInvalidate)

			// USER AUTH CONFIG
			s.echo.GET("/user/me/auth/enable/:module", userAuthModuleEnable)
			s.echo.GET("/user/me/auth/setup/:module", userAuthModuleSetupView)
			s.echo.POST("/user/me/auth/setup/:module", userAuthModuleCommit)
			s.echo.GET("/user/me/auth/config/:module", userAuthModuleConfigView)
			s.echo.POST("/user/me/auth/config/:module", userAuthModuleConfigCommit)
			s.echo.GET("/user/me/auth/disable/:module", userAuthModuleDisable)

			// REPO
			s.echo.GET("/repo", repoHandler)
			s.echo.GET("/repo/add", repoAddHandler)
			s.echo.GET("/repo/:id", repoViewHandler)
			s.echo.GET("/repo/view/:id", repoViewHandler)
			s.echo.GET("/repo/view/:id/:page", repoViewHandler)
			s.echo.GET("/repo/edit/:id", repoEditHandler)
			s.echo.GET("/repo/clone/:id", repoCloneHandler)
			s.echo.GET("/repo/members/:id", repoMembersHandler)
			s.echo.POST("/repo/members/:id", repoMembersPostHandler)
			s.echo.GET("/repo/members/:id/delete/:acl_id", repoMembersDeleteHandler)
			//s.echo.POST ( "/repo/post/add",       repoAddPostHandler );
			//s.echo.POST ( "/repo/post/edit/:id",  repoEditPostHandler )
			//s.echo.GET  ( "/repo/delete/:id",     repoDeleteHandler );
			s.echo.GET("/repo/build/:id", repoBuildHandler)
			s.echo.GET("/repo/state/:id", repoStateHandler)

			s.echo.GET("/repo/variants/:id", buildVariantHandler)
			s.echo.GET("/repo/variants/:id/add", buildVariantAddHandler)
			s.echo.GET("/repo/variants/:id/edit/:variant_id", buildVariantEditHandler)
			s.echo.GET("/repo/variants/:id/clone/:variant_id", buildVariantCloneHandler)

			s.echo.GET("/repo/tests/:id", repoTestsHandler)
			s.echo.GET("/repo/tests/:id/run/:run_id", repoTestRunHandler)
			s.echo.GET("/repo/tests/:id/run/:run_id/:filtermode", repoTestRunHandler)
			s.echo.GET("/repo/tests/:id/run/:run_id/test/:test_id", repoTestDetailHandler)

			// BUILDs
			s.echo.GET("/build/:id", buildViewHandler)
			s.echo.GET("/build/log/:id", buildLogHandler)
			s.echo.GET("/build/delete/:id", buildDeleteHandler)
			s.echo.GET("/build/rebuild/:id", buildRebuildHandler)
			s.echo.GET("/zip/:file", zipDownloadHandler)

			// SSH Keys
			s.echo.GET("/keys", keysOverviewHandler)
			s.echo.GET("/keys/edit/:host", keysEditHandler)
			s.echo.GET("/keys/delete/:host", keysDeleteHandler)
			s.echo.GET("/keys/add", keysAddHandler)
			s.echo.POST("/keys/add", keysAddPostHandler)
			s.echo.POST("/keys/edit/:host", keysEditPostHandler)

			// WEBHOOK!
			s.echo.POST("/push/hook/:api", webhook)
			s.echo.POST("/repo/hook/:id/:api", webhookRepo)

			// oauth redirect URI
			s.echo.GET("/oauth/validate/:api/:oauthid", oauthApiHook)
			// OAuth
			s.echo.GET("/oauth", oauthOverviewHandler)
			s.echo.GET("/oauth/add", oauthAddHandler)
			s.echo.POST("/oauth/add", oauthAddPostHandler)
			s.echo.GET("/oauth/:id/invalidate", oauthInvalidateHandler)

			// API
			s.echo.Any("/api/v1/:endpoint", apiHandler)
			s.echo.Any("/api/v1/:endpoint/:action", apiHandler)
			s.echo.Any("/api/v1/:endpoint/:action/:id", apiHandler)

			// admin stuff
			s.echo.GET("/admin/webhooklog", WebHookLogHandler)
			s.echo.GET("/admin/webhooklog/:repoid", WebHookLogHandler)
			s.echo.GET("/admin/webhooklog/detail/:id", WebHookLogDetailHandler)
			s.echo.GET("/admin/info", AdminInfoHandler)
			s.echo.GET("/admin/eventloop", AdminEventLoopHandler)
			s.echo.GET("/admin/eventloop/sendTest", AdminEventLoopTestEventHandler)
			s.echo.GET("/admin/eventloop/sendNightlyBuild", AdminNightlyTestEventHandler)
			s.echo.GET("/admin/mail", AdminMailHandler)
			s.echo.GET("/admin/mail/reset", AdminMailResetHandler)
			s.echo.POST("/admin/mail", AdminMailPostHandler)
			s.echo.GET("/admin/auth", AdminAuthHandler)
			s.echo.GET("/admin/auth/instance/:id", AdminAuthInstanceHandler)
			s.echo.GET("/admin/auth/instance/new/:name", AdminAuthInstanceNewHandler)
			s.echo.GET("/admin/vacuum", AdminDatabaseHandler)
			s.echo.GET("/admin/vacuum/:action", AdminDatabaseVacuumHandler)

			if core.Cfg.PublicEnable {
				s.echo.GET("/public", publicOverviewHandler)
				s.echo.GET("/public/:id", publicRepoHandler)
				s.echo.GET("/public/:id/:page", publicRepoHandler)
				s.echo.GET("/public/zip/:file", publicZipHandler)
				s.echo.GET("/public/:id/tests/:run_id", publicTestRunHandler)
				s.echo.GET("/public/:id/tests/:run_id/:filtermode", publicTestRunHandler)
				s.echo.GET("/public/:id/build/:build_id", publicBuildHandler)
				s.echo.GET("/public/:id/log/:build_id", publicBuildLogHandler)
			}
		}
		if core.Cfg.Install {
			s.echo.GET("/landing", installHandler)
			s.echo.GET("/install/:id", installHandler)
			s.echo.GET("/install/", installHandler)
			s.echo.GET("/install", installHandler)
			s.echo.POST("/install/post/:id", installPostHandler)
		}
	} else {
		s.echo.GET("/", dbErrorHandler)
	}
}

// renders a list of errors
func render_error_list(errors []string) template.HTML {
	if len(errors) <= 0 {
		return template.HTML("")
	}
	var result string
	result = "<div class=\"messages error\"><ul>"
	for _, v := range errors {
		result += "<li>" + v + "</li>\n"
	}
	result += "</ul></div>"
	return template.HTML(result)
}

// Returns html-code for the build-status of a repo
func BuildStatus(s string) template.HTML {
	var f string
	switch s {
	case "started":
		f = "<i class=\"fa fa-cog fa-spin\" title=\"Building\"></i>"
	case "enqueued":
		f = "<i class=\"fa fa-clock-o\" title=\"Queued\"></i>"
	case "failed", "Failed":
		f = "<i class=\"fa fa-times-circle\" title=\"Error\"></i>"
	case "finished", "Success":
		f = "<i class=\"fa fa-check-circle\" title=\"Success\"></i>"
	default:
		f = "<i class=\"fa fa-question-circle\" title=\"Unknown\"></i>"
	}
	return template.HTML(f)
}

// Render time on the website
func RenderTime(t time.Time) string {
	return t.Format("Mon Jan _2 15:04:05")
}

// sets up the echo server
func NewServer() Server {
	server := Server{}

	server.echo = echo.New()

	var serverlogger *core.ServerLogger
	serverlogger, err := core.ServerLoggerNew(core.Cfg.LogServer)
	if err == nil {
		server.echo.Use(middleware.LoggerWithConfig(
			middleware.LoggerConfig{
				Output:           serverlogger,
				Format:           "${time_custom} [ECHO] ${method} uri=${uri}, status=${status}, latency=${latency_human}: ${error}\n",
				CustomTimeFormat: "2006/01/02 15:04:05",
			},
		))
	} else {
		server.echo.Use(middleware.LoggerWithConfig(
			middleware.LoggerConfig{
				Format:           "${time_custom} [ECHO] ${method} uri=${uri}, status=${status}, latency=${latency_human}: ${error}\n",
				CustomTimeFormat: "2006/01/02 15:04:05",
			},
		))
	}

	server.echo.Use(middleware.Recover())
	server.echo.Renderer = echoview.New(goview.Config{
		Root:      "views",
		Extension: ".html",
		Master:    "layouts/master",
		Funcs: template.FuncMap{
			"app_title": func() string {
				return core.Cfg.AppTitle
			},
			"app_version": func() string {
				return core.Version
			},
			"siteurl": func() string {
				return core.Cfg.HttpSubUrl
			},
			"config": func(q string) string {
				return core.Cfg.Reflect(q)
			},
			"config_html": func(q string) template.HTML {
				return template.HTML(core.Cfg.Reflect(q))
			},
			"aclCheck":          core.WebAclCheck,
			"render_error_list": render_error_list,
			"build_status":      BuildStatus,
			"render_time":       RenderTime,
		},
		DisableCache: true,
	})
	server.echo.Use(middleware.Decompress())
	server.echo.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Skipper: func(c echo.Context) bool {
			return strings.Contains(c.Path(), "/zip/")
		},
	}))
	server.echo.Use(middleware.Secure())
	server.echo.HTTPErrorHandler = customHTTPErrorHandler

	server.Routes()
	server.echo.Static("/static", "static")
	server.echo.Static("/favicon.ico", "static/img/favicon.png")
	//server.echo.Static ( "/zip", "builds" ) //strings.TrimRight(core.Cfg.Directory.Build, "/") )
	//server.echo.Static ( "/public/zip", "builds" ) // strings.TrimRight(core.Cfg.Directory.Build, "/") )
	//server.echo.Static ( "/public/zip", core.Cfg.Directory.Build )
	s = &server
	return server
}

// returns an HTTP error
func HTTPError(code int, ctx echo.Context) error {
	errorPage := fmt.Sprintf("/error/HTTP%d.html", code)
	m := echo.Map{
		"author_email": core.Cfg.Author.Email,
		"author_name":  core.Cfg.Author.Name,
	}
	if _, err := os.Stat("/view" + errorPage); err != nil {
		return ctx.Render(code, errorPage, m)
	} else {
		ctx.Logger().Error(err)
		return nil
	}
}

// serves a custom HTTP error page
func customHTTPErrorHandler(err error, ctx echo.Context) {
	code := http.StatusInternalServerError
	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
	}
	errorPage := fmt.Sprintf("/error/HTTP%d.html", code)
	m := echo.Map{
		"author_email": core.Cfg.Author.Email,
		"author_name":  core.Cfg.Author.Name,
	}
	if err := ctx.Render(code, errorPage, m); err != nil {
		ctx.Logger().Error(err)
	}
	ctx.Logger().Error(err)
}

// starts the server and prints minor debug information
func (s *Server) Start() {
	core.Console.Log("Starting server at " + core.Cfg.HttpHost + ":" + strconv.FormatInt(int64(core.Cfg.HttpPort), 10))
	err := s.echo.Start(core.Cfg.HttpHost + ":" + strconv.FormatInt(int64(core.Cfg.HttpPort), 10))
	if err != nil {
		core.Console.Fail("Failed to start HTTP Server: ", err)
	}
}
