// installation views and handlers

package web

import (
	"net/http"
	"os"
	"regexp"
	"strconv"

	"codeberg.org/snaums/mvoCI/auth"
	"codeberg.org/snaums/mvoCI/core"

	//"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	//"github.com/labstack/echo/middleware"
)

// TODO medium to long term, this ([]string,[]string) thing should go.
// It was ment as (success, errors)-messages, but it turns out an error
// would also just suffise.

// try and commit the database configuration
func dbConfigurationHandler(ctx echo.Context) ([]string, []string) {
	provider := ctx.FormValue("db_type")
	var file string
	var host string
	var user string
	var passwd string
	var port int
	var dbname string
	var sslrequired string
	switch provider {
	case "sqlite3":
		file = ctx.FormValue("sqlite3_file")
		core.Cfg.Database.Provider = provider
		core.Cfg.Database.Filename = file
	case "postgres":
		sslrequired = ctx.FormValue("postgres_ssl")
		core.Cfg.Database.PostgresSSL = sslrequired
		fallthrough
	case "mssql":
		fallthrough
	case "mysql":
		host = ctx.FormValue(provider + "_host")
		passwd = ctx.FormValue(provider + "_password")
		user = ctx.FormValue(provider + "_user")
		port, _ = strconv.Atoi(ctx.FormValue(provider + "_port"))
		dbname = ctx.FormValue(provider + "_database")

		core.Cfg.Database.Provider = provider
		core.Cfg.Database.Host = host
		core.Cfg.Database.Port = port
		core.Cfg.Database.Username = user
		core.Cfg.Database.Password = passwd
		core.Cfg.Database.Dbname = dbname
	default:
		return []string{"Invalid provider selected"}, []string{}
	}

	var err error
	err = core.DBConnect(false)
	if core.DB != nil {
		core.Cfg.Install = false
		err = core.Cfg.ConfigWrite(core.ConfigFile)
		if err != nil {
			return []string{"Unable to write config: ", err.Error()}, []string{}
		}
		core.Cfg.Install = true
		return []string{}, []string{"Database configuration saved"}
	} else {
		return []string{"Cannot connect to the database: " + err.Error()}, []string{}
	}
}

// create the first user based on the input from the users
func userConfigurationHandler(ctx echo.Context) ([]string, []string) {
	username := ctx.FormValue("root_name")
	pass1 := ctx.FormValue("root_password")
	pass2 := ctx.FormValue("root_repeat")
	email := ctx.FormValue("root_email")

	if pass1 != pass2 {
		return []string{"Passwords were not identical"}, []string{}
	}
	if len(username) <= 0 || len(email) <= 0 || len(pass1) <= 0 {
		return []string{"Did you input anything?"}, []string{}
	}

	re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if !re.MatchString(email) {
		return []string{"You have to input an email-address."}, []string{}
	}

	usr := core.User{Name: username, Email: email, Superuser: true}
	if err := auth.AuthSetSecret(&usr, "native", pass1, pass2); err != nil {
		return []string{"Error in the auth module", err.Error()}, []string{}
	}

	core.DB.Create(&usr)
	if err := auth.AuthSetMain(&usr, "native", "native", ""); err == nil {
		core.DB.Save(&usr)
		_ = core.CreateGroupEveryone(usr)
		return []string{}, []string{"User creation successfully"}
	} else {
		return []string{"Error in the auth module", err.Error()}, []string{}
	}
}

// render the installation page based on the step (id)
func renderInstallPage(ctx echo.Context, id string, errs []string, succs []string) error {
	file := "install" + id
	if _, err := os.Stat("views/" + file + ".html"); err == nil {
		return ctx.Render(http.StatusOK, file, echo.Map{
			"installpage": func(i int) string {
				return "/install/" + strconv.Itoa(i)
			},
			"errors": func() []string {
				return errs
			},
			"success": func() []string {
				return succs
			},
			"navPage": "",
		})
	} else {
		return HTTPError(http.StatusNotFound, ctx)
	}
}

// POST /install/post/:id
// take the input from the user  and do database or user config based on stepcount
func installPostHandler(ctx echo.Context) error {
	id := ctx.Param("id")
	next, _ := strconv.Atoi(id)
	var errs []string
	var succs []string
	switch next {
	case 2:
		errs, succs = dbConfigurationHandler(ctx)
	case 3:
		if core.DB == nil {
			return ctx.String(http.StatusInternalServerError, "No database connection found")
		}
		errs, succs = userConfigurationHandler(ctx)
	default:
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"install/"+id)
	}
	if len(errs) <= 0 {
		next += 1
	}
	return renderInstallPage(ctx, strconv.Itoa(next), errs, succs)
}

// /install/:id
func installHandler(ctx echo.Context) error {
	id := ctx.Param("id")
	if id == "" {
		id = "1"
	}

	return renderInstallPage(ctx, id, []string{}, []string{})
}
