// group views and handlers

package web

import (
	"net/http"
	"strconv"

	"codeberg.org/snaums/mvoCI/core"

	"github.com/labstack/echo/v4"
)

func userGroupHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	group_id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil || group_id <= 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user?error=User not found")
	}

	var g core.Group
	core.DB.Where("id = ?", group_id).First(&g)
	if g.ID <= 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user?error=Group not found")
	}

	var u []core.User
	core.DB.Where("id IN (?)", core.DB.Table("user_groups").Select("user_id").Where("group_id = ?", group_id)).Select("id", "name", "superuser").Find(&u)

	var amIin = false
	if !user.Superuser {
		for _, ux := range u {
			if ux.ID == user.ID {
				amIin = true
				break
			}
		}
	}

	if user.Superuser || amIin {
		return ctx.Render(http.StatusOK, "user_group", echo.Map{
			"navPage": "user",
			"group":   g,
			"users":   u,
			"user":    user,
		})
	} else {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user?error=Permission Error")
	}
}

func userLeaveGroupHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	user_id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil || user_id <= 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user?error=User not found")
	}
	if user.ID != uint(user_id) && !user.Superuser {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user?error=Permission Error")
	}

	var u core.User
	var g core.Group
	core.DB.Where("id = ?", user_id).First(&u)
	if u.ID <= 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user?error=User not found")
	}

	group_id, err := strconv.Atoi(ctx.Param("group_id"))
	if err != nil || group_id <= 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/"+strconv.Itoa(user_id)+"?error=GroupEmail not found")
	}

	core.DB.Where("id = ?", group_id).First(&g)
	if g.ID <= 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/"+strconv.Itoa(user_id)+"?error=Group not found")
	}

	err = g.Leave(u)
	if err != nil {
		core.Console.Warn("Group leave error: ", err.Error())
	}

	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/"+strconv.Itoa(user_id)+"?success=Left the group")
}
