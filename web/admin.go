// handlers for /admin/*

package web

import (
	"net/http"
	"strconv"
	"time"

	"codeberg.org/snaums/mvoCI/auth"
	"codeberg.org/snaums/mvoCI/core"
	"codeberg.org/snaums/mvoCI/events"

	"github.com/labstack/echo/v4"
)

// represents a modulename and a version
type iversions struct {
	Title string // the version name or title
	Value string // the version of the module or part
}

// represents a headline on the info page and its modules and versions
type versionStruct struct {
	Name     string      // headline
	Versions []iversions // list of versions
}

// /admin/auth
// list all authentication modules
func AdminAuthHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return HTTPError(401, ctx)
	}

	provider := auth.GetAuthProvider()
	return ctx.Render(http.StatusOK, "admin_auth", echo.Map{
		"navPage":  "admin",
		"user":     user,
		"provider": provider,
	})
}

type VacuumFn func()
type AdminVacuumAction struct {
	Label       string
	Description string
	Url         string
	Fn          VacuumFn
}

var admin_vacuum_actions = []AdminVacuumAction{
	{Label: "Clean Old Builds/Tests + Artifacts", Description: "Empties > 3mo old Build Logs and removes build artifacts", Url: "clean_build_log_artifacts", Fn: adminVacuum_cleanBuildLogArtifacts},
	{Label: "Clean Old Builds/Tests", Description: "Empties > 3mo old Build Logs", Url: "clean_build_log", Fn: adminVacuum_cleanBuildLog},
	{Label: "Delete Build/Test Entries", Description: "Delete > 3mo old Build Entries from Database", Url: "remove_build_log", Fn: adminVacuum_removeBuilds},
	{Label: "Remove Webhook Logs", Description: "Delete all Webhook-Log Entries", Url: "remove_webhook_log", Fn: adminVacuum_removeWebhookLog},
}

func adminVacuum_cleanBuildLogArtifacts() {
	var until = time.Now().Add(-3 * (24 * time.Hour * 30 * 3))
	var b []core.Build
	core.DB.Select("id, zip").Where("finished_at < ?", until).Find(&b)
	for _, v := range b {
		buildDeleteArchive(v.Zip)
		core.DB.Model(&core.Test{}).Where("build_id = ?", v.ID).Update("log", "")
	}
	core.DB.Model(&core.Build{}).Where("finished_at < ?", until).Update("log", "")
	core.DB.Model(&core.Build{}).Where("finished_at < ?", until).Update("zip", "")
}

func adminVacuum_cleanBuildLog() {
	var until = time.Now().Add(-3 * (24 * time.Hour * 30 * 3))
	var b []core.Build
	core.DB.Select("id").Where("finished_at < ?", until).Find(&b)
	for _, v := range b {
		core.DB.Model(&core.Test{}).Where("build_id = ?", v.ID).Update("log", "")
	}
	core.DB.Model(&core.Build{}).Where("finished_at < ?", until).Update("log", "")
}

func adminVacuum_removeBuilds() {
	var until = time.Now().Add(-3 * (24 * time.Hour * 30 * 3))
	var b []core.Build
	core.DB.Select("id").Where("finished_at < ?", until).Find(&b)
	for _, v := range b {
		buildDelete(v.ID)
	}
}

func adminVacuum_removeWebhookLog() {
	core.DB.Delete(&core.WebHookLog{})
}

func AdminDatabaseVacuumHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return HTTPError(401, ctx)
	}

	action := ctx.Param("action")
	for _, v := range admin_vacuum_actions {
		if action == v.Url {
			v.Fn()
			return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"admin/vacuum?success=Finished")
		}
	}

	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"admin/vacuum?error=Unknown Action")
}

func AdminDatabaseHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return HTTPError(401, ctx)
	}

	return ctx.Render(http.StatusOK, "admin_vacuum", echo.Map{
		"navPage": "admin",
		"user":    user,
		"actions": admin_vacuum_actions,
	})
}

// /admin/auth/instance/:id
// TODO
func AdminAuthInstanceHandler(ctx echo.Context) error {
	return nil
}

// /admin/auth/instance/new/:name
// TODO
func AdminAuthInstanceNewHandler(ctx echo.Context) error {
	return nil
}

// /admin/info
// prints some information about the mvoCI server and its dependencies
func AdminInfoHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return HTTPError(401, ctx)
	}

	Versions := []versionStruct{
		{
			Name: "mvoCI",
			Versions: []iversions{
				{"Version", core.Version},
				{"Build Time", core.BuildTime},
				{"Compiler", core.Compiler},
				{"Git Hash", core.GitHash},
			},
		},
		{
			Name: "Dependencies",
			Versions: []iversions{
				{"Echo", echo.Version},
				// TODO: gorm.io
				// TODO: totp
			},
		},
	}

	return ctx.Render(http.StatusOK, "admin_info", echo.Map{
		"navPage":  "admin",
		"user":     user,
		"Versions": Versions,
	})
}

// /admin/mail
func AdminMailHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return HTTPError(401, ctx)
	}

	return ctx.Render(http.StatusOK, "admin_mail", echo.Map{
		"navPage": "admin",
		"user":    user,
	})
}

func AdminMailResetHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return HTTPError(401, ctx)
	}

	core.Cfg.Email = core.EmailServerSettings{}
	core.Cfg.ConfigWrite(core.ConfigFile)

	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"admin/mail?success=Mail Settings reset")
}

// /admin/mail (POST)
// sets mail server settings and writes config
func AdminMailPostHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return HTTPError(401, ctx)
	}

	host, err := apiGetParam(ctx, "email_host")
	port, err2 := apiGetNumber(ctx, "email_port")
	from, err3 := apiGetParam(ctx, "email_from")
	username, err4 := apiGetParam(ctx, "email_username")
	password, err5 := apiGetParam(ctx, "email_password")

	if host == "" || port == 0 || from == "" || username == "" ||
		err != nil || err2 != nil || err3 != nil || err4 != nil || err5 != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"admin/mail?error=Please fill the form fields")
	}

	core.Cfg.Email.Host = host
	core.Cfg.Email.Port = port
	core.Cfg.Email.From = from
	core.Cfg.Email.Username = username
	core.Cfg.Email.Password, err = core.MailPasswordSet(password)
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"admin/mail?error=Error settings the properties")
	}
	core.Cfg.ConfigWrite(core.ConfigFile)

	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"admin/mail?success=Mail Settings set")
}

// /admin/webhooklog and /admin/webhooklog/:repoid
// renders a listing of the received webhook events
func WebHookLogHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return HTTPError(401, ctx)
	}

	var r core.Repository
	var repoID int
	repoID, err := strconv.Atoi(ctx.Param("repoid"))
	if err == nil {
		core.DB.Where("id = ?", repoID).Find(&r)
		if r.ID > 0 {
			var WebHookLogs []core.WebHookLog
			core.DB.Preload("Repository").Where("repository_id = ?", r.ID).Order("created_at DESC").Find(&WebHookLogs)
			for i := range WebHookLogs {
				core.DB.Preload("Repository").Model(WebHookLogs[i])
			}
			return ctx.Render(http.StatusOK, "webhooklog", echo.Map{
				"navPage":     "admin",
				"repo":        r,
				"user":        user,
				"webhooklogs": WebHookLogs,
			})
		} else {
			return HTTPError(404, ctx)
		}
	}

	var WebHookLogs []core.WebHookLog
	core.DB.Preload("Repository").Order("created_at DESC").Find(&WebHookLogs)
	//for i, _ := range WebHookLogs {
	//    core.DB.Model( WebHookLogs[i] ).Related ( &WebHookLogs[i].Repository );
	//}

	return ctx.Render(http.StatusOK, "webhooklog", echo.Map{
		"navPage":     "admin",
		"user":        user,
		"webhooklogs": WebHookLogs,
	})
}

// admin/webhooklog/detail/:id
// renders a given webhook event with the response and request
func WebHookLogDetailHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return HTTPError(401, ctx)
	}

	var id int
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"admin/webhooklog")
	}

	var whlog core.WebHookLog
	core.DB.Preload("Repository").Preload("Build").First(&whlog, id)

	return ctx.Render(http.StatusOK, "webhooklog_view", echo.Map{
		"navPage": "admin",
		"user":    user,
		"whlog":   whlog,
	})
}

// admin/webhooklog/detail/:id
// renders a given webhook event with the response and request
func AdminEventLoopHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return HTTPError(401, ctx)
	}

	return ctx.Render(http.StatusOK, "admin_eventloop", echo.Map{
		"navPage": "admin",
		"user":    user,
	})
}

func AdminNightlyTestEventHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return HTTPError(401, ctx)
	}

	// Attaching something, so the nightly-event handler doesn't reset the nightly-timer
	events.SendEvent(events.OnTimerNightlyBuild, 1)

	return ctx.Redirect(http.StatusFound, "/admin/eventloop")
}

func AdminEventLoopTestEventHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	if !user.Superuser {
		return HTTPError(401, ctx)
	}

	events.SendEvent(events.OnTestEventSend, nil)

	return ctx.Redirect(http.StatusFound, "/admin/eventloop")
}
