// views and handlers for creating an OAUTH token

package web

import (
	//"regexp"
	"context"
	"encoding/json"
	"net/http"
	"strconv"

	"codeberg.org/snaums/mvoCI/core"
	"codeberg.org/snaums/mvoCI/hook"

	//"golang.org/x/crypto/bcrypt"
	//"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	//"github.com/labstack/echo/middleware"
)

// POST /oauth/add
func oauthAddPostHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	var name = ctx.FormValue("oauth_name")
	var api = ctx.FormValue("oauth_api")
	var url = ctx.FormValue("oauth_url")
	var clientId = ctx.FormValue("oauth_clientId")
	var clientSecret = ctx.FormValue("oauth_clientSecret")
	var id = ctx.FormValue("oauth_id")

	if len(name) > 0 && len(api) > 0 && len(url) > 0 && len(clientId) > 0 && len(clientSecret) > 0 && len(id) > 0 {
		var o core.OauthToken
		nID, err := strconv.ParseUint(id, 10, 32)
		if err != nil {
			return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"oauth?error=Invalid ID")
		}
		o.ID = uint(nID)
		o.Name = name
		o.Api = api
		o.Url = url
		o.ClientId = clientId
		o.ClientSecret = clientSecret
		o.State = "unvalidated"
		o.UserID = user.ID
		o.RedirectUri = ctx.Scheme() + "://" + ctx.Request().Host + core.Cfg.HttpSubUrl + "oauth/validate/" + o.Api + "/" + strconv.FormatUint(uint64(o.ID), 10)

		core.DB.Save(&o)

		authUrl := hook.AuthUrl(o.Url, o.ClientId, o.ClientSecret, []string{}, o.RedirectUri)

		return ctx.Redirect(http.StatusFound, authUrl)
	} else {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"oauth")
	}
}

// /oauth/add
func oauthAddHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	var o core.OauthToken
	o.State = "initialize"
	o.UserID = user.ID
	core.DB.Save(&o)

	o.RedirectUri = ctx.Scheme() + "://" + ctx.Request().Host + core.Cfg.HttpSubUrl + "oauth/validate/{{ api }}/" + strconv.FormatUint(uint64(o.ID), 10)
	core.DB.Save(&o)

	return ctx.Render(http.StatusOK, "oauth_edit", echo.Map{
		"navPage":     "integration",
		"user":        user,
		"oauthid":     o.ID,
		"redirectUrl": o.RedirectUri,
	})
}

// /oauth
func oauthOverviewHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	var oauth []core.OauthToken
	core.DB.Where("user_id = ? AND state='finished'", user.ID).Find(&oauth)

	return ctx.Render(http.StatusOK, "oauth_overview", echo.Map{
		"navPage": "integration",
		"user":    user,
		"oauth":   oauth,
	})
}

// /oauth/:id/invalidate
func oauthInvalidateHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	core.DB.Where("id=?", ctx.Param("id")).Delete(core.OauthToken{})

	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"oauth")
}

// /oauth/validate/:api/:oauthid
// redirect handler for being led to when OAUTH is returned.
// TODO add more APIs
func oauthApiHook(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	code := ctx.QueryParam("code")
	api := ctx.Param("api")
	id := ctx.Param("oauthid")

	if len(code) <= 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"oauth")
	}

	var o core.OauthToken
	switch api {
	case "gitea":
		core.DB.Where("id = ? ", id).First(&o)
		if o.ID <= 0 {
			return ctx.Render(http.StatusOK, "oauth_overview", echo.Map{
				"navPage": "integration",
				"user":    user,
				"errors":  []string{"Invalid OauthToken referenced by Oauth Provider"},
			})
		}

		_ctx := context.Background()
		conf := hook.CreateConf(o.Url, o.ClientId, o.ClientSecret, []string{}, o.RedirectUri)
		tok, err := conf.Exchange(_ctx, code)
		if err != nil {
			return ctx.Render(http.StatusOK, "oauth_overview", echo.Map{
				"navPage": "integration",
				"user":    user,
				"errors":  []string{"Error exchaning token: " + err.Error()},
			})
		}
		b, err := json.Marshal(tok)
		if err != nil {
			return ctx.Render(http.StatusOK, "oauth_overview", echo.Map{
				"navPage": "integration",
				"user":    user,
				"errors":  []string{"Unmarshalling error: " + err.Error()},
			})
		}

		o.ClientToken = string(b)
		o.State = "finished"
		core.DB.Save(&o)
	}

	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"oauth")
}
