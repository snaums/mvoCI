// user views and handlers

package web

import (
	"html/template"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"codeberg.org/snaums/mvoCI/auth"
	"codeberg.org/snaums/mvoCI/core"

	"github.com/labstack/echo/v4"
)

// /user
// list of users
func userHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}
	var u []core.User
	core.DB.Order("name asc").Find(&u)

	return ctx.Render(http.StatusOK, "user", echo.Map{
		"navPage": "user",
		"user":    user,
		"users":   u,
	})
}

// POST /user/:id/token/add
// add a API token to the current user with a given name
func userTokenAdd(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return HTTPError(500, ctx)
	}
	if user.ID != uint(id) && !user.Superuser {
		return HTTPError(401, ctx)
	}

	name := ctx.FormValue("user_token_name")
	var l core.LoginToken
	if NewAPIToken(&l, ctx, user, name) {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/edit/"+strconv.Itoa(int(user.ID)))
	}
	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/edit/"+strconv.Itoa(int(user.ID)))
}

// /user/:id/token/invalidate/:tid
// delete the API token from user id, with the token id tid
func userTokenInvalidate(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return HTTPError(500, ctx)
	}
	if user.ID != uint(id) && !user.Superuser {
		return HTTPError(401, ctx)
	}

	tid, err := strconv.Atoi(ctx.Param("tid"))
	if err != nil {
		return HTTPError(500, ctx)
	}

	core.DB.Where("id = ? AND type != 'login' AND user_id = ?", tid, user.ID).Delete(&core.LoginToken{})
	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/edit/"+strconv.Itoa(int(user.ID)))
}

// /user/repo/:id
// show repositories owned by a given user
func userRepoHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return HTTPError(401, ctx)
	}

	var u core.User
	core.DB.Where("id = ?", id).First(&u)
	if u.ID > 0 && (user.ID == u.ID || user.Superuser) {
		var r []core.Repository
		core.DB.Where("user_id = ?", u.ID).Order("name ASC").Find(&r)

		// repo_state type defined in web/repo.go
		var rx []repo_state
		for _, v := range r {
			var bx core.Build
			core.DB.Where("repository_id = ?", v.ID).Order("started_at DESC").Limit(1).First(&bx)
			rx = append(rx, repo_state{R: v, State: strings.ToLower(bx.Status)})
		}

		return ctx.Render(http.StatusOK, "repo", echo.Map{
			"navPage":      "repo",
			"user":         user,
			"repositories": rx,
			"u":            u,
		})
	}
	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/"+strconv.Itoa(id))
}

// /user/edit/:id
func userEditHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	var u core.User
	var idstring = ctx.Param("id")
	if idstring == "me" {
		u = user
	} else {
		id, err := strconv.Atoi(ctx.Param("id"))
		if err != nil {
			return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?error=Invalid ID")
		}
		core.DB.Where("id = ?", strconv.Itoa(id)).First(&u)
		if u.ID <= 0 {
			return HTTPError(404, ctx)
		}
	}

	navPage := "user"
	if u.ID == user.ID {
		navPage = "profile"
	} else {
		if !user.Superuser {
			return HTTPError(401, ctx)
		}
	}

	// list of user tokens
	var token []core.LoginToken
	core.DB.Where("user_id = ?", u.ID).Find(&token)

	var groups []core.Group
	core.DB.Where("id IN (?)", core.DB.Table("user_groups").Select("group_id").Where("user_id=?", u.ID)).Order("name ASC").Find(&groups)

	// get listing of auth modules and their config for the user
	authSetting := auth.ListAuth(u)
	return ctx.Render(http.StatusOK, "user_edit", echo.Map{
		"m":            "edit",
		"navPage":      navPage,
		"user":         user,
		"u":            u,
		"groups":       groups,
		"Auth":         authSetting,
		"MainProvider": auth.HumanName(u.AuthProvider),
		"errors":       []string{},
		"success":      []string{},
		"LoginToken":   token,
	})
}

// /user/me/auth/enable/:module
// enable a given auth module, may just do, or may go to a setup-page
func userAuthModuleEnable(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	module := ctx.Param("module")
	if module == "" {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?error=Module not found")
	}

	str, err := auth.AuthEnable(&user, module)
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?error="+err.Error())
	}

	if str == "" {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?success="+module+" enabled")
	} else { // if str == stage2 it is transferred to the
		// second step enabling is necessary
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me/auth/setup/"+module+"?challenge="+url.QueryEscape(str))
	}
}

// /user/me/auth/setup/:module
// show a setup view, present secrets and a challenge to the user
func userAuthModuleSetupView(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	module := ctx.Param("module")
	if module == "" {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?error=Module not found")
	}

	challenge := ctx.QueryParam("challenge")
	view, challenge := auth.SetupView(user, module, challenge)
	if view == "" {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?error=Module not found")
	}

	return ctx.Render(http.StatusOK, view, echo.Map{
		"navPage":   "profile",
		"user":      user,
		"module":    module,
		"challenge": template.HTML(challenge),
		"errors":    []string{ctx.QueryParam("error")},
		"success":   []string{},
	})
}

// POST /user/me/auth/setup/:module
// take the user input and try to commit enabling the module
func userAuthModuleCommit(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	module := ctx.Param("module")
	if module == "" {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?error="+url.QueryEscape("Module not found"))
	}

	userInput := ctx.FormValue("userinput")
	if userInput == "" {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me/"+module+"?error=Module not found!!")
	}

	err := auth.AuthEnableCommit(&user, module, userInput)
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me/auth/setup/"+module+"?challenge="+url.QueryEscape(err.Error()))
	}

	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?success="+module+" enabled")
}

// /user/me/auth/config/:module
// configure an auth module
func userAuthModuleConfigView(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	module := ctx.Param("module")
	if module == "" {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?error=Module not found")
	}

	view, info := auth.ConfigView(user, module)
	if view == "" {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?error=Module not found")
	}

	return ctx.Render(http.StatusOK, view, echo.Map{
		"navPage": "profile",
		"user":    user,
		"module":  module,
		"info":    info,
		"errors":  []string{ctx.QueryParam("error")},
		"success": []string{},
	})

}

// POST /user/me/auth/config/:module
// take user input and apply the config change
func userAuthModuleConfigCommit(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	module := ctx.Param("module")
	if module == "" {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?error=Module not found")
	}

	params, _ := ctx.FormParams()
	err := auth.ConfigCommit(user, module, params)
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?error="+err.Error())
	}

	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?success="+module+" configured successfully")
}

// /user/me/auth/disable/:module
// disable an auth step
func userAuthModuleDisable(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	module := ctx.Param("module")
	if module == "" {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?error=Module not found")
	}

	err := auth.AuthDisable(&user, module)
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?error="+err.Error())
	}
	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"user/me?success="+module+" disabled")
}

// /user/add
// create a new user
func userAddHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}
	if !user.Superuser {
		return HTTPError(401, ctx)
	}
	return ctx.Render(http.StatusOK, "user_edit", echo.Map{
		"m":       "add",
		"navPage": "user",
		"user":    user,
		"u":       core.User{},
		"errors":  []string{},
		"success": []string{},
	})
}

func SetupAuthProvider(user *core.User) {
	user.AuthProvider = "native"
	user.AuthExtra = "{\"Enable\":false, \"Order\":\"\"}"
}
