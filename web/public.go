// public views and handlers

package web

import (
	"net/http"
	"strconv"
	"time"

	"codeberg.org/snaums/mvoCI/core"

	"github.com/labstack/echo/v4"
)

// /public/zip/:file
func publicZipHandler(ctx echo.Context) error {
	if !core.Cfg.PublicEnable {
		return loginHandler(ctx)
	}
	file := ctx.Param("file")
	var b core.Build
	core.DB.Preload("Repository").Where("zip = ?", file).First(&b)
	if !b.Repository.Public {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"public")
	}
	return ctx.File(core.Cfg.Directory.Build + file)
}

func publicTestRunHandler(ctx echo.Context) error {
	if !core.Cfg.PublicEnable {
		return loginHandler(ctx)
	}

	id, _ := strconv.Atoi(ctx.Param("id"))
	run_id, _ := strconv.Atoi(ctx.Param("run_id"))
	filtermode := ctx.Param("filtermode")
	switch filtermode {
	case core.TestResultOk, core.TestResultError, core.TestResultFail, core.TestResultSkip, core.TestResultUnknown:
	default:
		filtermode = ""
	}

	var r core.Repository
	core.DB.Where("id = ?", id).First(&r)
	if r.ID == 0 || !r.Public {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"public")
	}

	var run core.Build
	if filtermode == "" {
		core.DB.Where("repository_id = ? AND id = ?", id, run_id).Preload("Tests").First(&run)
	} else {
		var t []*core.Test
		core.DB.Where("repository_id = ? AND id =?", id, run_id).First(&run)
		core.DB.Where("build_id = ? AND result = ?", run.ID, filtermode).Find(&t)
		run.Tests = t
	}

	if run.ID == 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"public/"+strconv.Itoa(int(r.ID)))
	}

	return ctx.Render(http.StatusOK, "public_test_run", echo.Map{
		"navPage": "public",
		"repo":    r,
		"run":     run,
		"render_duration": func(d time.Duration) string {
			return d.Truncate(time.Millisecond).String()
		},
		"filtermodes":            []string{core.TestResultOk, core.TestResultSkip, core.TestResultError, core.TestResultFail},
		"filtermode":             filtermode,
		"create_linear_gradient": repoTestCreateGradient,
	})
}

// /public
// render the list of publicly visible repositories
func publicOverviewHandler(ctx echo.Context) error {
	if !core.Cfg.PublicEnable {
		return loginHandler(ctx)
	}
	var r []core.Repository
	core.DB.Where("public = true").Order("name asc").Find(&r)
	return ctx.Render(http.StatusOK, "public_dashboard", echo.Map{
		"navPage":      "public",
		"repositories": r,
	})
}

func publicBuildHandler(ctx echo.Context) error {
	if !core.Cfg.PublicEnable {
		return loginHandler(ctx)
	}
	id, _ := strconv.Atoi(ctx.Param("id"))
	build_id, _ := strconv.Atoi(ctx.Param("build_id"))

	var r core.Repository
	core.DB.Where("id = ?", id).First(&r)
	if r.ID == 0 || !r.Public {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"public")
	}

	var b core.Build
	core.DB.Where("id = ?", build_id).First(&b)
	if b.ID == 0 || r.ID != b.RepositoryID {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"public/"+strconv.Itoa(int(r.ID)))
	}

	var children []core.Build
	core.DB.Where("parent_id = ?", build_id).Find(&children)

	return ctx.Render(http.StatusOK, "public_build_view", echo.Map{
		"navPage":           "public",
		"repo":              r,
		"build":             b,
		"children":          children,
		"render_test_state": repoTestRenderResult,
	})
}

func publicBuildLogHandler(ctx echo.Context) error {
	if !core.Cfg.PublicEnable || !core.Cfg.PublicBuildLog {
		return loginHandler(ctx)
	}
	id, _ := strconv.Atoi(ctx.Param("id"))
	build_id, _ := strconv.Atoi(ctx.Param("build_id"))

	var r core.Repository
	core.DB.First(&r, id)
	if r.ID == 0 || !r.Public {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"public")
	}

	var b core.Build
	core.DB.Select("id, repository_id, log").First(&b, build_id)
	if b.ID == 0 || r.ID != b.RepositoryID {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"public/"+strconv.Itoa(int(r.ID)))
	}

	return ctx.String(http.StatusOK, b.Log)
}

// /public/:id
// render the list of successful builds on a public repository
func publicRepoHandler(ctx echo.Context) error {
	if !core.Cfg.PublicEnable {
		return loginHandler(ctx)
	}
	id, _ := strconv.Atoi(ctx.Param("id"))
	pg, _ := strconv.Atoi(ctx.Param("page"))
	var page = int64(pg)
	if page <= 0 {
		page = 1
	}
	var offset = (int64(page) - 1) * 10
	var r core.Repository
	core.DB.Where("id = ? AND public = true", strconv.Itoa(id)).First(&r)
	if r.ID == 0 || !r.Public {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"public")
	}

	var cnt int64
	core.DB.Model(core.Build{}).Where("repository_id = ? AND zip != ''", r.ID).Count(&cnt)
	var pageMax int64 = (cnt / 10) + 1
	if offset >= cnt {
		offset = (cnt / 10) * 10
		page = (offset / 10) + 1
	}

	cnt = 0
	var b []core.Build
	core.DB.Where("repository_id = ? AND zip != ''", r.ID).Order("finished_at DESC").Offset(int((page - 1) * 10)).Limit(10).Find(&b)

	// TODO pagination must be possible more simple...
	var pageRange []int64
	if pageMax > 9 {
		pageRange = make([]int64, 11)
		pageRange[0] = 1
		pageRange[1] = 2
		pageRange[2] = 3
		if page == 4 || page == 5 {
			pageRange[3] = 4
			pageRange[4] = 5
			pageRange[5] = 6
			pageRange[6] = 7
			pageRange[7] = -1 // => ...
		} else if page == pageMax-4 || page == pageMax-3 {
			pageRange[3] = -1
			pageRange[4] = pageMax - 6
			pageRange[5] = pageMax - 5
			pageRange[6] = pageMax - 4
			pageRange[7] = pageMax - 3
		} else {
			pageRange[3] = -1
			if page < pageMax-4 && page > 5 {
				pageRange[4] = page - 1
				pageRange[5] = page
				pageRange[6] = page + 1
			} else {
				pageRange[4] = pageMax / 2
				pageRange[5] = pageMax/2 + 1
				pageRange[6] = pageMax/2 + 2
			}
			pageRange[7] = -1
		}
		pageRange[8] = pageMax - 2
		pageRange[9] = pageMax - 1
		pageRange[10] = pageMax
	} else {
		pageRange = make([]int64, pageMax)
		var i int64 = 1
		for i <= pageMax {
			pageRange[i-1] = i
			i++
		}
	}

	return ctx.Render(http.StatusOK, "public_repo", echo.Map{
		"navPage":                "public",
		"repo":                   r,
		"builds":                 b,
		"page":                   page,
		"pageMax":                pageMax,
		"pageRange":              pageRange,
		"render_test_state":      repoTestRenderResult,
		"create_linear_gradient": repoTestCreateGradient,
	})
}
