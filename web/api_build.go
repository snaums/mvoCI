// handlers for the /api/v1/build endpoints

package web

import (
	"bufio"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"codeberg.org/snaums/mvoCI/build"
	"codeberg.org/snaums/mvoCI/core"

	"github.com/labstack/echo/v4"
)

// @Summary     List/Search builds of a given repository
// @Description Given a Repository ID, search builds by commit-message, author and hash and order them
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} []core.Build "Expect the following fields to be populated: id, created_at, updated_at, finished_at, started_at, status, commit_sha, branch, zip, repository_id, commit_author, commit_message, commit_url, event"
// @Param       id              query   integer true    "ID of the Repository"
// @Param       name            query   string  false   "Search string into hash, commit-author and commit-message"
// @Param       order_by        query   string  false   "Order the builds by: CreatedAt, StartedAt, FinishedAt, Status, CommitSha, Event, Branch"
// @Param       length          query   integer false   "Limit the number of returned objects"
// @Param       start_at        query   integer false   "Skip the first n objects"
// @Failure     404     {string}    string  The repository has not been found
// @Failure     400     {string}    string  Required parameters are mising
// @Security    securitydefinitions.apikey
// @Tags        build, list
// @Router      /build/list [get]
func apiBuildListHandler(endpoint string, user core.User, ctx echo.Context) error {
	var allowedOrders []string = []string{"created_at", "started_at", "finished_at", "status", "commit_sha", "commit_author", "event", "branch"}
	id, err := apiGetID(ctx)
	if err != nil {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	name, orderby, limit, startAt := apiListHelper(ctx, allowedOrders)

	var r core.Repository
	if user.Superuser {
		core.DB.Model(&core.Repository{}).Select("id, user_id").Where("id = ?", id).First(&r)
	} else {
		core.DB.Model(&core.Repository{}).Select("id, user_id").Where("id = ? AND (public = ? OR user_id = ?)", id, true, user.ID).First(&r)
	}

	if r.ID <= 0 {
		return apiReturn(ctx, http.StatusNotFound, "dashbard", apiError("No such repository"))
	}

	acl := core.AclCheck(user, r)
	if acl < core.AclView {
		return apiReturn(ctx, http.StatusNotFound, "dashbard", apiError("No such repository"))
	}
	var b []core.Build
	if name == "" {
		core.DB.Select("id, created_at, updated_at, finished_at, started_at, status, commit_sha, branch, zip, repository_id, commit_author, commit_message, commit_url, event").Where("repository_id = ?", r.ID).Order(orderby).Offset(startAt).Limit(limit).Find(&b)
	} else {
		core.DB.Select("id, created_at, updated_at, finished_at, started_at, status, commit_sha, branch, zip, repository_id, commit_author, commit_message, commit_url, event").Where("repository_id = ? AND (commit_message LIKE ? OR commit_sha LIKE ? OR commit_author LIKE ?)", r.ID, name, name, name).Order(orderby).Offset(startAt).Limit(limit).Find(&b)
	}
	ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	return ctx.JSON(http.StatusOK, b)
}

func apiTestListHandler(endpoint string, user core.User, ctx echo.Context) error {
	var allowedOrders []string = []string{"duration", "name", "suite_name"}
	id, err := apiGetID(ctx)
	if err != nil {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	name, orderby, limit, startAt := apiListHelper(ctx, allowedOrders)

	var b core.Build
	core.DB.Model(&core.Build{}).Select("id, repository_id").Where("id = ?", id).First(&b)
	if b.ID <= 0 {
		return apiReturn(ctx, http.StatusNotFound, "dashbard", apiError("No such repository"))
	}

	var r core.Repository
	if user.Superuser {
		core.DB.Model(&core.Repository{}).Select("id").Where("id = ?", b.RepositoryID).First(&r)
	} else {
		core.DB.Model(&core.Repository{}).Select("id").Where("id = ? AND (public = ? OR user_id = ?)", b.RepositoryID, true, user.ID).First(&r)
	}

	if r.ID <= 0 {
		return apiReturn(ctx, http.StatusNotFound, "dashbard", apiError("No such repository"))
	}

	acl := core.AclCheck(user, r)
	if acl < core.AclView {
		return apiReturn(ctx, http.StatusNotFound, "dashbard", apiError("No such repository"))
	}

	var t []core.Test
	if name == "" {
		core.DB.Select("id, duration, name, suite_name, result, build_id").Where("build_id = ? ", b.ID).Order(orderby).Offset(startAt).Limit(limit).Find(&t)
	} else {
		core.DB.Select("id, duration, name, suite_name, result, build_id").Where("build_id = ? AND (name LIKE ? OR suite_name LIKE ?)", b.ID, name, name).Order(orderby).Offset(startAt).Limit(limit).Find(&t)
	}
	for k := range t {
		t[k].RepositoryID = r.ID
	}

	ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	return ctx.JSON(http.StatusOK, t)
}

func apiTestInfoHandler(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	var t core.Test
	core.DB.Model(&core.Test{}).Select("id, build_id, log").Where("id = ?", id).First(&t)
	if t.ID <= 0 {
		return apiReturn(ctx, http.StatusNotFound, "dashbard", apiError("No such test"))
	}

	var b core.Build
	core.DB.Model(&core.Build{}).Select("id, repository_id").Where("id = ?", t.BuildID).First(&b)
	if b.ID <= 0 {
		return apiReturn(ctx, http.StatusNotFound, "dashbard", apiError("No such test"))
	}

	var r core.Repository
	if user.Superuser {
		core.DB.Model(&core.Repository{}).Select("id").Where("id = ?", b.RepositoryID).First(&r)
	} else {
		core.DB.Model(&core.Repository{}).Select("id").Where("id = ? AND (public = ? OR user_id = ?)", b.RepositoryID, true, user.ID).First(&r)
	}

	if r.ID <= 0 {
		return apiReturn(ctx, http.StatusNotFound, "dashbard", apiError("No such repository"))
	}

	acl := core.AclCheck(user, r)
	if acl < core.AclView {
		return apiReturn(ctx, http.StatusNotFound, "dashbard", apiError("No such repository"))
	}

	return apiReturn(ctx, http.StatusOK, "dashboard", struct{ Log string }{t.Log})
}

// API: return the log of a specified build
// @Summary     Return the build-log for a given build
// @Description Given a build ID, return the log of that build
// @Accept      mpfd
// @Produce     json
// @Success     200 {json} {"log":"{log}"} "JSON with the only member log"
// @Param       id              query   integer true    "ID of the requested build"
// @Failure     404     {string}    string  "The build has not been found"
// @Failure     403     {string}    string  "You don't have permission to query that build"
// @Failure     400     {string}    string  "Required parameters are mising"
// @Security    securitydefinitions.apikey
// @Tags        build, log
// @Router      /build/log [get]
func apiBuildLogHandler(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil || id <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	var b core.Build
	var r core.Repository
	core.DB.Preload("Repository").Select("id, log, repository_id").Where("id = ?", id).First(&b)
	if b.ID <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Build not found"))
	}

	r = b.Repository
	if r.ID <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Build not found"))
	}

	acl := core.AclCheck(user, r)
	if acl >= core.AclView {
		ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		return ctx.JSON(http.StatusOK, struct{ Log string }{b.Log})
	}

	return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Operation not permitted"))
}

// API: return information about the build like, in a sense everything except the log
// @Summary     Return a build object by it's ID (without log)
// @Description Given a build-ID, return the object as is, omitting the build-log
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} core.Build The build object
// @Param       id              query   integer true    "ID of the requested build"
// @Failure     404     {string}    string  "The build has not been found"
// @Failure     403     {string}    string  "You don't have permission to query that build"
// @Failure     400     {string}    string  "Required parameters are mising"
// @Security    securitydefinitions.apikey
// @Tags        build, query
// @Router      /build [get]
func apiBuildInfoHandler(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil || id <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	var b core.Build
	var r core.Repository
	core.DB.Preload("Repository").Select("id, created_at, updated_at, finished_at, started_at, status, commit_sha, branch, zip, repository_id, commit_author, commit_message, commit_url, event, num_total, num_success, num_failed").Where("id = ?", id).First(&b)
	if b.ID <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Build not found"))
	}

	r = b.Repository
	if r.ID <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Build not found"))
	}

	acl := core.AclCheck(user, r)
	if acl >= core.AclView {
		ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		return ctx.JSON(http.StatusOK, b)
	}
	return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Operation not permitted"))
}

// helper function returning the file path to a build-artifact
func apiBuildArtifactFilePath(user core.User, ctx echo.Context) (int, string, error) {
	id, err := apiGetID(ctx)
	if err != nil || id <= 0 {
		return -1, "badRequest", apiError("Invalid ID")
	}

	var b core.Build
	var r core.Repository
	core.DB.Select("zip, id, repository_id").Where("id = ?", id).First(&b)
	core.DB.Select("id", "user_id").Where("id = ?", b.RepositoryID).First(&r)
	acl := core.AclCheck(user, r)
	if acl < core.AclView {
		return -1, "forbidden", apiError("Operation not permitted")
	}

	if b.Zip == "" {
		return id, "", nil
	}

	return id, core.Cfg.Directory.Build + b.Zip, nil
}

// API: return metadata about an artifact to a build
// @Summary     Return file metadata about a build artifact
// @Description Given a build-ID, return file metadata (size, name, modified-date) about an artifact archive
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} object struct{Name,Size,ModTime}
// @Param       id              query   integer true    "ID of the requested build"
// @Failure     404     {string}    string  "The build has not been found"
// @Failure     403     {string}    string  "You don't have permission to query that build"
// @Security    securitydefinitions.apikey
// @Tags        build, artifact, query
// @Router      /build/artifact/info      [get]
func apiBuildArtifactInfo(endpoint string, user core.User, ctx echo.Context) error {
	id, fp, err := apiBuildArtifactFilePath(user, ctx)
	if err != nil {
		status := http.StatusBadRequest
		if fp == "forbidden" {
			status = http.StatusBadRequest
		}
		return apiReturn(ctx, status, "dashboard", err)
	}

	type mvoFileInfo struct {
		Name    string
		Size    int64
		ModTime time.Time
	}

	if fp == "" {
		return apiReturn(ctx, http.StatusNoContent, "build/"+strconv.Itoa(id), apiSuccess("build/artifact", "No artifact is present to the build"))
	}

	fi, err := os.Stat(fp)
	if err != nil {
		return apiReturn(ctx, http.StatusInternalServerError, "build/"+strconv.Itoa(id), apiError("stat failed"))
	}

	mvofi := mvoFileInfo{
		Name:    fi.Name(),
		Size:    fi.Size(),
		ModTime: fi.ModTime(),
	}

	return apiReturn(ctx, http.StatusOK, "build/"+strconv.Itoa(id), mvofi)
}

// API: download the artifact file to a build
// @Summary     Download an artifact archive to a build
// @Description Give the build-ID download the artifact archive
// @Accept      mpfd
// @Produce     json, application/x-bzip, application/x-gzip, application/x-compressed
// @Success     200 {object} object artifact file
// @Param       id              query   integer true    "ID of the requested build"
// @Failure     404     {string}    string  "The build has not been found"
// @Failure     403     {string}    string  "You don't have permission to query that build"
// @Security    securitydefinitions.apikey
// @Tags        build, artifact, get
// @Router      /build/artifact      [get]
func apiBuildArtifact(endpoint string, user core.User, ctx echo.Context) error {
	id, fp, err := apiBuildArtifactFilePath(user, ctx)
	if err != nil {
		status := http.StatusBadRequest
		if fp == "forbidden" {
			status = http.StatusBadRequest
		}
		return apiReturn(ctx, status, "dashboard", err)
	}

	if fp == "" {
		return apiReturn(ctx, http.StatusNoContent, "build/"+strconv.Itoa(id), apiSuccess("build/artifact", "No artifact is present to the build"))
	}

	return ctx.File(fp)
}

// API: delete the artifact archive file to a build
// @Summary     Delete the artifact archive of a build
// @Description Given a build-ID remove the artifact archive to that Build
// @Accept      mpfd
// @Produce     json
// @Success     200
// @Param       id              query   integer true    "ID of the requested build"
// @Failure     404     {string}    string  "The build has not been found"
// @Failure     403     {string}    string  "You don't have permission to query that build"
// @Failure     204     {string}    string  "There is no artifact archive associated with that build. Check if the build is still ongoing"
// @Failure     500     {string}    string  "Unable to delete the artifact file"
// @Security    securitydefinitions.apikey
// @Tags        build, artifact, delete
// @Router      /build/artifact/delete   [delete]
func apiBuildArtifactDelete(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil || id <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	var b core.Build
	var r core.Repository
	core.DB.Select("zip, id, repository_id").Where("id = ?", id).First(&b)
	core.DB.Select("id", "user_id").Where("id = ?", b.RepositoryID).First(&r)
	acl := core.AclCheck(user, r)
	if acl < core.AclDelete {
		return apiReturn(ctx, http.StatusBadRequest, "build/"+strconv.Itoa(id), apiError("Operation not permitted"))
	}

	if b.Zip == "" {
		return apiReturn(ctx, http.StatusNoContent, "build/"+strconv.Itoa(id), apiSuccess("build/artifact", "No artifact is present to the build"))
	}

	core.DB.Model(&core.Build{}).Where("id=?", b.ID).Update("zip", "")
	fp := core.Cfg.Directory.Build + b.Zip
	err = os.Remove(fp)
	if err != nil {
		// something went wrong, try to stat the file
		fi, err2 := os.Stat(fp)
		if err2 != nil {
			// the file is not present -> removing was unnecessary; just say success
			// fall through to success return
		} else {
			if fi.Size() > 0 {
				// the file is still present -> reinstate the DB entry and return an error
				core.DB.Model(&core.Build{}).Where("id=?", b.ID).Update("zip", b.Zip)
				return apiReturn(ctx, http.StatusInternalServerError, "build"+strconv.Itoa(id), apiError("Artifact could not be deleted"))
			} else {
				// the file is present but size 0 ? weird; state success; This should never occur
				// fall through to the success return
				core.Console.Warn("File is present, but size is 0 Byte: ", b.Zip)
			}
		}
	}

	return apiReturn(ctx, http.StatusInternalServerError, "build"+strconv.Itoa(id), apiSuccess("build/artifact/delete", "artifact delete successful"))
}

// API: delete a build
// @Summary     Delete a build
// @Description Given a build-ID delete that build
// @Accept      mpfd
// @Produce     json
// @Success     200
// @Param       id              query   integer true    "ID of the requested build"
// @Failure     404     {string}    string  "The build has not been found"
// @Failure     403     {string}    string  "You don't have permission to query that build"
// @Security    securitydefinitions.apikey
// @Tags        build, delete
// @Router      /build/delete [delete]
func apiBuildDelete(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil || id <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	var b core.Build
	var r core.Repository
	core.DB.Select("id, status, repository_id").Where("id = ?", id).First(&b)
	core.DB.Select("id", "user_id").Where("id = ?", b.RepositoryID).First(&r)
	acl := core.AclCheck(user, r)
	if acl < core.AclDelete {
		return apiReturn(ctx, http.StatusBadRequest, "build/"+strconv.Itoa(id), apiError("Operation not permitted"))
	}

	if b.Status != "started" {
		rid := buildDelete(uint(id))
		return apiReturn(ctx, http.StatusOK, "repo/view/"+strconv.FormatUint(uint64(rid), 10), "Build deleted")
	} else {
		return apiReturn(ctx, http.StatusOK, "build/"+strconv.FormatUint(uint64(b.ID), 10), "Cannot delete build")
	}
}

// API: rebuild a build
// @Summary     Rebuild a build
// @Description Given a build-ID, rebuild that build
// @Accept      mpfd
// @Produce     json
// @Success     200     {object} object  id,message ; id of the newly created build and a message stating that the build has been triggered
// @Param       id              query   integer true    "ID of the requested build"
// @Failure     404     {string}    string  "The build has not been found"
// @Failure     403     {string}    string  "You don't have permission to execute that build"
// @Security    securitydefinitions.apikey
// @Tags        build, rebuild
// @Router      /build/rebuild      [post]
func apiBuildRebuild(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil || id <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	var b core.Build
	var r core.Repository
	core.DB.Where("id = ?", id).First(&b)
	core.DB.Select("id", "user_id").Where("id = ?", b.RepositoryID).First(&r)
	acl := core.AclCheck(user, r)
	if acl < core.AclExecute {
		return apiReturn(ctx, http.StatusBadRequest, "build/"+strconv.Itoa(id), apiError("Operation not permitted"))
	}

	bid := build.ApiReBuild(&b)
	return apiReturn(ctx, http.StatusOK, "build/"+strconv.FormatUint(uint64(bid), 10), struct {
		id      uint
		message string
	}{id: bid,
		message: "rebuild triggered",
	})
}

func buildVariantPostHandler(ctx echo.Context, r core.Repository, variant *core.BuildVariant) error {
	name, err := apiGetParam(ctx, "name")
	if err != nil || name == "" {
		return err
	}
	description, err := apiGetParam(ctx, "description")
	if err != nil {
		return err
	}
	variables, err := apiGetParam(ctx, "variables")
	if err != nil || variables == "" {
		return err
	}

	variant.Name = name
	variant.Description = description

	variant.Variables = []core.BuildVariantVariable{}
	scanner := bufio.NewScanner(strings.NewReader(variables))
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		l, _ := strings.CutPrefix(line, "export")
		l = strings.TrimSpace(l)
		if l == "" || l[0] == '#' {
			continue
		}

		fmt.Println(" input ", l)
		n := strings.SplitN(l, "=", 2)
		fmt.Printf(" %v", n)
		if len(n) != 2 {
			return errors.New("Malformed variables")
		}
		key := strings.TrimSpace(n[0])
		value := strings.TrimSpace(n[1])
		variant.Variables = append(variant.Variables, core.BuildVariantVariable{Key: key, Value: value})
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}

func apiBuildVariantCreate(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil || id <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	var r core.Repository
	core.DB.First(&r, id)

	acl := core.AclCheck(user, r)
	if acl < core.AclEdit {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/"+strconv.Itoa(id)+"?error=Permission Error")
	}

	var variant core.BuildVariant
	variant.RepositoryID = r.ID
	err = buildVariantPostHandler(ctx, r, &variant)
	if err != nil {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id?error="+err.Error()))
	}

	core.DB.Create(&variant)
	return apiReturn(ctx, http.StatusOK, "repo/variants/"+strconv.Itoa(int(variant.RepositoryID)), apiSuccess("build_variant/modify", "modified"))
}

func apiBuildVariantDelete(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil || id <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	var v core.BuildVariant
	core.DB.Joins("Repository").First(&v, id)
	if v.ID == 0 || v.RepositoryID == 0 || v.Repository == nil {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	acl := core.AclCheck(user, *v.Repository)
	if acl < core.AclEdit {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/"+strconv.Itoa(id)+"?error=Permission Error")
	}

	core.DB.Where("build_variant_id = ?", v.ID).Delete(&core.BuildVariantVariable{})
	core.DB.Delete(&v)
	return apiReturn(ctx, http.StatusOK, "repo/variants/"+strconv.Itoa(int(v.RepositoryID)), apiSuccess("build_variant/delete", "modified"))
}

func apiBuildVariantModify(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil || id <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	var v core.BuildVariant
	core.DB.Preload("Variables").Joins("Repository").First(&v, id)
	if v.ID == 0 || v.RepositoryID == 0 || v.Repository == nil {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	acl := core.AclCheck(user, *v.Repository)
	if acl < core.AclEdit {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/"+strconv.Itoa(id)+"?error=Permission Error")
	}

	core.DB.Delete(&v.Variables)
	v.Variables = []core.BuildVariantVariable{}
	err = buildVariantPostHandler(ctx, *v.Repository, &v)
	if err != nil {
		core.DB.Save(&v)
	}

	return apiReturn(ctx, http.StatusOK, "repo/variants/"+strconv.Itoa(int(v.RepositoryID)), apiSuccess("build_variant/modify", "modified"))
}

func apiBuildVariantBuildNow(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil || id <= 0 {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	var v core.BuildVariant
	core.DB.Preload("Variables").Joins("Repository").First(&v, id)
	if v.ID == 0 || v.RepositoryID == 0 || v.Repository == nil {
		return apiReturn(ctx, http.StatusBadRequest, "dashbard", apiError("Invalid Id"))
	}

	acl := core.AclCheck(user, *v.Repository)
	if acl < core.AclExecute {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/"+strconv.Itoa(id)+"?error=Permission Error")
	}

	build_id := build.BuildNowVariant(v, *v.Repository)
	if build_id > 0 {
		return apiReturn(ctx, http.StatusFound, core.Cfg.HttpSubUrl+"build/"+strconv.Itoa(int(build_id)), apiSuccess("variant/build", "successful"))
	}
	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/"+strconv.Itoa(id)+"?error=Permission Error")
}
