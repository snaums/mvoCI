// handlers and functions for the webhook

package web

import (
	"bytes"
	"io"
	"strconv"
	"strings"

	//"regexp"
	"crypto/hmac"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/hex"
	"net/http"
	"net/http/httputil"
	neturl "net/url"

	"codeberg.org/snaums/mvoCI/build"
	"codeberg.org/snaums/mvoCI/core"
	. "codeberg.org/snaums/mvoCI/hook"

	//"golang.org/x/crypto/bcrypt"
	//"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	//"github.com/labstack/echo/middleware"
)

// type for a generic hmac function
type hmacFunc func([]byte, []byte) string

// check the secret based on hmac function method
func SecretCheck(data io.Reader, secret string, theirs string, method string) bool {
	var fn hmacFunc
	switch method {
	case "sha1":
		fn = hmacSha1
	case "sha256":
		fn = hmacSha256
	default:
		core.Console.Log("BUG: method not found")
		return false
	}

	var buff []byte
	buff, err := io.ReadAll(data)
	if err == nil {
		sha := fn(buff, []byte(secret))
		if sha == theirs {
			return true
		}
	}
	return false
}

// hmac with sha1
func hmacSha1(data []byte, secret []byte) string {
	h := hmac.New(sha1.New, []byte(secret))
	h.Write([]byte(data))
	return hex.EncodeToString(h.Sum(nil))
}

// hmac with sha256
func hmacSha256(data []byte, secret []byte) string {
	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(data))
	return hex.EncodeToString(h.Sum(nil))
}

// return the branch from git-ref
func branchFromRef(ref string) string {
	ref = strings.TrimSpace(ref)
	if strings.HasPrefix(ref, "refs/heads/") && len(ref) > 11 {
		return ref[11:]
	}
	var tmp = strings.Split(ref, "/")
	return tmp[len(tmp)-1]
}

// /repo/push/:id/:api
func webhookRepo(ctx echo.Context) error {
	var api = ctx.Param("api")
	var id = ctx.Param("id")
	i, err := strconv.Atoi(id)

	if err != nil {
		return ctx.String(404, "Invalid Parameter")
	}

	return webhookStage2(ctx, api, i)
}

// internal webhook stage 2
// set the id to -1 if you want to "unset" it
func webhookStage2(ctx echo.Context, api string, id int) error {
	var err error
	var tmp []byte
	var HookLog = core.WebHookLog{}
	// skip this costly operation if not necessary
	if core.Cfg.WebHookLog {
		tmp, _ = httputil.DumpRequest(ctx.Request(), true)
	}
	body := ctx.Request().Body

	var bodyBuff bytes.Buffer
	rd := io.TeeReader(body, &bodyBuff)

	var bID uint
	// body for an error message
	var ResponseBody = "Did not work :("
	// status for the error case
	var ResponseStatus = 500

	var event string

	HookLog.Request = string(tmp)
	HookLog.API = api
	HookLog.Status = "Failure"

	var author string
	var message string
	var url string

	args := make(map[string]string)
	urlargs := ctx.QueryParams()
	for k, v := range urlargs {
		if len(v) == 1 {
			args[k] = v[0]
		}
	}

	r := core.Repository{}
	if id > 0 {
		core.DB.Where("id = ?", id).First(&r)
		if r.ID != uint(id) {
			r = core.Repository{}
		}
	}

	switch api {
	case "gogs":
		event = ctx.Request().Header["X-Github-Event"][0]
		if event == "push" {
			var pl GogsPayload
			err = core.GenericJSONDecode(rd, &pl)
			if err != nil {
				break
			}
			if r.ID <= 0 {
				core.DB.Where("clone_url LIKE ? OR clone_url LIKE ?", pl.Repository.Clone_Url, pl.Repository.Ssh_Url).First(&r)
			}
			if r.ID > 0 && r.WebHookEnable {
				HookLog.Repository = r
				sig := ctx.Request().Header["X-Gogs-Signature"][0]
				if SecretCheck(&bodyBuff, r.Secret, sig, "sha256") {
					author = pl.Pusher.Full_Name
					if len(author) < 1 {
						author = pl.Pusher.Login
					}

					message = pl.Commits[0].Message
					url = pl.Commits[0].Url

					bID = build.BuildSpecificOrNightly2(r, branchFromRef(pl.Ref), pl.After, author, url, message, event, args)
				} else {
					ResponseBody = "Signature check failed"
				}
			} else {
				bID = 0
				ResponseBody = "Repo not found"
				ResponseStatus = 404
			}
		} else {
			ResponseBody = "Listening to 'push', got '" + event + "'"
		}
	case "forgejo":
		fallthrough
	case "gitea":
		event = ctx.Request().Header["X-Gitea-Event"][0]
		if event == "push" || event == "release" {
			var pl GiteaPayload
			err = core.GenericJSONDecode(rd, &pl)
			if err != nil {
				break
			}
			//bID = build.BuildHookedFromSecret (pl.Secret, branchFromRef ( pl.Ref ), pl.After )
			if r.ID <= 0 {
				core.DB.Where("clone_url LIKE ? OR clone_url LIKE ?", pl.Repository.Clone_Url, pl.Repository.Ssh_Url).First(&r)
			}
			if r.ID > 0 && r.WebHookEnable {
				HookLog.Repository = r
				sig := ctx.Request().Header["X-Gitea-Signature"][0]
				if SecretCheck(&bodyBuff, r.Secret, sig, "sha256") {
					if event == "push" {
						author = pl.Pusher.Full_Name
						if len(author) < 1 {
							author = pl.Pusher.Login
						}

						if len(pl.Commits) > 0 {
							message = pl.Commits[0].Message
							url = pl.Commits[0].Url

							bID = build.BuildSpecificOrNightly2(r, branchFromRef(pl.Ref), pl.After, author, url, message, event, args)
						}
					} else if event == "release" {
						author = pl.Release.Author.Full_Name
						if len(author) < 1 {
							author = pl.Release.Author.Login
						}

						message = "Release: " + pl.Release.TagName + " - " + pl.Release.Name
						url = pl.Release.HtmlUrl
						apiUrl := pl.Release.Url

						bID = build.BuildRelease(r, pl.Release.TargetCommitish, pl.Release.TagName, author, url, message, event, api, apiUrl, args)
					}
				} else {
					ResponseBody = "Signature check failed"
				}
			} else {
				bID = 0
				ResponseBody = "Repo not found"
				ResponseStatus = 404
			}
		} else {
			ResponseBody = "Listening to 'push', got '" + event + "'"
		}
	case "gitbucket":
		event = ctx.Request().Header["X-Github-Event"][0]
		if event == "push" {
			var escaped string
			var e []byte

			e, err = io.ReadAll(rd)
			if err != nil {
				ResponseBody = "Invalid gitbucket request"
				ResponseStatus = 500
			}

			escaped = string(e)
			escaped, err = neturl.QueryUnescape(escaped[8:])
			if err != nil {
				ResponseBody = "Invalid gitbucket request"
				ResponseStatus = 500
			}

			rd = strings.NewReader(escaped)
			var pl GitBucketPayload
			err = core.GenericJSONDecode(rd, &pl)
			if err != nil {
				break
			}

			if r.ID <= 0 {
				core.DB.Where("clone_url LIKE ? OR clone_url LIKE ?", pl.Repository.Clone_Url, pl.Repository.Http_Url).First(&r)
			}
			if r.ID > 0 && r.WebHookEnable {
				HookLog.Repository = r
				sig := strings.Split(ctx.Request().Header["X-Hub-Signature"][0], "=")
				if SecretCheck(&bodyBuff, r.Secret, sig[1], sig[0]) {
					if len(pl.Commits) > 0 {
						message = pl.Commits[0].Message
						author = pl.Commits[0].Author.Name
						url = pl.Commits[0].Html_Url
					}

					bID = build.BuildSpecificOrNightly2(r, branchFromRef(pl.Ref), pl.After, author, url, message, event, args)
				} else {
					ResponseBody = "Signature check failed"
				}
			} else {
				bID = 0
				ResponseBody = "Repo not found"
				ResponseStatus = 404
			}
		} else {
			ResponseBody = "Listening to 'push', got '" + event + "'"
		}
	case "bitbucket":
		event = ctx.Request().Header["X-Event-Key"][0]
		if event == "diagnostics:ping" {
			ResponseBody = "Ok"
			ResponseStatus = 200
		} else if event == "repo:refs_changed" {
			var pl BitBucketPayload
			err = core.GenericJSONDecode(rd, &pl)
			if err != nil {
				break
			}

			var links []string
			for _, l := range pl.Repository.Links.Clone {
				links = append(links, l.Href)
			}

			if r.ID <= 0 {
				core.DB.Where("clone_url IN (?)", links).First(&r)
			}
			if r.ID > 0 && r.WebHookEnable {
				HookLog.Repository = r
				sig := strings.Split(ctx.Request().Header["X-Hub-Signature"][0], "=")
				if SecretCheck(&bodyBuff, r.Secret, sig[1], sig[0]) {
					author = pl.Actor.DisplayName
					if author == "" {
						author = pl.Actor.Name
					}

					var toHash, ref string
					if len(pl.Changes) > 0 {
						ref = pl.Changes[0].RefId
						toHash = pl.Changes[0].ToHash
					}

					bID = build.BuildSpecificOrNightly2(r, branchFromRef(ref), toHash, author, url, message, event, args)
				} else {
					ResponseBody = "Signature check failed"
				}
			} else {
				bID = 0
				ResponseBody = "Repo not found"
				ResponseStatus = 404
			}
		} else {
			ResponseBody = "Listening to 'repo:refs_changed', got '" + event + "'"
		}
	case "github":
		event = ctx.Request().Header["X-Github-Event"][0]
		if event == "push" {
			var pl GithubPayload
			err = core.GenericJSONDecode(rd, &pl)
			if err != nil {
				break
			}
			if r.ID <= 0 {
				core.DB.Where("clone_url LIKE ? OR clone_url LIKE ? OR clone_url LIKE ?", pl.Repository.Clone_Url, pl.Repository.Git_Url, pl.Repository.Ssh_Url).First(&r)
			}
			if r.ID > 0 && r.WebHookEnable {
				HookLog.Repository = r
				sig := strings.Split(ctx.Request().Header["X-Hub-Signature"][0], "=")
				if SecretCheck(&bodyBuff, r.Secret, sig[1], sig[0]) {
					bID = build.BuildSpecificOrNightly2(r, branchFromRef(pl.Ref), pl.After, author, url, message, event, args)
				} else {
					ResponseBody = "Signature check failed"
				}
			} else {
				bID = 0
				ResponseBody = "Repo not found"
				ResponseStatus = 404
			}
		} else {
			ResponseBody = "Listening to 'push', got '" + event + "'"
		}
	case "gitlab":
		event = ctx.Request().Header["X-Gitlab-Event"][0]
		if event == "Push Hook" {
			var pl GitlabPayload
			err = core.GenericJSONDecode(rd, &pl)
			if err != nil {
				break
			}
			//bID = build.BuildHookedFromSecret (pl.Secret, branchFromRef ( pl.Ref ), pl.After )
			if r.ID <= 0 {
				core.DB.Where("clone_url LIKE ? OR clone_url LIKE ? or clone_url LIKE ? or clone_url LIKE ?", pl.Repository.GitSshUrl, pl.Repository.GitHttpUrl, pl.Project.GitHttpUrl, pl.Project.GitSshUrl).First(&r)
			}
			if r.ID > 0 {
				HookLog.Repository = r
				sig := ctx.Request().Header["X-Gitlab-Token"][0]
				if sig == r.Secret && r.WebHookEnable {
					author = pl.UserName
					if len(author) < 1 {
						author = pl.UserUsername
					}

					message = pl.Message
					if len(pl.Commits) > 0 {
						message = pl.Commits[0].Message
						url = pl.Commits[0].Url
					}

					bID = build.BuildSpecificOrNightly2(r, branchFromRef(pl.Ref), pl.After, author, url, message, event, args)
				} else {
					ResponseBody = "Signature check failed"
				}
			} else {
				bID = 0
				ResponseBody = "Repo not found"
				ResponseStatus = 404
			}
		} else {
			ResponseBody = "Listening to 'Push Hook', got '" + event + "'"
		}
	default:
		ResponseBody = "API not implemented"
		ResponseStatus = 404
		// woopsie!
	}

	if bID > 0 {
		HookLog.Status = "Success"
		HookLog.BuildID = bID

		ResponseStatus = http.StatusOK
		ResponseBody = "Enqueued, build #" + strconv.Itoa(int(bID))
	}

	HookLog.ResponseBody = ResponseBody
	HookLog.ResponseStatus = ResponseStatus
	if core.Cfg.WebHookLog {
		core.DB.Save(&HookLog)
	}
	return ctx.String(ResponseStatus, ResponseBody)
}

// /push/hook/:api
// Called by the code versioning software, when a commit or release,... is made
// to notify mvoCI to rebuild that repo.
// TODO loads of code is copied here, can that be done better?
func webhook(ctx echo.Context) error {
	var api = ctx.Param("api")
	return webhookStage2(ctx, api, -1)
}
