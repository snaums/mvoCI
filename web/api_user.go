// handlers for /api/v1/user

package web

import (
	"errors"
	"net/http"
	"regexp"
	"strconv"

	"codeberg.org/snaums/mvoCI/auth"
	"codeberg.org/snaums/mvoCI/core"

	//"golang.org/x/crypto/bcrypt"
	//"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	//"github.com/labstack/echo/middleware"
)

// simplified user object used instead of core.User
type apiUser struct {
	ID        uint   // ID of the user object for use in API calls
	Name      string // username of the user
	Email     string // email-address of the user
	Superuser bool   // flag whether the user is a superuser
}

func apiUserCreateList(users []core.User) []apiUser {
	var result []apiUser
	for _, u := range users {
		result = append(result, apiUser{Name: u.Name, ID: u.ID, Email: u.Email, Superuser: u.Superuser})
	}
	return result
}

// API: returns a list of user accounts based on search criteria
// @Summary     List/Search user accounts
// @Description returns a list of user based on search criteria and orders
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} []core.User "Expect the following fields to be populated: name, id, email and superuser"
// @Param       name            query   string  false   "Search string into usernames"
// @Param       order_by        query   string  false   "Order the user objects by: CreatedAt, Name, Email"
// @Param       length          query   integer false   "Limit the number of returned objects"
// @Param       start_at        query   integer false   "Skip the first n objects"
// @Security    securitydefinitions.apikey
// @Tags        user, list
// @Router      /user/list [get]
func apiUserList(endpoint string, user core.User, ctx echo.Context) error {
	var allowedOrders = []string{"created_at", "name", "email"}
	name, orderby, limit, startAt := apiListHelper(ctx, allowedOrders)
	var ul []core.User
	if name == "" {
		if user.Superuser {
			// TODO add filter for private users only a superuser can see
			core.DB.Model(&core.User{}).Select("name", "id", "email", "superuser").Order(orderby).Offset(startAt).Limit(limit).Find(&ul)

		} else {
			core.DB.Model(&core.User{}).Select("name", "id", "email", "superuser").Order(orderby).Offset(startAt).Limit(limit).Find(&ul)
		}
	} else {
		if user.Superuser {
			// TODO add filter for private users only a superuser can see
			core.DB.Model(&core.User{}).Select("name", "id", "email", "superuser").Where("name LIKE ?", name).Order(orderby).Offset(startAt).Limit(limit).Find(&ul)

		} else {
			core.DB.Model(&core.User{}).Select("name", "id", "email", "superuser").Where("name LIKE ?", name).Order(orderby).Offset(startAt).Limit(limit).Find(&ul)
		}
	}

	apiusers := apiUserCreateList(ul)
	return apiReturn(ctx, http.StatusOK, "", apiusers)
}

// API: delete a user (only if you are superuser)
// @Summary     Delete a user account
// @Description Delete a user account
// @Accept      mpfd
// @Produce     json
// @Success     200 {json} apiSuccessObj "user/delete: User deleted"
// @Param       id            query   integer  true   "ID of the user to be deleted"
// @Failure     400     {string} string "The user-ID parameter is missing"
// @Failure     400     {string} string "You've tried to delete yourself"
// @Failure     403     {string} string "You must be superuser to delete other users"
// @Security    securitydefinitions.apikey
// @Tags        user, delete
// @Router      /user/delete [delete]
func apiUserDelete(endpoint string, user core.User, ctx echo.Context) error {
	if user.Superuser {
		id, err := apiGetID(ctx)
		if err != nil {
			return apiReturn(ctx, http.StatusBadRequest, "user", apiError("ID parameter missing"))
		}

		if uint(id) == user.ID {
			return apiReturn(ctx, http.StatusBadRequest, "user/me", apiError("You can't delete yourself"))
		}

		core.DB.Delete(&core.User{}, id).Where("id = ?", id)
		return apiReturn(ctx, http.StatusOK, "user", apiSuccess("user/delete", "User deleted"))
	} else {
		return apiReturn(ctx, http.StatusForbidden, "user", apiError("Only a Superuser can delete other users"))
	}
}

// API: create a new user account (only if you are superuser)
// @Summary     Create a new user account (only if you are superuser)
// @Description Create a new user account
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} apiUser "newly created user"
// @Param       name            query   string   true   "username"
// @Param       email           query   integer  true   "email address"
// @Param       password        query   integer  true   "password"
// @Param       password2       query   integer  true   "password repeated"
// @Param       superuser       query   bool     true   "checkbox for superuser flag (allows true, 1, or on for true, otherwise interpreted as false"
// @Failure     400     {string} string "A parameter is wrong or missing, see returned error"
// @Failure     403     {string} string "You must be superuser to create other users"
// @Security    securitydefinitions.apikey
// @Tags        user, create
// @Router      /user/create [post]
func apiUserCreate(endpoint string, user core.User, ctx echo.Context) error {
	if user.Superuser {
		u, err := apiUserChecker(ctx, false, user)
		if err != nil {
			return apiReturn(ctx, http.StatusBadRequest, "user/add", apiError(err.Error()))
		}
		return apiReturn(ctx, http.StatusOK, "user/edit/"+strconv.FormatUint(uint64(u.ID), 10), u)
	} else {
		return apiReturn(ctx, http.StatusForbidden, "user", apiError("Only a Superuser can create new users"))
	}
}

// API: modify a user account account (only if you are superuser, or modify yourself)
// @Summary     Modify a user account
// @Description Modify a user account (if you are superuser or modify yourself)
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} apiUser "modified user object"
// @Param       name            query   string   true   "username"
// @Param       email           query   integer  true   "email address"
// @Param       password        query   integer  true   "password"
// @Param       password2       query   integer  true   "password repeated"
// @Param       superuser       query   bool     true   "checkbox for superuser flag (allows true, 1, or on for true, otherwise interpreted as false"
// @Failure     400     {string} string "A parameter is wrong or missing, see returned error"
// @Failure     403     {string} string "You must be superuser to modify other users"
// @Security    securitydefinitions.apikey
// @Tags        user, modify
// @Router      /user/modify [post]
func apiUserModify(endpoint string, user core.User, ctx echo.Context) error {
	u, err := apiUserChecker(ctx, true, user)
	if err != nil {
		if err.Error() == "Permission error" {
			return apiReturn(ctx, http.StatusForbidden, "user", apiError(err.Error()))
		}
		if u.ID == user.ID {
			return apiReturn(ctx, http.StatusBadRequest, "user/me", apiError(err.Error()))
		}
		return apiReturn(ctx, http.StatusBadRequest, "user/edit/"+strconv.FormatUint(uint64(u.ID), 10), apiError(err.Error()))
	}
	return apiReturn(ctx, http.StatusOK, "user/edit/"+strconv.FormatUint(uint64(u.ID), 10), u)
}

// API: return information about a given user
// @Summary     Return a user by ID
// @Description Given a User-ID, return the user object, with the following fields: id, created_at, name, email, superuser
// @Accept      mpfd
// @Produce     json
// @Success     200 {object} core.User  "The requested user"
// @Param       id        query   integer true   "The user-ID to be queried"
// @Failure     404     {string} string "The requested user was not found"
// @Failure     400     {string} string "The user-ID parameter is missing"
// @Security    securitydefinitions.apikey
// @Tags        user, query
// @Router      /user [get]
func apiUserInfoHandler(endpoint string, user core.User, ctx echo.Context) error {
	id, err := apiGetID(ctx)
	if err != nil || id <= 0 {
		return HTTPError(http.StatusBadRequest, ctx)
	}

	var u core.User
	core.DB.Select("id, created_at, name, email, superuser").Where("id = ?", id).First(&u)
	if u.ID <= 0 {
		return HTTPError(http.StatusNotFound, ctx)
	}
	ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	return ctx.JSON(http.StatusOK, u)
}

const _emailRegex_ string = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"

// check inputs on add or edit views and commit the changed data to database
func apiUserChecker(ctx echo.Context, edit bool, user core.User) (apiUser, error) {
	var id int
	var u core.User
	var us apiUser
	var err error

	if edit {
		// check if the user exists
		id, err = apiGetID(ctx)
		if err != nil {
			return us, errors.New("Invalid ID")
		}

		if !user.Superuser && uint(id) != user.ID {
			return us, errors.New("Permission error")
		}

		core.DB.Where("ID = ?", id).First(&u)
		if u.ID == 0 {
			return us, errors.New("User not found")
		}
		us.ID = uint(id)
	} else {
		if !user.Superuser {
			return us, errors.New("Permission error")
		}
	}

	// get and check inputs
	name, _ := apiGetParam(ctx, "name")
	email, _ := apiGetParam(ctx, "email")
	pass1, _ := apiGetParam(ctx, "password")
	pass2, _ := apiGetParam(ctx, "password2")
	superuser, _ := apiGetCheckBox(ctx, "superuser")

	if pass1 != pass2 {
		return us, errors.New("Passwords were not identical")
	}
	if len(email) <= 0 || ((len(name) <= 0 || len(pass1) <= 0) && !edit) {
		return us, errors.New("Did you input anything?")
	}

	if !edit {
		// if add: check if the username is still free
		var cnt int64 = 0
		core.DB.Model(&core.User{}).Where("name = ?", name).Count(&cnt)
		if cnt > 0 {
			return us, errors.New("Username already taken")
		}
		u = core.User{}
		u.Name = name
	}

	// check email
	re := regexp.MustCompile(_emailRegex_)
	if !re.MatchString(email) {
		return us, errors.New("You have to input an email-address.")
	}

	// start filling the user-struct
	u.Email = email
	if superuser {
		if !edit || user.Superuser {
			// only superuser can add other users
			u.Superuser = true
		}
	} else {
		u.Superuser = false
	}

	if len(pass1) > 0 {
		err := auth.AuthSetSecret(&u, "native", pass1, pass2)
		if err != nil {
			return us, err
		}
	}

	if edit {
		// Save the modified user
		core.DB.Save(&u)
	} else {
		SetupAuthProvider(&u)

		// Store the new user and add them to Everything
		core.DB.Create(&u)
		_ = core.CreateGroupEveryone(u)
	}

	ur := apiUserCreateList([]core.User{u})
	return ur[0], nil
}
