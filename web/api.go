// handlers for /api

package web

import (
	"errors"
	"strconv"

	//"regexp"
	"net/http"
	"strings"

	"codeberg.org/snaums/mvoCI/core"

	"github.com/labstack/echo/v4"
)

// type for a handler-function for API-calls.
type apiHandlerFn func(endpoint string, user core.User, ctx echo.Context) error

// actions represent fine grain handlers, e.g. edit, list,...
type apiActionT struct {
	Fn     apiHandlerFn // pointer to a handler function
	Method string       // the allowed method (POST, GET, PUT, ...)
}

// The handler type represents a broader range of handler, like a heading in a text.
// May or may not be callable itself
type apiHandlerT struct {
	Descriptor apiActionT            // descriptor and head-level action
	Actions    map[string]apiActionT // a list of sub-actions
}

// internal routining table for API-Calls
var apiRoutingTable = map[string]apiHandlerT{
	"repo": {
		Actions: map[string]apiActionT{
			"detail": {
				Fn:     apiRepoDetailHandler,
				Method: "GET",
			},
			"list": {
				Fn:     apiRepoListHandler,
				Method: "GET",
			},
			"build": {
				Fn:     apiRepoBuildHandler,
				Method: "GET",
			},
			"create": {
				Fn:     apiRepoCreate,
				Method: "POST",
			},
			"modify": {
				Fn:     apiRepoModify,
				Method: "POST",
			},
			"delete": {
				Fn:     apiRepoDelete,
				Method: "GET",
			},
			"variant-create": {
				Fn:     apiBuildVariantCreate,
				Method: "POST",
			},
			"variant-modify": {
				Fn:     apiBuildVariantModify,
				Method: "POST",
			},
			"variant-delete": {
				Fn:     apiBuildVariantDelete,
				Method: "GET",
			},
			"variant-build": {
				Fn:     apiBuildVariantBuildNow,
				Method: "GET",
			},
		},
	},
	"build": {
		Descriptor: apiActionT{
			Fn:     apiBuildInfoHandler,
			Method: "GET",
		},
		Actions: map[string]apiActionT{
			"log": {
				Fn:     apiBuildLogHandler,
				Method: "GET",
			},
			"list": {
				Fn:     apiBuildListHandler,
				Method: "GET",
			},
			"list-test": {
				Fn:     apiTestListHandler,
				Method: "GET",
			},
		},
	},
	"test": {
		Descriptor: apiActionT{
			Fn:     apiTestInfoHandler,
			Method: "GET",
		},
		Actions: map[string]apiActionT{
			"log": {
				Fn:     apiTestInfoHandler,
				Method: "GET",
			},
		},
	},
	"user": {
		Descriptor: apiActionT{
			Fn:     apiUserInfoHandler,
			Method: "GET",
		},
		Actions: map[string]apiActionT{
			"create": {
				Fn:     apiUserCreate,
				Method: "POST",
			},
			"delete": {
				Fn:     apiUserDelete,
				Method: "GET",
			},
			"modify": {
				Fn:     apiUserModify,
				Method: "POST",
			}, /*
			   "password": {
			       Fn: apiUserPassword,
			       Method: "POST",
			   },*/
			"list": {
				Fn:     apiUserList,
				Method: "GET",
			},
		},
	},
	"group": {
		Descriptor: apiActionT{
			Fn:     apiGroupQuery,
			Method: "GET",
		},
		Actions: map[string]apiActionT{
			"members": {
				Fn:     apiGroupMembers,
				Method: "GET",
			},
			"list": {
				Fn:     apiGroupList,
				Method: "GET",
			},
			"create": {
				Fn:     apiGroupCreate,
				Method: "POST",
			},
			"leave": {
				Fn:     apiGroupLeave,
				Method: "POST",
			},
			/*            "delete": {
			              Fn: apiGroupDelete,
			              Method: "DELETE",
			          },*/
			"addMember": {
				Fn:     apiGroupAddMember,
				Method: "POST",
			}, /*
			   "removeMember": {
			       Fn: apiGroupRemoveMember,
			       Method: "POST",
			   },
			   "modifyMember": {
			       Fn: apiGroupModifyMember,
			       Method: "POST",
			   },*/
		},
	},
	/*"daemon": {
	    Actions: map[string]apiActionT {
	        "status": {
	            Fn: apiDaemonStatus,
	            Method: "GET",
	        },
	        "join": {
	            Fn: apiDaemonJoin,
	            Method: "POST",
	        },
	        "start": {
	            Fn: apiDaemonStart,
	            Method: "POST",
	        },
	        "finish": {
	            Fn: apiDaemonFinish,
	            Method: "POST",
	        },
	        "reject": {
	            Fn: apiDaemonReject,
	            Method: "POST",
	        },
	    },
	},*/
}

// generic API-handler.
// All API-calls are firstly handled by this function, which then
// routes the query to the correct function
func apiHandler(ctx echo.Context) error {
	user := core.User{}
	if core.Cfg.Api.Enable {
		if !ApiTokenFromSession(&user, ctx) {
			return apiReturn(ctx, http.StatusForbidden, "/", apiError("No valid API Token"))
		}
	} else {
		if !UserFromSession(&user, ctx) {
			return apiReturn(ctx, http.StatusForbidden, "/", apiError("No valid API Token"))
		}
	}

	endpoint := ctx.Param("endpoint")
	action := apiGetAction(ctx)
	method := ctx.Request().Method

	var d apiActionT
	var di apiHandlerT
	var ok bool
	if action == "" {
		di, ok = apiRoutingTable[endpoint]
		if ok {
			d = di.Descriptor
		}
	} else {
		di, ok = apiRoutingTable[endpoint]
		if ok {
			d, ok = di.Actions[action]
		}
	}

	if !ok || d.Fn == nil || d.Method != method {
		return apiReturn(ctx, http.StatusBadRequest, "/", apiError("Invalid action"))
	}

	return d.Fn(endpoint, user, ctx)
}

func apiError(msg string) apiErrorObj {
	return apiErrorObj{ErrorMsg: msg, Result: "Error"}
}
func apiSuccess(action string, msg string) apiSuccessObj {
	return apiSuccessObj{Action: action, Message: msg, Result: "Success"}
}

const acceptHtml int = 1
const acceptJson int = 2

func apiAccepts(ctx echo.Context) int {
	var accepts []string
	accepts, ok := ctx.Request().Header["Accept"]
	if !ok {
		return acceptHtml
	}

	var Json = false
	for _, v := range accepts {
		if strings.Contains(v, "text/html") {
			return acceptHtml
		}
		if strings.Contains(v, "application/json") {
			Json = true
		}
	}

	if Json {
		return acceptJson
	} else {
		return acceptHtml
	}
}

type apiErrorObj struct {
	ErrorMsg string
	Result   string
}
type apiSuccessObj struct {
	Action  string
	Message string
	Result  string
}

func (err apiErrorObj) Error() string {
	return err.ErrorMsg
}

func (err apiSuccessObj) Error() string {
	return err.Message
}

func redirect(ctx echo.Context, url string) error {
	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+url)
}

func apiReturn(ctx echo.Context, status int, redirectUrl string, obj interface{}) error {
	accepts := apiAccepts(ctx)
	if accepts == acceptJson {
		ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		return ctx.JSON(http.StatusOK, obj)
	} else {
		// HTML
		if obj != nil {
			core.Console.Log("Redirecting to", redirectUrl)
			err, ok := obj.(apiErrorObj)
			if ok {
				return redirect(ctx, redirectUrl+"?error="+err.ErrorMsg)
			}
			succ, ok := obj.(apiSuccessObj)
			if ok {
				return redirect(ctx, redirectUrl+"?success="+succ.Message)
			}
		}
		return redirect(ctx, redirectUrl)
	}
}

// helper: retrieves a string parameter (by name) from the context, either from param (GET) or FormValue (mostly used by POST)
func apiGetParam(ctx echo.Context, param string) (string, error) {
	value := ctx.Param(param)
	if value == "" {
		value = ctx.FormValue(param)
	}
	if value == "" {
		value = ctx.QueryParam(param)
	}
	if value == "" {
		return "", errors.New("Value missing")
	}
	return value, nil
}

// helper: retrieves a number (by name) from the context, either from param (GET) or FormValue (mostly used by POST)
func apiGetNumber(ctx echo.Context, param string) (int, error) {
	i, err := strconv.Atoi(ctx.Param(param))
	if err != nil {
		i, err = strconv.Atoi(ctx.FormValue(param))
		if err != nil {
			i, err = strconv.Atoi(ctx.QueryParam(param))
			if err != nil {
				return -1, err
			}
		}
	}
	return i, nil
}

// helper: shorthand for action retrieval
func apiGetAction(ctx echo.Context) string {
	str, _ := apiGetParam(ctx, "action")
	return str
}

// shorthand for id retrieval
func apiGetID(ctx echo.Context) (int, error) {
	return apiGetNumber(ctx, "id")
}

// implements a in b - query. I.e. is an element a in a list b
func in(a string, b []string) bool {
	for _, v := range b {
		if a == v {
			return true
		}
	}

	return false
}

func apiGetCheckBox(ctx echo.Context, name string) (bool, error) {
	value, err := apiGetParam(ctx, name)
	if err != nil {
		return false, err
	}
	if value == "true" || value == "1" || value == "on" {
		return true, nil
	} else {
		return false, nil
	}
}

func apiListHelper(ctx echo.Context, allowedOrders []string) (string, string, int, int) {
	name, err := apiGetParam(ctx, "name")
	if err != nil {
		name = ""
	} else {
		name = "%" + name + "%"
	}

	orderby, err := apiGetParam(ctx, "order_by")
	if err != nil {
		orderby = allowedOrders[0] + " DESC"
	}

	if !in(orderby, allowedOrders) {
		orderby = allowedOrders[0] + " DESC"
	}

	limit, err := apiGetNumber(ctx, "limit")
	if err != nil {
		limit = 10
	}

	startAt, err := apiGetNumber(ctx, "start_at")
	if err != nil {
		startAt = 0
	}

	return name, orderby, limit, startAt
}
