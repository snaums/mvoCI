// Handlers for /build functions

package web

import (
	"os"

	//"fmt"
	"strconv"
	//"regexp"
	"net/http"

	"codeberg.org/snaums/mvoCI/build"
	"codeberg.org/snaums/mvoCI/core"

	//"golang.org/x/crypto/bcrypt"
	//"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	//"github.com/labstack/echo/middleware"
)

// /zip/:file
// find and download a zip file, deny if the user has no permissions
func zipDownloadHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	file := ctx.Param("file")
	var b core.Build
	var r core.Repository
	core.DB.Select("zip, id, repository_id").Where("zip = ?", file).First(&b)
	core.DB.Select("id", "user_id").Where("id = ?", b.RepositoryID).First(&r)
	acl := core.AclCheck(user, r)
	if acl < core.AclView {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(int(b.RepositoryID)))
	}

	return ctx.File(core.Cfg.Directory.Build + file)
}

// /build/:id
// render the build view
func buildViewHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}
	var b core.Build
	var r core.Repository
	var id int
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return HTTPError(500, ctx)
	}
	core.DB.Where("id = ?", id).First(&b)
	core.DB.Select("name, id, user_id").Where("id = ?", b.RepositoryID).First(&r)

	acl := core.AclCheck(user, r)
	if acl < core.AclView {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(int(b.RepositoryID))+"?error=Permission Error")
	}

	unfinished := false
	if b.Status != "finished" && b.Status != "failed" {
		unfinished = true
	}

	var children []core.Build
	core.DB.Where("parent_id = ?", id).Find(&children)

	return ctx.Render(http.StatusOK, "build", echo.Map{
		"navPage":                "repo",
		"user":                   user,
		"repo":                   r,
		"acl":                    acl,
		"build":                  b,
		"unfinished":             unfinished,
		"children":               children,
		"render_test_state":      repoTestRenderResult,
		"create_linear_gradient": repoTestCreateGradient,
	})
}

// /build/log/:id
// return the build log as string
func buildLogHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}
	var b core.Build
	var id int
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return HTTPError(500, ctx)
	}
	var r core.Repository
	core.DB.Select("log, repository_id").Where("id = ?", id).First(&b)
	core.DB.Select("user_id, id").Where("id = ?", b.RepositoryID).First(&r)

	acl := core.AclCheck(user, r)
	if acl < core.AclView {
		return ctx.String(403, "Permission denied")
	}

	if len(b.Log) != 0 {
		return ctx.String(http.StatusOK, b.Log)
	}

	return HTTPError(404, ctx)
}

// /build/delete/:id
// delete the build and its artifact
func buildDeleteHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}
	var id int
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return HTTPError(500, ctx)
	}
	var b core.Build
	var r core.Repository
	core.DB.Select("id, repository_id, zip").Where("id = ?", id).First(&b)
	core.DB.Select("user_id, id").Where("id = ?", b.RepositoryID).First(&r)

	acl := core.AclCheck(user, r)
	if acl < core.AclDelete {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(int(r.ID))+"?error=Permission Error")
	}

	r.ID = buildDelete(uint(id))
	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(int(r.ID)))
}

// removes a build archive
func buildDeleteArchive(zip string) {
	os.Remove(core.Cfg.Directory.Build + zip)
}

// helper function to delete a build from the database and the artifact from the file system
func buildDelete(id uint) uint {
	var rID uint
	var b core.Build

	core.DB.Select("id, repository_id, zip").Where("id = ?", id).First(&b)
	rID = b.RepositoryID
	buildDeleteArchive(b.Zip)
	core.DB.Delete(&b)

	core.DB.Where("build_id = ?", id).Delete(&core.Test{})
	return rID
}

// /build/rebuild/:id
// rebuild a given build with the current settings
func buildRebuildHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	var b core.Build
	var id int
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo?error=Repo not found")
	}
	core.DB.Joins("Repository").First(&b, id)
	r := b.Repository
	if r.ID == 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo?error=Repo not found")
	}

	acl := core.AclCheck(user, r)
	if acl < core.AclExecute {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(int(r.ID))+"?error=Permission Error")
	}

	bID := build.ReBuild(&b)
	return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"build/"+strconv.Itoa(int(bID)))
}

func buildVariantAddHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	var r core.Repository
	var id int
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo?error=Repo not found")
	}

	core.DB.First(&r, id)
	if r.ID == 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo?error=Repo not found")
	}

	acl := core.AclCheck(user, r)
	if acl < core.AclView {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(int(r.ID))+"?error=Permission Error")
	}

	return ctx.Render(http.StatusOK, "repo_variant_edit", echo.Map{
		"navPage": "repo",
		"user":    user,
		"repo":    r,
		"acl":     acl,
		"mode":    "add",
	})
}

func buildVariantCloneHandler(ctx echo.Context) error {
	return buildVariantFormHandler(ctx, "clone")
}
func buildVariantEditHandler(ctx echo.Context) error {
	return buildVariantFormHandler(ctx, "edit")
}

func buildVariantFormHandler(ctx echo.Context, mode string) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	var r core.Repository
	var id int
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo?error=Repo not found")
	}

	core.DB.First(&r, id)
	if r.ID == 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo?error=Repo not found")
	}

	acl := core.AclCheck(user, r)
	if acl < core.AclView {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(int(r.ID))+"?error=Permission Error")
	}

	variant_id, err := strconv.Atoi(ctx.Param("variant_id"))
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(int(r.ID))+"?error=Invalid ID")
	}

	var variant core.BuildVariant
	core.DB.Preload("Variables").Where("id = ?", variant_id).First(&variant)
	if variant.ID == 0 || variant.ID != uint(variant_id) {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(int(r.ID))+"?error=Invalid ID")
	}

	return ctx.Render(http.StatusOK, "repo_variant_edit", echo.Map{
		"navPage": "repo",
		"user":    user,
		"repo":    r,
		"acl":     acl,
		"mode":    mode,
		"variant": variant,
	})
}

func buildVariantHandler(ctx echo.Context) error {
	user := core.User{}
	if !UserFromSession(&user, ctx) {
		return loginHandler(ctx)
	}

	var r core.Repository
	var id int
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo?error=Repo not found")
	}

	core.DB.First(&r, id)
	if r.ID == 0 {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo?error=Repo not found")
	}

	acl := core.AclCheck(user, r)
	if acl < core.AclView {
		return ctx.Redirect(http.StatusFound, core.Cfg.HttpSubUrl+"repo/view/"+strconv.Itoa(int(r.ID))+"?error=Permission Error")
	}

	var variants []core.BuildVariant
	core.DB.Preload("Variables").Where("repository_id = ?", id).Order("name").Find(&variants)

	return ctx.Render(http.StatusOK, "repo_variant", echo.Map{
		"navPage":  "repo",
		"user":     user,
		"repo":     r,
		"acl":      acl,
		"variants": variants,
	})
}
