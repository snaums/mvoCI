package auth

// Dummy-Auth, the most simple authentication provider, can be used as an example

import (
	"errors"
	"math/rand"
	"strconv"
	"strings"

	"codeberg.org/snaums/mvoCI/core"
)

var dummyProvider = Provider{
	Name:        "dummy",
	HumanName:   "Dummy Authentification",
	Description: "Check that the user can read (do not use)",

	Enable:       dummyEnable,
	EnableView:   dummyEnableView,
	EnableCommit: dummyEnableCommit,
	Verify:       dummyVerify,
	Seed:         dummySeed,
	Cap: ProviderCap{
		Seed: true,
	},
}

// all providers have to add themseleves to the auth provider list
func init() {
	__init(&dummyProvider)
}

// verify user input against a secret
func dummyVerify(user *core.User, stepExtra string, given string, extra string, providerExtra string) error {
	core.Console.Log("dummyVerify", stepExtra, given, extra, providerExtra)
	g, _ := strconv.Atoi(given)
	se, _ := strconv.Atoi(stepExtra)
	e, _ := strconv.Atoi(providerExtra)
	if g == se+e {
		return nil
	} else {
		return errors.New("Incorrect login")
	}
}

// seed the secret for some run-time variations, eg. against time
func dummySeed(stepExtra string) string {
	e, _ := strconv.Atoi(stepExtra)
	return strconv.FormatInt((rand.Int63()%10)+(int64(e)%10)*10, 10)
}

// enable the module if possible
func dummyEnable(_ core.User, _ *core.AuthProvider) (string, error) {
	return "stage2", nil
}

// configure the enable-view of the module, here by giving the secret and a challenge to the user
func dummyEnableView(user core.User, prov *core.AuthProvider) (string, error) {
	secret := strconv.FormatInt(rand.Int63()%10, 10)
	challenge := strconv.FormatInt(rand.Int63()%10, 10)
	prov.Extra = secret + "," + challenge
	return "Your secret is: <b>" + secret + "</b>. Please add it to " + challenge + ".", nil
}

// enable commit checks the challenge of dummyEnableView and commits the secret to the users database entry if correct
func dummyEnableCommit(user core.User, prov *core.AuthProvider, given string) error {
	o := strings.Split(prov.Extra, ",")
	s, _ := strconv.Atoi(o[0])
	c, _ := strconv.Atoi(o[1])

	wanted := s + c
	g, _ := strconv.Atoi(given)
	if wanted == g {
		prov.Extra = strconv.FormatInt(int64(s), 10)
		return nil
	}

	secret := strconv.FormatInt(int64(s), 10)
	challenge := strconv.FormatInt(rand.Int63()%10, 10)
	prov.Extra = secret + "," + challenge

	return errors.New("Is not good, Your secret is: <b>" + secret + "</b>. Please add it to " + challenge)
}
