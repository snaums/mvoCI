package core

// logging facility

import (
	"fmt"
	. "log"
	"os"
	"runtime"
	"strconv"
	"strings"
)

// structure containing callback for different log levels
type LogWrapper interface {
	Debug(...interface{})
	Debugf(string, ...interface{})
	Log(...interface{})
	Logf(string, ...interface{})
	Warn(...interface{})
	Warnf(string, ...interface{})
	Fail(...interface{})
	Failf(string, ...interface{})
}

const (
	DEBUG = 0 // debug log level
	INFO  = 1 // info log level
	WARN  = 2 // warning log level
	FAIL  = 3 // failure log level
)

var levelString []string = []string{
	"DEBUG",
	"INFO",
	"WARN",
	"FAIL",
	"INVALID",
}

// string representation with ANSI Escape Sequence colors attached to them
var levelStringColor []string = []string{
	"\033[90m" + levelString[DEBUG] + "\033[0m",
	"\033[97m" + levelString[INFO] + "\033[0m",
	"\033[33m" + levelString[WARN] + "\033[0m",
	"\033[31;1m" + levelString[FAIL] + "\033[0m",
	"INVALID",
}

// configuration of the log output
type lwrapper struct {
	ColorOutput bool    // is coloured output enabled?
	LogLevel    int     // the log level to be used
	l           *Logger //
	s           *Logger
}

// is file logging enabled?
var LogFileEnable bool = false

// is stdout logging enabled?
var LogStdoutEnable bool = true

// is stderr loggin enabled?
var LogStderrEnable bool = false

// the file to be used for server logging
var f *os.File

// Console is the main logging facility in mvoCI
var Console lwrapper

// ServerLogger for the echo web server
type ServerLogger struct {
	f *os.File
}

// create a new server logger by opening the logfile
func ServerLoggerNew(logfile string) (*ServerLogger, error) {
	var err error
	f, err = os.OpenFile(logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return nil, err
	}

	var s ServerLogger
	s.f = f
	return &s, nil
}

// close the server logger on shutdown
func (s ServerLogger) Close() {
	s.f.Close()
}

// write into the server logger
func (s ServerLogger) Write(p []byte) (n int, err error) {
	s.f.Write(p)
	s.f.Close()
	return os.Stdout.Write(p)
}

// debug logger output is only enable if cfg.Debug is set to true
func (lw lwrapper) Debug(args ...interface{}) {
	if Cfg.Debug {
		lw.outputString(DEBUG, "", args)
	}
}

// debug logger output is only enable if cfg.Debug is set to true
func (lw lwrapper) Debugf(format string, args ...interface{}) {
	if Cfg.Debug {
		lw.outputString(DEBUG, format, args)
	}
}

// set the log level
func (lw *lwrapper) LogMode(level int) {
	lw.LogLevel = int(level)
}

func (lw lwrapper) Log(args ...interface{}) {
	lw.outputString(INFO, "", args)
}
func (lw lwrapper) Logf(format string, args ...interface{}) {
	lw.outputString(INFO, format, args)
}
func (lw lwrapper) Warn(args ...interface{}) {
	lw.outputString(WARN, "", args)
}
func (lw lwrapper) Warnf(format string, args ...interface{}) {
	lw.outputString(WARN, format, args)
}
func (lw lwrapper) Fail(args ...interface{}) {
	lw.outputString(FAIL, "", args)
}
func (lw lwrapper) Failf(format string, args ...interface{}) {
	lw.outputString(FAIL, format, args)
}

// Checks the log level and whether this output should be generated at all. If so
// create the string of the output according to the level, format and parameters.
// Used by the logging functions of the different levels
func (lw lwrapper) outputString(level int, format string, args []interface{}) {
	if lw.LogLevel <= level {
		str := lw.createString(level, format, args)
		lw.l.Print(str)
		if level > DEBUG {
			lw.s.Print(str)
		}
	}
}

// Creates the string of the output according to colour enabling, level, formatstring and its parameters
func (lw lwrapper) createString(level int, format string, args []interface{}) string {
	var lvl string
	var retval string

	if len(levelString) > level {
		if lw.ColorOutput {
			lvl = levelStringColor[level]
		} else {
			lvl = levelString[level]
		}
	} else {
		lvl = "INVALID"
	}

	_, file, line, ok := runtime.Caller(3) // the direct caller is a log function
	if !ok {
		file = "invalid"
		line = -1
	}

	fs := strings.Split(file, "/")
	file = fs[len(fs)-1]
	retval = "[" + lvl + "] " + file + ":" + strconv.FormatInt(int64(line), 10) + " "
	if format == "" {
		retval += fmt.Sprintln(args...)
	} else {
		retval += fmt.Sprintf(format, args...)
	}
	return retval
}

// initialize the logging facility
func LogInit(lf string) (LogWrapper, error) {
	var err error
	f, err = os.OpenFile(lf, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return nil, err
	}

	var s *Logger
	l := New(f, "", Ldate|Ltime)
	if LogStderrEnable {
		s = New(os.Stderr, "", Ldate|Ltime)
	} else if LogStdoutEnable {
		s = New(os.Stdout, "", Ldate|Ltime)
	} else {
		fnull, _ := os.OpenFile(os.DevNull, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		s = New(fnull, "", 0)
	}
	Console.l = l
	Console.s = s
	Console.ColorOutput = true
	return &Console, nil
}

// close the logger
func LogClose() {
	f.Close()
}
