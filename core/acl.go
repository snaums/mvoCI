package core

// type representing AclLevels
type AclLevel int

func (a AclLevel) String() string {
	return AclStrings[int(a)]
}

// mapping AclLevels to string representations
var AclStrings map[int]string = map[int]string{
	int(AclForbidden): "AclForbidden",
	int(AclView):      "AclView",
	int(AclExecute):   "AclExecute",
	int(AclEdit):      "AclEdit",
	int(AclDelete):    "AclDelete",
	int(AclComplete):  "AclComplete",
}

// mapping String representations to AclLevels for reverse lookup
var ReverseAclStrings map[string]AclLevel = map[string]AclLevel{
	"AclForbidden": AclForbidden,
	"AclView":      AclView,
	"AclExecute":   AclExecute,
	"AclEdit":      AclEdit,
	"AclDelete":    AclDelete,
	"AclComplete":  AclComplete,
}

// AclCheck for Template calls
func WebAclCheck(have AclLevel, want string) bool {
	wantLevel, ok := ReverseAclStrings[want]
	if ok {
		return (have >= wantLevel)
	} else {
		return false
	}
}

const (
	AclForbidden AclLevel = 0 // No Access is granted
	AclView      AclLevel = 1 // Access to view the repo is granted
	AclExecute   AclLevel = 2 // Access to run builds is granted
	AclEdit      AclLevel = 3 // Access to edit information is granted
	AclDelete    AclLevel = 4 // Access to delete the repository or builds is granted
	AclComplete  AclLevel = 5 // Complete access is granted
)

// returns the string (human readable) representation of the Acl-States
func AclToString(id int) string {
	if id > int(AclComplete) || id < int(AclForbidden) {
		return ""
	}
	return AclStrings[id]
}

// check the permissions for a repository and return the level of access granted
// or AclForbidden if no access has been granted or explicitely forbidden
func AclCheck(user User, object Repository) AclLevel {
	if user.ID <= 0 || object.ID <= 0 {
		return AclForbidden
	}
	// Superuser and owner always have complete access
	if user.Superuser || user.ID == object.UserID {
		return AclComplete
	}

	var acls []Acl

	// first check user privileges:
	// forbidden takes precedence; otherwise search for highest membership
	DB.Where("user_id = ? AND repository_id = ?", user.ID, object.ID).Find(&acls)
	var access_level AclLevel = AclForbidden
	for _, v := range acls {
		if v.AccessLevel == AclForbidden {
			return AclForbidden
		}
		if v.AccessLevel > access_level {
			access_level = v.AccessLevel
		}
	}
	if access_level != AclForbidden {
		return access_level
	}

	// then check for groups:
	// forbidden takes precedence; otherwiese search for highest membership
	DB.Where("repository_id = ? and group_id IN (?)", object.ID, DB.Table("user_groups").Select("group_id").Where("user_id=?", user.ID)).Find(&acls)
	for _, v := range acls {
		if v.AccessLevel == AclForbidden {
			return AclForbidden
		}
		if v.AccessLevel > access_level {
			access_level = v.AccessLevel
		}
	}

	return access_level
}
