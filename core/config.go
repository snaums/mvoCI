// core contains the most central elements of mvoCI, except the webserver.
// it contains the global config object and the database connection.
// could be used without the webserver to have the same backend for e.g. CLI UI
package core

import (
	"encoding/json"
	"io"
	"os"
	"reflect"
	"strconv"
	"strings"
)

var Cfg Config = Config{}

// configuration file
const ConfigFile string = "mvo.cfg"

// build time entered by the linker (see Makefile)
var BuildTime string = ""

// Compiler version entered by the linker (see Makefile)
var Compiler string = ""

// version entered by the linker (see Makefile), either release number or git-sha
var Version string = "development"

// git commit hash entered by the linker (see Makefile)
var GitHash string = ""

// structure for directory configurations
type ConfigDirectory struct {
	Repo  string // directory for checkout of the repositories for building
	Build string // directory for storing the build artifacts
}

// information about the author for the impress
type ConfigAuthor struct {
	Link    string // link to an external impress, will take precedence if not empty
	Email   string // email address of the webmaster
	Name    string // name of the webmaster
	Street  string // address information
	Zip     string // zip code
	City    string // the city
	Privacy string // privacy and gdpr information text
}

// configuration about the database connection
type ConfigDatabase struct {
	Provider    string // database provider, i.e. sqlite, mssql, mysql or postgres (supported by gorm.io)
	Username    string // username for auth at the database
	Password    string // password for auth at the database
	Port        int    // port of the databases socket
	Host        string // host, as it may be on another machine
	Dbname      string // name of the database to use
	Filename    string // filename of the sqlite database if used
	PostgresSSL string // whether or not postgres should use ssl/tsl encryption
}

// configuration about the build page, with an ongoing build
// if the api is enabled, this gives the pace at which the pace at which query will be sent by javascript
type ConfigBuild struct {
	RefreshPage     bool // whether or not the page should be refreshed automagically at all
	RefreshInterval int  // interval in seconds at which the page should be refreshed automagically
	LiveLog         bool // whether the build log is updated while the build is running or only when the build is finished
}

// information about the API
type ConfigApi struct {
	Enable bool // API is allowed with API-token. Otherwise only logged in users are allowed to use the API
}

// information about (a single) OAuth token
type ConfigOauth struct {
	OauthClientId     string // OAuth Client ID
	OauthClientSecret string // OAuth Secret
}

// global config structure
type Config struct {
	AppTitle             string // title of the application (defaults to mvoCI)
	Install              bool   // is the application currently in installation mode
	Debug                bool   // set it into debug mode
	HttpPort             int    // the http port to be used
	HttpHost             string // the http host to be used
	HttpSubUrl           string // the http subdir to be used
	LogFile              string // path to the log file
	LogServer            string // log file for the echo webserver
	LogFileEnable        bool   // enable the file-log
	LogMode              string // writing logs to stdout or stderr?
	LoginTokenDuration   int    // how long does a loginToken survive if inactive?
	LoginTokenInvalidate bool   // invalidate all loginTokens at start-up?
	ParallelBuilds       int    // number of parallel workers to be spawned
	CompressionMethod    string // compression method to be used for the artifacts (file-ending to be used with tar command)
	RepoSecretShow       bool   // should the repo-page show the secret or a placeholder?
	Directory            ConfigDirectory
	Author               ConfigAuthor
	Database             ConfigDatabase
	WebHookLog           bool // log events of the webhook for debugging purposes?
	PublicEnable         bool // have a page with the publically visible repositories?
	PublicBuildLog       bool // show the build log of a build publically
	LandingEnable        bool // enable the landing page
	Api                  ConfigApi
	Build                ConfigBuild
	GiteaApi             ConfigOauth
	Auth                 ConfigAuth
	Email                EmailServerSettings
}

// information about the authentication providers
type ConfigAuth struct {
	TOTPEnable   bool // is totp enabled?
	NativeEnable bool // is native password auth enabled?
}

// generate the default configuration
func ConfigDefault() Config {
	return Config{
		AppTitle:             "mvoCI",
		Install:              false,
		Debug:                false,
		HttpPort:             4042,
		HttpHost:             "localhost",
		HttpSubUrl:           "/",
		LoginTokenDuration:   300,
		LoginTokenInvalidate: true,
		CompressionMethod:    "gz",
		ParallelBuilds:       2,
		LogFile:              "log/mvoCI.log",
		LogFileEnable:        true,
		LogMode:              "stderr",
		LogServer:            "log/server.log",
		Directory: ConfigDirectory{
			Repo:  "repo/",
			Build: "builds/",
		},
		WebHookLog:     false,
		PublicEnable:   false,
		PublicBuildLog: false,
		Api: ConfigApi{
			Enable: true,
		},
		Build: ConfigBuild{
			RefreshPage:     true,
			RefreshInterval: 5000, // 1000 -> 1 second
			LiveLog:         false,
		},
		Auth: ConfigAuth{
			NativeEnable: true,
			TOTPEnable:   true,
		},
		LandingEnable: true,
		// Email -> empty strings
		// Author -> empty strings
		// Database -> empty strings
	}
}

// reflect into the config-object for using config in the templates, the recursive work-part of reflection
func (cfg Config) recurseReflect(current reflect.Value, parts []string) string {
	var v reflect.Value = current.FieldByName(parts[0])
	if v.Type().Name() == "int" {
		return strconv.FormatInt(v.Int(), 10)
	} else if v.Type().Name() == "string" {
		return v.String()
	} else if v.Type().Name() == "bool" {
		if v.Bool() {
			return "true"
		} else {
			return "false"
		}
	}
	if len(parts) > 1 {
		return cfg.recurseReflect(v, parts[1:])
	}
	return "Invalid Type"
}

// reflect into the config-object for using config in the templates
func (cfg Config) Reflect(q string) string {
	var parts = strings.Split(q, ".")
	current := reflect.Indirect(reflect.ValueOf(cfg))
	var r = cfg.recurseReflect(current, parts)
	//Console.Log ("Found value", r, "in reflecting cfg for", q)
	return r
}

// write the config to the a file
func (c Config) ConfigWrite(filename string) error {
	b, err := json.MarshalIndent(c, "", "  ")
	if err != nil {
		return err
	}

	err = os.WriteFile(filename, b, 0644)
	return err
}

func (c *Config) ConfigValidate() {
	if !strings.HasPrefix(c.HttpSubUrl, "/") {
		c.HttpSubUrl = "/" + c.HttpSubUrl
	}
	if !strings.HasSuffix(c.HttpSubUrl, "/") {
		c.HttpSubUrl = c.HttpSubUrl + "/"
	}
}

// reads the configuration from file to a KeyValueStore-object
func (c *Config) ConfigRead(filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		// Todo: early console
		//Console.Fail ("Cannot open config file")
		return err
	}
	defer f.Close()

	err = GenericJSONDecode(f, c)
	if err != nil {
		// TODO: early config
		//Console.Log ( "Fatal error", err )
		//panic("")
		return err
	}

	return nil
}

// helper function for parsing JSON-files into an object of our choosing
func GenericJSONDecode(rd io.Reader, pl interface{}) error {
	dec := json.NewDecoder(rd)
	for dec.More() {
		err := dec.Decode(pl)
		if err != nil {
			// early debug
			//Console.Log ( "JSON-decode error: ", err )
			return err
		}
	}
	return nil
}
