//go:build windows
// +build windows

// default interface for Pthread_create for use on Windows, uses goroutines
// for creating the build workers.

package core

type Thread uintptr
type ThreadCallback func()

// would normally create a pthread, but will just create a go-routine
func Pthread_create(cb ThreadCallback) Thread {
	go cb()
	return 0
}
