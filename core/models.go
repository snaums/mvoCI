package core

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var DB *gorm.DB

const GroupEveryone string = "Everyone"
const UserFailedLoginAttempts int = 4

// structure representing a user
type User struct {
	ID                uint      `gorm:"primary_key"` // database ID
	CreatedAt         time.Time // time the record was created
	UpdatedAt         time.Time // time the record was last updated
	BannedUntil       time.Time // time until the user is banned
	LastLoginAttempt  time.Time // time at which the user last tried to log in
	LoginAttempts     int       // number of login attempts
	Name              string    // the username of the user
	Passhash          string    // a hashed password
	Email             string    // email address of the user
	Superuser         bool      // is the user a superuser or not?
	Group             []*Group  `gorm:"many2many:user_groups"`
	AuthProvider      string    // name of the main auth provider
	AuthProviderExtra string    // extra information for the main auth provider
	AuthExtra         string    // extra information for the main auth provider
}

// structure for an auth provider for a single user (not shared!)
type AuthProvider struct {
	ID     uint   `gorm:"primary_key"`
	Name   string // name of the auth provider
	Type   string // name of the auth provider module
	Extra  string // information of the auth provider used for verification
	UserId uint   // reference to the according user
	User   User   // the user object
}

func (u *User) StepLoginAttempts() {
	if time.Now().After(u.LastLoginAttempt.Add(5 * time.Minute)) {
		u.LoginAttempts = 0
	}
	if u.ID > 0 {
		DB.Save(&u)
	}
}

func (u *User) FailedLoginAttempt() bool {
	if u.ID > 0 {
		u.LoginAttempts++
		u.LastLoginAttempt = time.Now()
		if u.LoginAttempts > UserFailedLoginAttempts {
			u.BannedUntil = time.Now().Add(time.Minute * 5)

			DB.Save(&u)
			return true
		}
		DB.Save(&u)
	}
	return false
}

func (u *User) SuccessfullLogin() {
	u.LastLoginAttempt = time.Now()
	u.BannedUntil = time.Unix(0, 0)
	u.LoginAttempts = 0
	DB.Save(&u)
}

// group information, grouping users into groups for easier access control
type Group struct {
	ID        uint      `gorm:"primary_key"` // database ID
	CreatedAt time.Time // time the record was created
	UpdatedAt time.Time // time the record was last updated
	Name      string    `gorm:"unique"`                // name of the group
	User      []User    `gorm:"many2many:user_groups"` // a list of users in this group
}

// structure for LoginTokens, used for authentification after login
type LoginToken struct {
	ID        uint      `gorm:"primary_key"` // database ID
	CreatedAt time.Time // date of creation
	ExpiresAt time.Time // Expiration Date of the loginToken
	Name      string    // name of that LoginToken, e.g. for API tokens
	UserID    uint      // Link to the user holding the Token
	User      User
	Type      string // api or login-token?
	Secret    string // a secret string
	Step      string // auth step, at which this loginToken sits
	StepExtra string // extra information of the authProvider of the next step, e.g. for storing seeds for the next try
}

// OAuthToken for the gitea release-hook
type OauthToken struct {
	ID           uint `gorm:"primary_key"`
	CreatedAt    time.Time
	UpdatedAt    time.Time
	State        string
	Name         string
	Api          string
	Url          string
	UserID       uint
	ClientId     string
	ClientSecret string
	ClientToken  string
	RedirectUri  string
}

// structure representing an Access Control Entity. Acls are represent
// a relation between a single User or a Group and a Repository together
// with a permission level (AclLevel). Only one (User or Group) may be
// populated.
type Acl struct {
	ID           uint      `gorm:"primary_key"` // database ID
	CreatedAt    time.Time // time the record was created
	UpdatedAt    time.Time // time the record was last updated
	Group        *Group    // this group may access the repository
	GroupID      uint
	User         *User // this user may access the repository
	UserID       uint
	Repository   Repository // Access to this Repository is granted
	RepositoryID uint
	AccessLevel  AclLevel // what permission level is granted?
}

// structure representing a Repository
type Repository struct {
	ID                 uint `gorm:"primary_key"`
	CreatedAt          time.Time
	UpdatedAt          time.Time
	Name               string // name of the repository
	Secret             string // secret of the repository to be used with the webhook feature of gitea, gogs, etc.
	CloneUrl           string // the clone url is to be used when building the repository for cloning and for finding out which repo needs to be build when receiving a webhook event.
	DefaultBranch      string // default branch to be build when nothing else is specified (master or main)
	BuildScript        string // Builds script
	WebHookEnable      bool   // enable webhook events for this repository
	KeepBuilds         bool   // keep build artifacts?
	UserID             uint
	User               User
	Acl                []Acl
	Public             bool // is this repository to be shown on the Public Repository page? Only successful builds are shown there. Needs cfg.PublicEnable
	SendMail           bool
	Nightly            bool // build this repository at Nightly
	NightlyWhenChanged bool // build it nightly but only if it has changes at the day.
	NightlyHasChanges  bool // internal mark whether or not the repo has seen a webhook throughout the day
}

type Checksum struct {
	Sha1 string
	Md5  string
}

type BuildVariantVariable struct {
	ID             uint
	Key            string
	Value          string
	BuildVariantID uint
}

type BuildVariant struct {
	ID           uint
	CreatedAt    time.Time
	UpdatedAt    time.Time
	Name         string
	Description  string
	Variables    []BuildVariantVariable
	RepositoryID uint
	Repository   *Repository
}

// structure representing a Build of Repositories
type Build struct {
	ID            uint       `gorm:"primary_key"` // database ID
	CreatedAt     time.Time  // starting time of the build
	UpdatedAt     time.Time  // structure last updated
	StartedAt     time.Time  // the point when a build worker started the build
	FinishedAt    time.Time  // time of the moment the build finished
	Duration      string     // duration, used for calculating the duration of a build
	Status        string     // the status of the Build (started, finished, failed)
	Log           string     // build log
	CommitSha     string     // stores the commit reference
	CommitAuthor  string     // author of a commit
	CommitMessage string     // message of a commit
	CommitUrl     string     // url to view the commit in the CVS-interface
	TagName       string     // Git Tag Name
	Event         string     // event string, why the build was built
	Branch        string     // stores the branch built
	Zip           string     // keeps the name of the zipped Build
	Repository    Repository // link to the Repository
	RepositoryID  uint
	Api           string // build because of which api?
	ApiUrl        string
	Arguments     string // json encoded arguments to be passed to the build script

	Name           string
	BuildVariables []BuildVariantVariable `gorm:"-"`
	ParentID       uint
	Parent         *Build

	Checksum // checksums for the artifact package

	NumTotal   uint    // total number of tests executed
	NumSuccess uint    // number of passed tests
	NumFailed  uint    // number of failed tests
	Tests      []*Test // reference to the associated tests
}

func (bi *Build) CalcDuration() {
	if bi.Status == "finished" || bi.Status == "failed" {
		d := bi.FinishedAt.Sub(bi.StartedAt)
		bi.Duration = d.Truncate(time.Millisecond).String()
	} else {
		bi.Duration = "-"
	}
}

// structure representing a single run unit test
type Test struct {
	ID           uint          `gorm:"primary_key"` // database ID
	Duration     time.Duration // how long did the test run
	Name         string        // Name of the test
	SuiteName    string        // Name of the Testsuite the test is associated with (or empty)
	Result       string        // The result of the test, see TestResult* constants
	Log          string        // Log-output of the test
	BuildID      uint          // associated with this build
	RepositoryID uint          `gorm:"-"` // RepositoryID for API returns, unused otherwise
}

// identify the last successful Database Migration, to eliminate start-up cost at next start-up
type Migration struct {
	ID      uint // useless ID
	Version int  // Known value identifying the last (successful) Migration
}

const TestResultOk = "ok"
const TestResultFail = "fail"
const TestResultError = "error"
const TestResultSkip = "skip"
const TestResultUnknown = "unknown"

func (run *Build) BuildCalcTotalTests() {
	run.NumTotal = 0
	run.NumSuccess = 0
	run.NumFailed = 0

	for _, v := range run.Tests {
		run.NumTotal++
		switch v.Result {
		case TestResultOk:
			run.NumSuccess++
		case TestResultError, TestResultFail:
			run.NumFailed++
		}
	}
}

// log entry for webhook events
type WebHookLog struct {
	ID             uint `gorm:"primary_key"`
	CreatedAt      time.Time
	Status         string     // of the webhook event, successful or not?
	API            string     // which api endpoint was used?
	Request        string     // the request body of the webhook request
	ResponseBody   string     // body of the response sent by mvoCI
	ResponseStatus int        // status of the response (ideally 200 if successful)
	Repository     Repository // link to the Repository
	RepositoryID   uint
	Build          Build // link to the Build
	BuildID        uint
}

type SshKey struct {
	CreatedAt    time.Time
	Host         string `gorm:"primary_key"`
	Hostname     string
	User         string
	IdentityFile string
	Port         int
	ProxyJump    string
}

func SshKeysToString(keys []SshKey) string {
	var b strings.Builder
	for _, v := range keys {
		v.ToString(&b)
	}

	Console.Warn("Ssh Keys:" + b.String())

	return b.String()
}

func (s SshKey) ToString(b *strings.Builder) {
	if s.Host == "" || s.IdentityFile == "" {
		Console.Log("SSH Key invalid")
		return
	}

	b.WriteString("host ")
	b.WriteString(s.Host)

	b.WriteString("\n  IdentityFile ")
	b.WriteString(s.IdentityFile)

	if s.Hostname != "" {
		b.WriteString("\n  Hostname ")
		b.WriteString(s.Hostname)
	}

	if s.User != "" {
		b.WriteString("\n  User ")
		b.WriteString(s.User)
	}

	if s.Port > 0 {
		b.WriteString("\n  Port ")
		b.WriteString(strconv.Itoa(s.Port))
	}

	if s.ProxyJump != "" {
		b.WriteString("\n  ProxyJump ")
		b.WriteString(s.ProxyJump)
	}
	b.WriteString("\n")
}

// create special group "Everyone" and add the user, or just add the user if it already exists
func CreateGroupEveryone(u User) error {
	var grp Group
	DB.Preload("User").Where("name = ?", GroupEveryone).First(&grp)
	if grp.Name == GroupEveryone && grp.ID > 0 {
		if grp.AmIMember(u) {
			return errors.New("Group Everyone already exists and you are member of it")
		}

		grp.User = append(grp.User, u)
		DB.Save(grp)

		return nil
	} else {
		grp.Name = GroupEveryone
		grp.User = []User{u}
		DB.Create(grp)
		return nil
	}
}

// returns whether or not the user is member of the group. If the User field is
// unpopulated it tries to query the members for the database.
func (g Group) AmIMember(user User) bool {
	if g.ID <= 0 || user.ID <= 0 {
		return false
	}

	var members []User = g.User
	if len(g.User) == 0 {
		var grp Group
		DB.Model(&Group{}).Preload("User").Where("id = ?", g.ID).First(&grp)
		if grp.ID > 0 {
			members = grp.User
		}
	}

	if len(members) == 0 {
		return false
	}

	for _, v := range members {
		if v.ID == user.ID {
			return true
		}
	}

	return false
}

// Remove a member from the group and remove the group if it was the last
// member.
func (g Group) Leave(user User) error {
	if g.ID <= 0 || user.ID <= 0 {
		return errors.New("objects missing")
	}

	DB.Table("user_groups").Delete(nil, "group_id = ? AND user_id = ?", g.ID, user.ID)

	var cnt int64
	DB.Table("user_groups").Where("group_id = ?", g.ID).Count(&cnt)
	if cnt == 0 {
		DB.Delete(Acl{}, "group_id = ?", g.ID)
		DB.Delete(Group{}, "id = ?", g.ID)
		return nil
	}

	return nil
}

// parses the database configuration values and sets a configuration string
// for GORM accordingly, so it can set up the database connection.
// used by DBConnect
func connection_settings() string {
	var x string
	switch Cfg.Database.Provider {
	case "sqlite3":
		x = Cfg.Database.Filename
	case "mysql":
		x = fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True",
			Cfg.Database.Username,
			Cfg.Database.Password,
			Cfg.Database.Host,
			Cfg.Database.Dbname)
	case "postgres":
		x = fmt.Sprintf("user=%s password=%s host=%s dbname=%s port=%d sslmode=%s",
			Cfg.Database.Username,
			Cfg.Database.Password,
			Cfg.Database.Host,
			Cfg.Database.Dbname,
			Cfg.Database.Port,
			Cfg.Database.PostgresSSL)
	case "mssql":
		x = fmt.Sprintf("sqlserver://%s:%s@%s:%d?database=%s",
			Cfg.Database.Username,
			Cfg.Database.Password,
			Cfg.Database.Host,
			Cfg.Database.Port,
			Cfg.Database.Dbname)
	}
	return x
}

type gormOpenFunc func(string) gorm.Dialector

const MIGRATION_GROUPS int = 1
const MIGRATION_BUILD_SCRIPTS int = 2
const MIGRATION_BUILD_PARENT_ID int = 3

func DBMigrator() error {
	var mig Migration
	mig.Version = 0
	DB.Order("version desc").First(&mig)

	// 2024-07-06 always add duration to build which have none.
	var builds []Build
	DB.Where("duration = ?", "").Find(&builds)
	Console.Logf("Recalculating Build Durations for %d Builds", len(builds))
	for _, v := range builds {
		v.CalcDuration()
		DB.Save(&v)
	}

	var root User
	var rootCnt int64
	DB.Where("id=1").First(&root).Count(&rootCnt)
	if rootCnt > 1 {
		return errors.New("More than one root-user found, please fix users with ID=1")
	}

	// 2021-12-18 add groups, create group everyone and include every user in it
	if mig.Version < MIGRATION_GROUPS {
		var publicGroup Group
		var publicCount int64
		DB.Where("name=?", "Everyone").First(&publicGroup).Count(&publicCount)
		if publicCount == 0 {
			var us []User
			DB.Model(&User{}).Find(&us)
			publicGroup.Name = "Everyone"
			publicGroup.User = us
			DB.Save(&publicGroup)

			//db.Model ( &User{} ).Association("Group").Append([]*Group{publicGroup} )
		}
	}

	// 2024-03-28 remove BuildScripts -> migrate the push-build script
	// back into the repository table
	if mig.Version < MIGRATION_BUILD_SCRIPTS {
		Console.Log("Migrating Builds Scripts: Removing Build Scripts, add it back to Repository Table")
		DB.Exec(`UPDATE repositories SET build_script = subquery.shell_script
FROM ( SELECT shell_script, repository_id FROM build_scripts WHERE event_type = "push") subquery
WHERE id = subquery.repository_id and (build_script = "" or build_script is NULL);`)
	}

	if mig.Version < MIGRATION_BUILD_PARENT_ID {
		Console.Log("Migrating Builds: Setting parent_id = 0")
		DB.Exec("UPDATE builds SET parent_id = 0 WHERE parent_id IS NULL")
		var b []Build
		DB.Find(&b)
		for _, v := range b {
			v.CalcDuration()
			DB.Save(v)
		}
	}

	migOld := mig
	mig.Version = MIGRATION_BUILD_PARENT_ID
	if migOld.Version != mig.Version {
		DB.Save(&mig)
	}
	return nil
}

func DBMigratorReset() error {
	var mig Migration
	mig.Version = 0
	DB.Order("version desc").First(&mig)

	mig.Version = 0
	DB.Save(&mig)

	return nil
}

// creates the database connection and creates the tables if they don't exist already
// depends on the following settings in the configuration ile:
//   - database_provider : sets the type of database server, i.e.
//     postgres, sqlite3, mssql, mysql
//   - database_username : username for the database connection
//   - database_password : the password for the database connection
//   - database_host     : if mysql, postgres or mssql - host of the database
//   - database_filename : only sqlite3 - file (can be :memory)
//   - database_port     : communication port of the DB
//   - database_dbname   : the name of the database to use
func DBConnect(resetDbMigrator bool) error {
	var openFn gormOpenFunc
	switch Cfg.Database.Provider {
	case "sqlite3":
		openFn = sqlite.Open
	case "mysql":
		openFn = mysql.Open
	case "postgres":
		openFn = postgres.Open
	case "mssql":
		openFn = sqlserver.Open
	}

	gormcfg := &gorm.Config{}
	if Cfg.Debug {
		gormcfg = &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		}
	}
	db, err := gorm.Open(openFn(connection_settings()), gormcfg)
	if err != nil {
		Console.Fail("FB: Failed to Open DB Connection: ", err)
		return err
	}

	err = db.AutoMigrate(
		&User{},
		&AuthProvider{},
		&Group{},
		&LoginToken{},
		&Repository{},
		&Build{},
		&BuildVariant{},
		&BuildVariantVariable{},
		&WebHookLog{},
		&OauthToken{},
		&Acl{},
		&SshKey{},
		&Test{},
		&Migration{},
	)
	if err != nil {
		Console.Fail("DB: Failed to Automigrate (create not existing tables)", err)
		return err
	}

	DB = db
	if resetDbMigrator {
		err = DBMigratorReset()
		if err != nil {
			return err
		}
	}

	err = DBMigrator()
	if err != nil {
		Console.Fail("DB: Failed to mvoCI-migrate to next version", err)
		return err
	}

	return nil
}
