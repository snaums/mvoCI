package core

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"
)

const mailFile = "~/.config/mvoCI/secret"

var mailEnable bool = false

func init() {
	path, err := exec.LookPath("swaks")
	if err != nil || path == "" {
		fmt.Println("[fail] Swaks binary not found, disabling mail support")
		mailEnable = false
	} else {
		mailEnable = true
	}
}

type MailEncrypt int

const (
	NoEncrypt MailEncrypt = iota
	Tls
	StartTls
)

type EmailServerSettings struct {
	Host       string
	Port       int
	Username   string
	Password   string
	From       string
	Encryption MailEncrypt
}

func (e EmailServerSettings) IsEmpty() bool {
	return e.Host == "" || e.Port == 0 || e.From == ""
}

type emailValues struct {
	From    string
	To      string
	Subject string
	Body    string
}

func mailResolvePath(path string) (string, error) {
	if strings.HasPrefix(path, "~") {
		usr, err := user.Current()
		if err != nil {
			return "", err
		}

		path = usr.HomeDir + path[1:]
	}
	return path, nil
}

func mailGenerateKey(path string) ([]byte, error) {
	secret := make([]byte, 32)
	_, err := rand.Read(secret)
	if err != nil {
		return []byte{}, err
	}

	path, err = mailResolvePath(path)
	if err != nil {
		return []byte{}, err
	}

	err = os.MkdirAll(filepath.Dir(path), 0770)
	if err != nil {
		return []byte{}, err
	}

	err = os.WriteFile(path, secret, 0660)
	if err != nil {
		return []byte{}, err
	}

	return secret, nil
}

func MailPasswordSet(password string) (string, error) {
	key, err := mailReadKey(mailFile)
	if err != nil {
		key, err = mailGenerateKey(mailFile)
		if err != nil {
			return "", err
		}
	}

	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return "", err
	}
	plainText := []byte(password)
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}

	iv := make([]byte, 12)
	_, err = rand.Read(iv)
	if err != nil {
		return "", err
	}
	cipherText := gcm.Seal(iv, iv, plainText, nil)
	return base64.StdEncoding.EncodeToString(cipherText), nil
}

func mailReadKey(path string) ([]byte, error) {
	path, err := mailResolvePath(path)
	if err != nil {
		return []byte{}, err
	}

	key, err := os.ReadFile(path)
	if err != nil {
		return []byte{}, errors.New("Unable to send Email")
	}

	return key, nil
}

func mailPasswordGet(server EmailServerSettings) (string, error) {
	if server.Password != "" {
		cipherText, err := base64.StdEncoding.DecodeString(server.Password)
		if err != nil {
			return "", errors.New("Unable to send Email")
		}

		key, err := mailReadKey(mailFile)
		if err != nil {
			return "", errors.New("Unable to send Email")
		}
		aes_block, err := aes.NewCipher([]byte(key))
		if err != nil {
			return "", errors.New("Unable to send Email")
		}

		gcm, err := cipher.NewGCM(aes_block)
		if err != nil {
			return "", errors.New("Unable to send Email")
		}

		nonceSize := gcm.NonceSize()
		nonce, ciphertext := cipherText[:nonceSize], cipherText[nonceSize:]

		p, err := gcm.Open(nil, []byte(nonce), []byte(ciphertext), nil)
		if err != nil {
			return "", errors.New("Unable to send Email")
		}
		return string(p), nil
	}
	return "", nil
}

func SendMailSwaks(s EmailServerSettings, from string, to string, subject string, msg string) error {
	if !mailEnable {
		return errors.New("Swaks not found")
	}

	if s.Port <= 0 {
		s.Port = 587
	}

	if s.Host == "" || from == "" || to == "" {
		return errors.New("Email Settings Not Set")
	}

	args := []string{
		"--from", from,
		"--to", to,
		"--server", s.Host,
		"--port", strconv.Itoa(s.Port),
		"--header", subject,
		"--body", msg,
		// encryption
		// auth
	}
	switch s.Encryption {
	case Tls:
		args = append(args, "--tls")
	case StartTls:
		args = append(args, "--starttls")
	case NoEncrypt:
	}

	pswd, err := mailPasswordGet(s)
	if err != nil {
		return err
	}

	if pswd != "" {
		args = append(args, "--auth-user", s.Username, "--auth-password", pswd)
	}

	cmd := exec.Command("swaks", args...)
	var out, errout strings.Builder
	cmd.Stdout = &out
	cmd.Stderr = &errout
	err = cmd.Run()
	if err != nil {
		Console.Warn("Swaks failed with statuscode ", cmd.ProcessState.ExitCode)
		Console.Warn("ErrLogs: ", errout.String())
		Console.Warn("OutLogs: ", out.String())
	}

	return nil
}
