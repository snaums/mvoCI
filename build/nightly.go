package build

import (
	"codeberg.org/snaums/mvoCI/core"
	"codeberg.org/snaums/mvoCI/events"
)

type TimerNightlyBuildHandler struct{}

func (h TimerNightlyBuildHandler) HandlesType(tp events.EventType) bool {
	return tp == events.OnTimerNightlyBuild
}

func (h TimerNightlyBuildHandler) Handle(ev events.Event) bool {
	core.Console.Warn("[NightlyBuildTimer] Received Nightly Build Timer")

	// a payload is attached when the admin interface is used to test nightly build events
	if ev.Payload() == nil {
		// TODO hour should come from config
		events.SetNightlyBuildTimer(2)
	}
	WorkerStartNightlyBuilds()
	return true
}

func init() {
	events.AddHandler(events.OnTimerNightlyBuild, TimerNightlyBuildHandler{})
}

func WorkerStartNightlyBuilds() {
	// find all repository that received webhooks and enqueue a build
	var repos []core.Repository
	core.DB.Where("nightly = true AND ((nightly_when_changed = true AND nightly_has_changes = true) OR nightly_when_changed = false)").Find(&repos)
	for _, r := range repos {
		core.Console.Log("Starting nightly builds for Repo ", r.Name)
		BuildNightly(r)
		// reset the has changes state
		core.DB.Model(&r).Update("nightly_has_changes", false)
	}
}
