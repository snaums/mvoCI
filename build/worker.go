// There are n build workers, which are started at the start of mvoCI.
// They execute BuildWorker. When a new build object is spawned into the
// build queue / channel, it is fetched (from git repo), the buildscript
// is placed in the folder and executed with bash. Then the directory
// is zipped and later removed.
//
// Two builds cannot be ongoing on the same repo at the same time.
package build

import (
	"encoding/json"
	"errors"
	"math/rand"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"

	"codeberg.org/snaums/mvoCI/core"
	"codeberg.org/snaums/mvoCI/events"
	"codeberg.org/snaums/mvoCI/testing"

	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

// thread identifiers to the worker threads
var Workers map[int]core.Thread

// channel / build queue for communication with the build workers
var InChannel chan core.Build

// string builder wrapper
type mockerBuilder struct {
	strings.Builder
	Build *core.Build
}

// append a string to the builder
func (m *mockerBuilder) Write(p []byte) (n int, err error) {
	m.append(string(p))
	return len(p), nil
}

// append a string to the builder
func (m *mockerBuilder) append(args ...string) {
	for _, str := range args {
		m.WriteString(str)
	}
	if core.Cfg.Build.LiveLog {
		m.Build.Log = m.String()
		// Do not log every update in DEBUG mode
		core.DB.Session(&gorm.Session{
			Logger: core.DB.Logger.LogMode(logger.Silent),
		}).Save(&m.Build)
	}
}

func (m *mockerBuilder) RunAndLogCmd(cmd *exec.Cmd) error {
	//out, err := cmd.CombinedOutput()
	//m.Write(out)
	cmd.Stdout = m
	cmd.Stderr = m
	err := cmd.Run()
	return err
}

// Mutex for synchronizing the workers.
// The workers cannot at the same time all get a repo from the channel, try to find out if its already being build and then start building it.
// That would lead to the same repo being build at the same time, which is handled rather unwell at the moment.
var mux sync.Mutex

// sets up the worker threads in the given quantitiy
func SetupWorkerThreads() {
	numThreads := core.Cfg.ParallelBuilds

	// cleanup build directory
	os.RemoveAll(core.Cfg.Directory.Repo)

	InChannel = make(chan core.Build, 50)
	Workers = make(map[int]core.Thread)
	for i := 0; i < numThreads; i++ {
		Workers[i] = core.Pthread_create(BuildWorker)
	}

	// builds, which where started or enqueued before the server was shut down -> enqueue them again
	// and try to (re)build
	var b []core.Build
	core.DB.Model(&core.Build{}).Preload("Repository").Where("status = ? OR status = ?", "enqueued", "started").Find(&b)
	for _, bs := range b {
		// enqueue
		//core.DB.Model (&bs).Association("Repository").Find(&bs.Repository);
		bs.Status = "enqueued"
		core.DB.Save(&bs)
		InChannel <- bs
	}
}

// worker function: loop indefinitely and retrieve a new build from the build
// queue and build it.
func BuildWorker() {
	core.Console.Log("Starting BuildWorker")
	var b core.Build
	var cnt int64
	for {
		b = core.Build{}
		b = <-InChannel
		mux.Lock()
		core.DB.Model(&core.Build{}).Where("repository_id = ? AND status = 'started'", b.RepositoryID).Count(&cnt)
		// if there is another thread building the same repo, reject it and back off
		if cnt > 0 {
			core.Console.Log("Rejecting Build, another one on the same repo is running")
			InChannel <- b
			mux.Unlock()
			time.Sleep(5 * time.Second)
		} else {
			var dbBuild core.Build
			core.DB.Select("id").Where("id=?", b.ID).First(&dbBuild)
			if dbBuild.ID <= 0 {
				core.Console.Log("Build no longer exists")
				mux.Unlock()
			} else {
				b.Status = "started"
				b.StartedAt = time.Now()
				core.DB.Save(&b)
				mux.Unlock()
				core.Console.Log("Starting Build on ", b.Repository.Name)
				log := mockerBuilder{Build: &b}

				var dir string
				var try = 5
				var err = false
				for dir == "" && !err {
					dir = core.Cfg.Directory.Repo + dirNameFromRepo(b.Repository) + "-" + strconv.Itoa(rand.Intn(100000))
					if st, errstat := os.Stat(dir); errstat == nil && st.IsDir() {
						dir = ""
						try--
						if try <= 0 {
							log.append("> Cannot find a suitable directory\n")
							err = true
							break
						}
					}
				}

				var ok bool = false
				if !err {
					_, ok = WorkerGoGet(&b, dir, &log)
				}

				b.FinishedAt = time.Now()
				if err || !ok {
					log.append("> Build failed\n")
					b.Log = log.String()
					b.Status = "failed"

					core.DB.Save(&b)

					WorkerGoCleanup(&b, &log, dir)
				} else {
					log.append("> Build successful\n")
					b.Log = log.String()
					b.Status = "finished"
					b.CalcDuration()

					core.DB.Save(&b)
				}
				core.Console.Log("Finished Build on ", b.Name)
				events.SendEvent(events.OnBuildFinished, b)
			}
		}
	}
}

// starts building  a build, i.e. clone the repository, get the correct branch
// and commit, and calling every line of the build-script (WorkerGoBuild)
// if this all succeeds: set the status to "finished", "failed" otherwiese.
func WorkerGoGet(b *core.Build, dir string, log *mockerBuilder) (*mockerBuilder, bool) {
	// create repo directory
	log.append("> Started build of ", b.Repository.Name, "\n")

	_ = os.Mkdir(core.Cfg.Directory.Repo, 0777)
	_ = os.Mkdir(core.Cfg.Directory.Build, 0777)

	log.append("> Cloning repository\n")

	out, err := exec.Command("git", "clone", b.Repository.CloneUrl, dir).CombinedOutput()
	log.append(string(out))
	if err != nil {
		log.append("> Could not clone the repository :/\nLog: ", err.Error())
		return log, false
	}

	log.append("> Switching branch to ", b.Branch, "\n")
	cmd := exec.Command("git", "checkout", b.Branch)
	cmd.Dir = dir
	out, err = cmd.CombinedOutput()
	log.append(string(out))
	if err != nil {
		log.append("> Could not switch to branch: ", err.Error(), "\n")
		return log, false
	}

	cmd = exec.Command("git", "rev-parse", "HEAD")
	cmd.Dir = dir
	out, err = cmd.CombinedOutput()
	if err != nil {
		log.append("> Could not find out the current commit SHA: ", err.Error(), "\n")
		return log, false
	}

	log.append("> HEAD is ", string(out), "\n")
	if b.TagName != "" {
		log.append("> Switching to tag ", b.TagName, "\n")
		cmd = exec.Command("git", "checkout", b.TagName)
		cmd.Dir = dir
		out, err = cmd.CombinedOutput()
		log.append(string(out))
		if err != nil {
			log.append("> Could fetch the appropriate commit :/", err.Error(), "\n")
			return log, false
		}

		cmd = exec.Command("git", "rev-parse", "HEAD")
		cmd.Dir = dir
		out, err = cmd.CombinedOutput()
		if err != nil {
			log.append("> Could not find out the current commit SHA: ", err.Error(), "\n")
			return log, false
		}

		if b.CommitSha != "" && b.CommitSha != strings.TrimSpace(string(out)) {
			log.append("> Invalid Commit Sha given, using ", string(out), "from tag\n")
		}
		b.CommitSha = strings.TrimSpace(string(out))
	} else {
		if len(b.CommitSha) > 0 && b.CommitSha != string(out) {
			b.CommitSha = strings.TrimSpace(b.CommitSha)
			log.append("> Fetching Commit ", b.CommitSha, "\n")

			cmd = exec.Command("git", "checkout", b.CommitSha)
			cmd.Dir = dir
			out, err = cmd.CombinedOutput()
			log.append(string(out))
			if err != nil {
				log.append("> Could fetch the appropriate commit :/", err.Error(), "\n")
				return log, false
			}
		} else {
			b.CommitSha = strings.TrimSpace(string(out))
			log.append("> Building HEAD\n")
		}
	}

	// read author and commit-message from the git repository
	if b.CommitAuthor == "" || b.CommitMessage == "" {
		cmd = exec.Command("git", "show", "--format=%cn|%s", "--no-abbrev-commit", "--no-notes", "--no-patch")
		cmd.Dir = dir
		out, err = cmd.CombinedOutput()
		if err != nil {
			core.Console.Warn("Failed to read output: ", err.Error())
		}
		o := strings.Split(string(out), "\n")
		o2 := strings.Split(o[0], "|")
		b.CommitAuthor = strings.TrimSpace(o2[0])
		b.CommitMessage = strings.TrimSpace(o2[1])
	}

	return WorkerGoBuild(b, log, dir)
}

// Build the repo by executing every line of the build script
func WorkerGoBuild(b *core.Build, log *mockerBuilder, dir string) (*mockerBuilder, bool) {
	log.append("> Starting Build ... \n")

	ShellScript := b.Repository.BuildScript
	buildScript := []byte("set -e\n" + strings.ReplaceAll(ShellScript, "\r", "\n"))
	buildScriptName := dir + "/mvoBuild.sh"
	err := os.WriteFile(buildScriptName, buildScript, 0777)
	if err != nil {
		log.append("> Could not create build script\n")
		return log, false
	}

	e := execBuildScript("mvoBuild.sh", dir, b, log)
	if !e {
		return log, false
	}

	return WorkerGoZip(b, log, dir)
}

// Zips the repo directory with the builds
func WorkerGoZip(b *core.Build, log *mockerBuilder, dir string) (*mockerBuilder, bool) {
	if b.Repository.KeepBuilds {
		var ZipName = dirNameFromRepo(b.Repository) + "_" +
			strconv.Itoa(int(b.ID)) + ".tar." +
			core.Cfg.CompressionMethod

		ZipName = strings.ReplaceAll(ZipName, " ", "_")
		var ZipFile = "../../" + core.Cfg.Directory.Build + ZipName

		os.RemoveAll(dir + "/.git")
		log.append("> Zipping directory ", dir, " to ", ZipFile, "\n")
		cmd := exec.Command("tar", "--auto-compress", "--create", "-v", "--file="+ZipFile, ".")
		cmd.Dir = dir
		out, err := cmd.CombinedOutput()
		log.append(string(out))
		if err != nil {
			log.append("Zipping failed: ", err.Error(), "\n")
			return log, false
		}

		b.Zip = ZipName
		err = WorkerChecksums(b, log, ZipFile, dir)
		if err != nil {
			log.append("Checksum creation failed: ", err.Error(), "\n")
			return log, false
		}
	} else {
		b.Zip = ""
		log.append("> Skipping zipping because of repo-local configuration\n")
	}
	return WorkerGoCleanup(b, log, dir)
}

func WorkerChecksums(b *core.Build, log *mockerBuilder, ZipFile string, dir string) error {
	log.append("> Calculating SHA1\n")
	cmd := exec.Command("sha1sum", ZipFile)
	cmd.Dir = dir
	out, err := cmd.CombinedOutput()
	log.append(string(out))
	if err != nil {
		return err
	}

	sumSplit := strings.Split(string(out), " ")
	if len(sumSplit) < 2 {
		return errors.New("Invalid Format")
	}
	b.Sha1 = sumSplit[0]

	log.append("> Calculating MD5\n")
	cmd2 := exec.Command("md5sum", ZipFile)
	cmd2.Dir = dir
	out2, err := cmd2.CombinedOutput()
	log.append(string(out2))
	if err != nil {
		return err
	}

	sum2Split := strings.Split(string(out2), " ")
	if len(sum2Split) < 2 {
		return errors.New("Invalid Format")
	}
	b.Md5 = sum2Split[0]
	log.append("> SHA1: ", b.Sha1, "\n")
	log.append("> MD5 : ", b.Md5, "\n")

	return nil
}

// removes the repo directory
func WorkerGoCleanup(b *core.Build, log *mockerBuilder, dir string) (*mockerBuilder, bool) {
	log.append("> Cleaning up build directory\n")
	os.RemoveAll(dir)
	return log, true
}

// execute the shell build script
func execBuildScript(file string, dir string, b *core.Build, log *mockerBuilder) bool {
	log.append("> Executing build script '", file, "'\n")
	cmd := exec.Command("bash", file)
	cmd.Env = os.Environ()
	cmd.Env = append(cmd.Env, "VERSION="+b.CommitSha)
	cmd.Env = append(cmd.Env, "MVO_REPO="+b.Repository.Name)
	cmd.Env = append(cmd.Env, "MVO_BRANCH="+b.Branch)
	cmd.Env = append(cmd.Env, "MVO_EVENT="+b.Event)
	cmd.Env = append(cmd.Env, "MVO_COMMIT_SHA="+b.CommitSha)
	cmd.Env = append(cmd.Env, "MVO_COMMIT_AUTHOR="+b.CommitAuthor)
	cmd.Env = append(cmd.Env, "MVO_COMMIT_MESSAGE="+b.CommitMessage)
	switch b.Event {
	case nightly_event:
		cmd.Env = append(cmd.Env, "MVO_NIGHTLY=1")
	case release_event:
		cmd.Env = append(cmd.Env, "MVO_RELEASE=1")
	}

	for _, v := range b.BuildVariables {
		cmd.Env = append(cmd.Env, "MVO_VARIANT_"+strings.TrimSpace(v.Key)+"="+strings.TrimSpace(v.Value))
	}

	var args map[string]string
	err := json.Unmarshal([]byte(b.Arguments), &args)
	if err != nil {
		log.append("> ! Error parsing arguments to the build script\n")
	} else {
		for key, value := range args {
			cmd.Env = append(cmd.Env, "MVO_ARG_"+strings.ReplaceAll(key, " ", "_")+"="+value)
		}
	}

	cmd.Dir = dir

	err = log.RunAndLogCmd(cmd)
	if err != nil {
		log.append("> Execution failed with error: ", err.Error(), "\n")
		return false
	}

	return parseTestReports(dir, b, log)
}

func parseTestReports(dir string, b *core.Build, log *mockerBuilder) bool {
	err := testing.TestParserRun(dir, b)
	if err != nil {
		core.Console.Log("Failed to get Test Report: ", err.Error())
		return true
	}

	return true
}

// returns the name of the repository for usage as directory name
func dirNameFromRepo(r core.Repository) string {
	return r.Name
}
