package build

import (
	"encoding/json"
	"time"

	"codeberg.org/snaums/mvoCI/core"
)

const nightly_event string = "nightly"
const release_event string = "release"

func BuildNightly(r core.Repository) uint {
	return BuildSimple(r, nightly_event)
}

// build a repository now (the latest commit, default branch)
func BuildNow(r core.Repository) uint {
	return BuildSimple(r, "user build")
}

func BuildSimple(r core.Repository, event string) uint {
	b := core.Build{Log: "", CommitSha: "", FinishedAt: time.Unix(0, 0),
		Branch: r.DefaultBranch, Repository: r, Status: "enqueued", Event: event}
	buildStartVariants(&b)
	return b.ID
}

// TODO this api gets ridiculous, needs some rework
// Build a release of a given repository
func BuildRelease(
	r core.Repository,
	branch string,
	tagName string,
	author string,
	url string,
	message string,
	event string,
	api string,
	apiUrl string,
	args map[string]string) uint {

	jsonargs, err := json.Marshal(args)
	if err != nil {
		core.Console.Warn("Error storing arguments: ", err.Error())
		return 0
	}

	b := core.Build{Log: "", CommitSha: "", FinishedAt: time.Unix(0, 0),
		Branch: branch, Repository: r, Status: "enqueued",
		CommitAuthor: author, CommitUrl: url, CommitMessage: message, TagName: tagName,
		Event: event, Api: api, ApiUrl: apiUrl,
		Arguments: string(jsonargs)}
	buildStartVariants(&b)
	return b.ID
}

// intermediate layers are always nice.
func BuildSpecificOrNightly2(
	r core.Repository,
	branch string,
	sha string,
	author string,
	url string,
	message string,
	event string,
	args map[string]string) uint {

	if r.Nightly && r.NightlyWhenChanged {
		core.DB.Model(&r).Update("nightly_has_changes", true)
		return 0
	}

	return BuildSpecific2(r, branch, sha, author, url, message, event, args)
}

// TODO this api gets ridiculous, needs some rework
// build a specific repo, branch and commit, already having found out the commit-message, commit-url and build-event (most likely hooked)
func BuildSpecific2(
	r core.Repository,
	branch string,
	sha string,
	author string,
	url string,
	message string,
	event string,
	args map[string]string) uint {

	jsonargs, err := json.Marshal(args)
	if err != nil {
		core.Console.Warn("Error storing arguments: ", err.Error())
		return 0
	}

	b := core.Build{Log: "", CommitSha: sha, FinishedAt: time.Unix(0, 0),
		Branch: branch, Repository: r, Status: "enqueued",
		CommitAuthor: author, CommitUrl: url, CommitMessage: message,
		Event: event, Arguments: string(jsonargs)}
	buildStartVariants(&b)
	return b.ID
}

// build a specific repo, branch and commit
func BuildSpecific3(r core.Repository, branch string, sha string, u core.User) uint {
	b := core.Build{Log: "", CommitSha: sha, FinishedAt: time.Unix(0, 0),
		Branch: branch, Repository: r, Status: "enqueued"}
	buildStartVariants(&b)
	return b.ID
}

// build a repo given the secret provided by a version control service in its hook message
func BuildHookedFromSecret3(secret string, branch string, sha string) uint {
	var r core.Repository
	core.DB.Where("secret = ?", secret).First(&r)
	if r.ID <= 0 {
		return 0
	}
	return BuildSpecific3(r, branch, sha, core.User{})
}

// rebuild a given build
func ApiReBuild(b *core.Build) uint {
	bn := core.Build{Log: "", CommitSha: b.CommitSha,
		FinishedAt: time.Unix(0, 0), Branch: b.Branch,
		Repository: b.Repository, Status: "enqueued", Event: "API Rebuild",
		CommitAuthor: b.CommitAuthor, CommitMessage: b.CommitMessage,
		CommitUrl: b.CommitUrl, Arguments: b.Arguments}
	core.DB.Create(&bn)
	EnqueueBuild(bn)
	return bn.ID
}

// rebuild a given build
func ReBuild(b *core.Build) uint {
	bn := core.Build{Log: "", CommitSha: b.CommitSha,
		FinishedAt: time.Unix(0, 0), Branch: b.Branch,
		Repository: b.Repository, Status: "enqueued", Event: "User Rebuild",
		CommitAuthor: b.CommitAuthor, CommitMessage: b.CommitMessage,
		CommitUrl: b.CommitUrl, Arguments: b.Arguments}
	core.DB.Create(&bn)
	EnqueueBuild(bn)
	return bn.ID
}

func BuildNowVariant(v core.BuildVariant, r core.Repository) uint {
	b := core.Build{Log: "", CommitSha: "", FinishedAt: time.Unix(0, 0),
		Branch: r.DefaultBranch, Repository: r, Status: "enqueued", Event: "User Build",
		Name: r.Name + "-" + v.Name, BuildVariables: v.Variables,
	}
	core.DB.Create(&b)
	EnqueueBuild(b)
	return b.ID
}

func buildStartVariants(b *core.Build) {
	b.Name = b.Repository.Name
	core.DB.Create(b)
	EnqueueBuild(*b)

	var variants []core.BuildVariant
	core.DB.Preload("Variables").Where("repository_id = ?", b.Repository.ID).Find(&variants)
	for _, v := range variants {
		var bcopy = *b
		bcopy.Name = bcopy.Repository.Name + "-" + v.Name
		bcopy.BuildVariables = v.Variables
		bcopy.ID = 0
		bcopy.ParentID = b.ID
		core.DB.Create(&bcopy)
		EnqueueBuild(bcopy)
	}
}

// enqueue the build into the build-queue (channel)
func EnqueueBuild(b core.Build) {
	core.Console.Log("Enqueuing build", b.Name)
	InChannel <- b
	// the workers have the other end of the channel and read builds from it
}
