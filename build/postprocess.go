package build

import (
	"strings"

	"codeberg.org/snaums/mvoCI/core"
	"codeberg.org/snaums/mvoCI/events"
)

func WorkerSendMail(b core.Build) {
	core.Console.Warn("b core.Build", b.Name, " ", b.RepositoryID)
	var r = b.Repository
	if r.ID <= 0 {
		core.Console.Warn("Repo ID is null")
		return
	}
	if r.SendMail && !core.Cfg.Email.IsEmpty() {
		var u core.User
		core.DB.Where("id = ?", r.UserID).Select("email").First(&u)
		if u.Email != "" {
			var state string
			switch b.Status {
			case "finished":
				state = "succeeded"
			case "failed":
				state = "failed"
			default:
				return
			}
			var headline = "[mvoCI] Build " + state + " for Repo " + r.Name

			var body = b.Log
			var lines = strings.Split(b.Log, "\n")
			if len(lines) > 50 {
				var m mockerBuilder
				for _, l := range lines[len(lines)-50:] {
					m.append(l + "\n")
				}

				body = m.String()
			}

			err := core.SendMailSwaks(core.Cfg.Email, core.Cfg.Email.From, u.Email, headline, body)
			if err != nil {
				core.Console.Fail("Failed to send email: ", err)
			}
		}
	}
}

type WorkerFinishedMail struct{}

func (w WorkerFinishedMail) HandlesType(tp events.EventType) bool {
	return tp == events.OnBuildFinished
}

func (w WorkerFinishedMail) Handle(ev events.Event) bool {
	b, ok := ev.Payload().(core.Build)
	if !ok {
		core.Console.Fail("[event] Received onBuildFinished, but payload was not a build")
		return false
	}

	//if b != nil {
	WorkerSendMail(b)
	//} else {
	//    core.Console.Warn("is nil")
	//}

	// does not consume the build object
	return false
}

func init() {
	events.AddHandler(events.OnBuildFinished, WorkerFinishedMail{})
}
