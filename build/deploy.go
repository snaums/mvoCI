package build

import (
	"codeberg.org/snaums/mvoCI/core"
	"codeberg.org/snaums/mvoCI/events"
	"codeberg.org/snaums/mvoCI/hook"
)

type WorkerDeploy struct{}

func (w WorkerDeploy) HandlesType(tp events.EventType) bool {
	return tp == events.OnBuildFinished
}

func (w WorkerDeploy) Handle(ev events.Event) bool {
	core.Console.Warn("[onBuildFinished] Deploy hander")

	b, ok := ev.Payload().(core.Build)
	if !ok {
		core.Console.Fail("[event] Received onBuildFinished, but payload was not a build")
		return false
	}

	core.Console.Fail("b.Status ", b.Status)
	if b.Status == "finished" {
		// only gitea has release-api event implemented, atm. (2020-06-27)
		if (b.Api == "gitea" || b.Api == "forgejo") && b.Event == "release" {
			go WorkerPushRelease(b)
		}
	}

	return false
}

func init() {
	events.AddHandler(events.OnBuildFinished, WorkerDeploy{})
}

// atm for gitea we can push release builds to the code server using OAuth2 authentication (set-up beforehand by the user)
func WorkerPushRelease(b core.Build) bool {
	switch b.Api {
	case "gitea":
		fallthrough
	case "forgejo":
		err := hook.GiteaPushRelease(&b, b.ApiUrl, core.Cfg.Directory.Build+"/"+b.Zip, core.Cfg.CompressionMethod)
		if err != nil {
			core.Console.Fail("Error pushing the artifact: ", err.Error())
		} else {
			core.Console.Log("Pushing artifact should have worked.")
		}
	}
	return false
}
