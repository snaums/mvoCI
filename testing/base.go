package testing

import (
	"errors"
	"fmt"

	"codeberg.org/snaums/mvoCI/core"
)

type testParser interface {
	Name() string
	Description() string
	Probe(path string) bool
	Parse(path string, build *core.Build) error
}

var testParsers map[string]testParser

func __init(parser testParser) {
	if testParsers == nil {
		testParsers = make(map[string]testParser)
	}

	var name = parser.Name()
	testParsers[name] = parser
	fmt.Println("test_reports: added ", name, " test parser")
}

func init() {
	fmt.Println("test parser base init")
}

func TestParserRun(path string, build *core.Build) error {
	var err error
	var found bool = false
	for _, v := range testParsers {
		if v.Probe(path) {
			found = true
			err2 := v.Parse(path, build)
			if err == nil {
				err = err2
			}
		}
	}

	if found {
		return err
	}
	return errors.New("No Test Report found")
}
