package testing

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"codeberg.org/snaums/mvoCI/core"
	"github.com/joshdk/go-junit"
)

const junit_filename string = "junit_report.xml"

type junitParser struct {
	name        string
	description string
}

func init() {
	var junit = junitParser{
		name:        "JUnit Test Reports",
		description: "Analyzes JUnit Test Reports",
	}

	__init(junit)
}

func (j junitParser) Name() string {
	return j.name
}

func (j junitParser) Description() string {
	return j.description
}

func (j junitParser) Probe(path string) bool {
	_, err := os.Stat(filepath.Join(path, junit_filename))
	return err == nil
}

func (j junitParser) Parse(path string, build *core.Build) error {
	filename := filepath.Join(path, junit_filename)
	suites, err := junit.IngestFile(filename)
	if err != nil {
		return err
	}

	var tests []*core.Test = build.Tests
	for _, suite := range suites {
		for _, test := range suite.Tests {
			var log strings.Builder
			fmt.Fprintf(&log, "Test: %s > %s\n", suite.Name, test.Name)
			fmt.Fprintf(&log, "Error: %v\n", test.Error)
			fmt.Fprintf(&log, "Message: %s\n\n", test.Message)
			fmt.Fprintf(&log, "Stderr: %s\n\nStdout: %s\n", test.SystemErr, test.SystemOut)

			tests = append(tests, &core.Test{
				Duration:  test.Duration,
				Result:    j.ToResult(string(test.Status)),
				Name:      test.Name,
				Log:       log.String(),
				SuiteName: suite.Name,
			})
		}
	}

	build.Tests = tests
	build.BuildCalcTotalTests()

	return nil
}

func (j junitParser) ToResult(junitstate string) string {
	switch junitstate {
	case string(junit.StatusPassed):
		return core.TestResultOk
	case string(junit.StatusFailed):
		return core.TestResultFail
	case string(junit.StatusSkipped):
		return core.TestResultSkip
	case string(junit.StatusError):
		return core.TestResultError
	default:
		return core.TestResultUnknown
	}
}
