package testing

import (
	"io"
	"os"
	"path/filepath"
	"time"

	"codeberg.org/snaums/mvoCI/core"
)

const linters_filename string = "linter_report"

type linterParser struct {
	name        string
	description string
}

func init() {
	var linter = linterParser{
		name:        "Linter Reports",
		description: "Analyzes Linter Reports",
	}

	__init(linter)
}

func (j linterParser) Name() string {
	return j.name
}

func (j linterParser) Description() string {
	return j.description
}

func (j linterParser) Probe(path string) bool {
	_, err := os.Stat(filepath.Join(path, linters_filename))
	return err == nil
}

func (j linterParser) Parse(path string, build *core.Build) error {
	filename := filepath.Join(path, linters_filename)
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	b, err := io.ReadAll(f)
	if err != nil {
		return err
	}

	lint_log := string(b)
	var result = core.TestResultError
	if lint_log == "" {
		result = core.TestResultOk
	}

	var lint = core.Test{
		Duration:  time.Duration(1 * time.Millisecond),
		Result:    result,
		Name:      "Linter",
		Log:       lint_log,
		SuiteName: "Code Analysis",
	}

	build.Tests = append(build.Tests, &lint)
	build.BuildCalcTotalTests()

	return nil
}
