package events

import (
	"errors"
	"time"
)

type timer struct {
	event   EventType
	payload interface{}
	timer   time.Timer
}

func (t timer) GetEvent() EventType {
	return t.event
}
func (t timer) GetPayload() interface{} {
	return t.payload
}

func timer_elapsed(t timer) {
	SendEvent(t.event, t.payload)
}

func SetTimerAt(t time.Time, event EventType, payload interface{}) error {
	now := time.Now()
	if now.After(t) {
		// if the timer would already have fired, reject it with an error
		return errors.New("Timer needs to be in the future")
	} else {
		return SetTimer(now.Sub(t), event, payload)
	}
}

func SetTimer(d time.Duration, event EventType, payload interface{}) error {
	var t timer
	t.event = event
	t.payload = payload
	t.timer = *time.AfterFunc(d, func() {
		timer_elapsed(t)
	})
	return nil
}

func SetNightlyBuildTimer(hour int) {
	now := time.Now()
	// add 5 minutes for slack
	now.Add(5 * time.Minute)
	w := now.Round(0)
	yyyy, mm, dd := w.Date()
	wanted := time.Date(yyyy, mm, dd, hour, 0, 0, 0, now.Location())
	if wanted.Before(now) {
		wanted = time.Date(yyyy, mm, dd+1, hour, 0, 0, 0, now.Location())
	}

	SetTimerAt(wanted, OnTimerNightlyBuild, nil)
}
