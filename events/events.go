package events

import (
	"errors"

	"codeberg.org/snaums/mvoCI/core"
)

type EventType int

const (
	NullEvent EventType = iota
	OnBuildFinished
	OnTestEventSend
	OnTimerNightlyBuild
	NullEventEnd
)

func (ev EventType) ToStr() string {
	switch ev {
	case NullEvent:
		return "NullEvent"
	case NullEventEnd:
		return "NullEventEnd"
	case OnBuildFinished:
		return "OnBuildFinished"
	case OnTimerNightlyBuild:
		return "OnTimerNightlyBuild"
	case OnTestEventSend:
		return "OnTestSend"
	}
	return "Undefined"
}

type event struct {
	tp      EventType
	payload interface{}
}

func makeEvent(typ EventType, payload interface{}) Event {
	return event{tp: typ, payload: payload}
}

func (ev event) Type() EventType {
	return ev.tp
}
func (ev event) IsType(tp EventType) bool {
	return ev.tp == tp
}
func (ev event) Payload() interface{} {
	return ev.payload
}

type Event interface {
	Type() EventType
	IsType(EventType) bool
	Payload() interface{}
}

type EventHandler interface {
	HandlesType(EventType) bool
	Handle(Event) bool
}

var handlers map[EventType][]EventHandler
var eventloop chan Event

func init() {
	handlers = make(map[EventType][]EventHandler)
	eventloop = make(chan Event, 50)
	AddHandler(OnTestEventSend, TestEventHandler{})
}

func AddHandler(tp EventType, hdlr EventHandler) bool {
	handlers[tp] = append(handlers[tp], hdlr)
	return true
}

func SendEvent(tp EventType, payload interface{}) error {
	if tp < NullEvent || tp >= NullEventEnd {
		return errors.New("Event could not be posted")
	}
	var ev = makeEvent(tp, payload)
	eventloop <- ev
	return nil
}

func EventLoop() {
	for {
		ev := <-eventloop
		tp := ev.Type()
		handlers, ok := handlers[tp]
		if !ok || len(handlers) == 0 {
			core.Console.Warn("[event] Event was posted with type ", tp.ToStr(), " but was missing in handlers maps")
		} else {
			for _, h := range handlers {
				if !h.HandlesType(tp) {
					core.Console.Fail("[event] Event was posted with type ", tp.ToStr(), " but a handler said it would not handle this event")
				} else {
					var consumed = h.Handle(ev)
					if consumed {
						break
					}
				}
			}
		}
	}
}

type TestEventHandler struct {
}

func (h TestEventHandler) HandlesType(tp EventType) bool {
	return tp == OnTestEventSend
}

func (h TestEventHandler) Handle(ev Event) bool {
	core.Console.Warn("[TestEvent] Received a test-event")
	return true
}
