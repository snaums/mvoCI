DISTDIR=dist

RELEASEVERSION=$(shell echo ${VERSION})
DEBUGVERSION=$(shell git rev-list -1 HEAD | cut -c 1-10 )
RELEASEVERSION?=$(DEBUGVERSION)
HASH=$(shell git rev-list -1 HEAD)
GOVERSION=$(shell go version)
DATE=$(shell date --rfc-2822)

PROJECTURL=codeberg.org/snaums/mvoCI

LDFLAGS=-s -w
LDGENERICFLAGS=-X '$(PROJECTURL)/core.BuildTime=$(DATE)' \
			   -X '$(PROJECTURL)/core.Compiler=$(GOVERSION)' \
			   -X '$(PROJECTURL)/core.GitHash=$(HASH)'
DEBUGLDVERSION=-X '$(PROJECTURL)/core.Version=dev-$(DEBUGVERSION)'
RELEASELDVERSION=-X '$(PROJECTURL)/core.Version=$(RELEASEVERSION)'

DEBUGLDFLAGS=$(LDFLAGS) $(DEBUGLDVERSION) $(LDGENERICFLAGS)
RELEASELDFLAGS=$(LDFLAGS) $(RELEASELDVERSION) $(LDGENERICFLAGS)

all:
	go run -gccgoflags "-pthread" -ldflags "$(DEBUGLDFLAGS)" main.go

debug:
	go run -gccgoflags "-pthread" -ldflags "$(DEBUGLDFLAGS)" main.go --debug

install:
	go run -gccgoflags "-pthread" -ldflags "$(DEBUGLDFLAGS)" main.go --install

mvobuild:
	go build -gccgoflags "-pthread" -ldflags "$(DEBUGLDFLAGS)" main.go
	mv main mvo
	#-upx mvo

licenses:
	go-licenses report "$(PROJECTURL)" > licenses.csv

getswag:
	go install github.com/swaggo/swag/cmd/swag@latest

# You need https://github.com/swaggo/swag
# go install github.com/swaggo/swag/cmd/swag@latest
swagger:
	swag init --parseDependency --parseInternal
	-git clone https://codeberg.org/snaums/pelican-swagger.git
	-mkdir pelican-swagger/content
	cp docs/* pelican-swagger/content/
	cd pelican-swagger &&\
		git submodule update &&\
		make &&\
		make highlight

swagger-web: swagger
	cd pelican-swagger &&\
		make serve

release: swagger
	go build -gccgoflags "-pthread" -ldflags "$(RELEASELDFLAGS)" main.go
	-mv main mvo
	-mv main.exe mvo.exe
	-upx mvo

package:
	rm -rf $(DISTDIR)
	mkdir $(DISTDIR)
	-mv mvo $(DISTDIR)
	-mv mvo.exe $(DISTDIR)
	cp -r views $(DISTDIR)
	cp -r static $(DISTDIR)

dist: mvobuild
	make package
	make distclean

distrelease: release
	make package
	make distclean

distclean:
	rm -rf web hook log Makefile main.go mvo.cfg static repo static views core build builds auth .gitignore go.mod go.sum mvoBuild.sh testing
	mv $(DISTDIR)/* .
	rmdir $(DISTDIR)

clean:
	-rm mvo mvo.exe
	-rm -rf dist

dep:
	echo "Not needed anymore, use golangs modules"

doc:
	godoc -http=localhost:6060

vet:
	go vet

lint:
	golangci-lint run auth/... build/... core/... hook/... testing/... web/...

staticcheck:
	staticcheck

ctags:
	ctags -R --language-force=go .
