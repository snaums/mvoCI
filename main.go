// mvoCI is a simple continuous integration server aimed at personal use.
// You really should not use it as an open CI server and only provide it to
// people and code you trust. mvoCI does not isolate build-processes well, if
// you need that, please choose a different solution.
//
// mvoCI aims to be simple and easy to use, not a lot of features, just enough
// to build simple projects and maybe deploy them to your server fo example.
// It is not equipped with distributed building atm (while you of course could
// script that in the build script).
//
// Be aware, mvoCI lets you shoot off your own foot. Hint: Don't.

package main

import (
	"errors"
	"flag"
	"os"
	"time"

	"codeberg.org/snaums/mvoCI/auth"
	"codeberg.org/snaums/mvoCI/build"
	"codeberg.org/snaums/mvoCI/core"
	"codeberg.org/snaums/mvoCI/events"
	"codeberg.org/snaums/mvoCI/web"
)

type mvoFlags struct {
	web             bool
	install         bool
	port            int
	debug           bool
	defconfig       bool
	unattended      bool
	apikey          string
	resetDbMigrator bool
}

func parse_flags() mvoFlags {
	var result mvoFlags
	flag.BoolVar(&result.web, "web", false, "Starts the complete web-application, if not: only webhook and local hook")
	flag.BoolVar(&result.install, "install", false, "Starts "+os.Args[0]+" with installer enabled")
	flag.IntVar(&result.port, "port", 0, "Specify the port to bind to")
	flag.BoolVar(&result.debug, "debug", false, "Starts "+os.Args[0]+" with verbose debug output")
	flag.BoolVar(&result.defconfig, "defconfig", false, "Create a default config and exit")
	flag.BoolVar(&result.unattended, "unattended", false, "Together with --install; run unattended installer")
	flag.StringVar(&result.apikey, "apikey", "", "Together with --install --unattended file into which a first apikey will be written")
	flag.BoolVar(&result.resetDbMigrator, "resetDbMigrator", false, "Reset the Migration state, i.e. retry migrating the database from scratch (not recommended)")
	flag.Parse()

	return result
}

// Create a default config, a first root user and some necessary initialization
// data to get started.
// This is intended to be used for automatic tests of API calls.
func unattended_install() (*core.User, error) {
	core.Console.Log("Starting unattended install")

	_, err := os.Stat("db.sqlite")
	if err == nil {
		core.Console.Warn("Default database file does already exist, this may fail")
	}

	core.Cfg = core.ConfigDefault()
	core.Cfg.Database.Provider = "sqlite3"
	core.Cfg.Database.Filename = "db.sqlite"
	err = core.Cfg.ConfigWrite(core.ConfigFile)
	if err != nil {
		core.Console.Fail("Unable to write config: ", err)
		return nil, err
	}
	core.Console.Log("Written default config to ", core.ConfigFile)
	core.Console.Log("Set Database to ", core.Cfg.Database.Provider, " with file ", core.Cfg.Database.Filename)

	err = core.DBConnect(false)
	if err != nil {
		core.Console.Fail("Database access failed", err)
		return nil, err
	}

	// create root user
	username := "root"
	password := "extremelyRandom"
	var u = core.User{
		Name:      username,
		Email:     "root@example.org",
		Superuser: true,
	}

	err = auth.AuthSetSecret(&u, "native", password, password)
	if err != nil {
		core.Console.Fail("Unable to set password for root user", err)
		return nil, err
	}

	web.SetupAuthProvider(&u)

	var cnt int64
	core.DB.Model(core.User{}).Count(&cnt)
	if cnt > 0 {
		core.Console.Fail("Expected no users present in database")
		return nil, errors.New("Expected no users present in database")
	}

	core.DB.Create(&u)
	err = core.CreateGroupEveryone(u)
	if err != nil {
		core.Console.Fail("Error creating group Everyone: ", err)
	}
	core.Console.Log("Created root user: ", username, ":", password)

	return &u, nil
}

// In unattended install, create an apikey for the default user and write it
// into the file apikey.
// This is intended to be used for automatic tests of API calls.
func unattended_apikey(u *core.User, apikey string) error {
	secret, err := web.GenerateRandomString(64)
	if err != nil {
		core.Console.Fail("Can't generate api-token: ", err)
		return err
	}

	token := core.LoginToken{
		Secret:    secret,
		Name:      "Unattended Install",
		Type:      "api",
		ExpiresAt: time.Now().Add(24 * 356 * time.Hour),
		User:      *u,
	}

	err = os.WriteFile(apikey, []byte(secret), 0600)
	if err != nil {
		core.Console.Fail("Can't open the requested file: ", err)
		return err
	}

	err = core.DB.Create(&token).Error
	if token.ID <= 0 || err != nil {
		core.Console.Fail("Cannot store token in Database: ", err)
		return err
	}

	core.Console.Log("Written unattended apikey to file ", apikey)
	return nil
}

// @title My Very Own CI Server API
// @version 1.0
// @description mvoCI is a simple personal CI server.

// @contact.name Stefan Naumann
// @contact.url https://www.codeberg.org/snaums/mvoCI
// @contact.email me@stefannaumann.de

// @license.name GPLv3
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host
// @BasePath /api/v1

// @title My Very Own CI Server API
// @version 1.0
// @description mvoCI is a simple personal CI server.

// @contact.name Stefan 'snaums' Naumann
// @contact.url https://www.codeberg.org/snaums/mvoCI
// @contact.email me@stefannaumann.de

// @license.name GPLv3
// @license.url https://www.gnu.org/licenses/gpl-3.0.en.html

// @BasePath /api/v1

// entry point of mvoCI, sets up the config data structure for the rest of the
// system and bootstraps the database and the web-server
func main() {
	// set up command line parameters
	var err error
	flags := parse_flags()

	// read the config
	cfg_read_err := core.Cfg.ConfigRead(core.ConfigFile)
	if flags.install {
		core.Cfg.Install = true
	}
	core.Cfg.ConfigValidate()

	_ = os.Mkdir("log", 0777)
	lf := core.Cfg.LogFile
	lfenable := core.Cfg.LogFileEnable
	lmode := core.Cfg.LogMode
	if lfenable && len(lf) > 0 {
		core.LogFileEnable = true
	}
	if lmode == "stdout" {
		core.LogStdoutEnable = true
		core.LogStderrEnable = false
	} else {
		core.LogStdoutEnable = false
		core.LogStderrEnable = true
	}

	if flags.debug {
		core.Cfg.Debug = true
	}

	if flags.port != 0 {
		core.Cfg.HttpPort = flags.port
	}

	_, _ = core.LogInit(lf)
	defer core.LogClose()
	if flags.defconfig {
		if cfg_read_err != nil {
			err = core.Cfg.ConfigWrite(core.ConfigFile)
			if err != nil {
				core.Console.Fail("Unable to write config: ", err)
				return
			}
			core.Console.Log("Written default config to ", core.ConfigFile)
		} else {
			core.Console.Log("A valid config was found, will not write default config in its place")
		}
		return
	}

	if flags.install && flags.unattended {
		u, err := unattended_install()
		if err != nil {
			core.Console.Fail("Unattended installation failed: ", err)
			return
		}
		if flags.apikey != "" {
			err = unattended_apikey(u, flags.apikey)
			if err != nil {
				core.Console.Fail("Unattended API key generation failed", err)
			}
		}
		return
	}

	core.Console.Log("Read Configuration (", core.ConfigFile, ") successfully")
	// set up gorm database connection
	if core.Cfg.Install {
		core.Console.Log("Installation mode: Skipping Database connection")
		// ensure DB is nil
		core.DB = nil
	} else {
		core.Console.Log("Connecting to database: ", core.Cfg.Database.Provider)
		err = core.DBConnect(flags.resetDbMigrator)
		if err != nil {
			core.Console.Fail("Cannot connect to database: ", err)
			return
		}
	}

	if !core.Cfg.Install {
		core.Console.Log("Setting up build workers")
		build.SetupWorkerThreads()

		if core.Cfg.LoginTokenInvalidate {
			core.DB.Where("type = ?", "login").Delete(&core.LoginToken{})
		} else {
			core.DB.Where("type = ? AND (step != ? OR expires_at < ?)", "login", "fin", time.Now()).Delete(&core.LoginToken{})
		}
	}

	core.Console.Log("Starting EventLoop")
	go events.EventLoop()

	core.Console.Log("Starting", core.Cfg.AppTitle)

	// start eh web server and hand everything over to it
	server := web.NewServer()
	server.Start()
}
