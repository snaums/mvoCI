// this file mostly contains the object representation of the json request send
// by the webhook feature of Gitlab

package hook

type GitlabPayloadProject struct {
	Id          int
	Name        string
	Description string
	WebUrl      string `json:"web_url"`
	AvatarUrl   string `json:"avatar_url"`
	GitSshUrl   string `json:"git_ssh_url"`
	GitHttpUrl  string `json:"git_http_url"`
}

type GitlabPayloadCommit struct {
	Id        string
	Message   string
	Title     string
	Timestamp string
	Url       string
	Author    struct {
		Name  string
		Email string
	}
}

type GitlabPayloadRepository struct {
	Name        string
	Url         string
	Description string
	Homepage    string
	GitSshUrl   string `json:"git_ssh_url"`
	GitHttpUrl  string `json:"git_http_url"`
}

type GitlabPayload struct {
	ObjectKind        string `json:"object_kind"`
	EventName         string `json:"event_name"`
	Before            string
	After             string
	Ref               string
	CheckoutSha       string
	Message           string
	UserId            int    `json:"user_id"`
	UserName          string `json:"user_name"`
	UserUsername      string `json:"user_username"`
	UserEmail         string `json:"user_email"`
	ProjectId         int    `json:"project_id"`
	Project           GitlabPayloadProject
	Commits           []GitlabPayloadCommit
	TotalCommitsCount int `json:"total_commits_count"`
	Repository        GitlabPayloadRepository
}
