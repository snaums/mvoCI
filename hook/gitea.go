// this file mostly contains the object representation of the json request send
// by the webhook feature of gitea

package hook

import (
	"encoding/json"
	"errors"
	"strconv"
	"strings"
	"time"

	"codeberg.org/snaums/mvoCI/core"
	"golang.org/x/oauth2"
)

// push a release artifact to the gitea server
func GiteaPushRelease(b *core.Build, url string, zip string, compressionMethod string) error {
	if b == nil {
		return errors.New("No build")
	}

	// (0) Find a fitting Token in our OAuthToken Table
	var ex []string = strings.Split(url, "/api/v1/repos")
	var baseurl = ex[0]

	var cred core.OauthToken
	var tok oauth2.Token
	var api = b.Api
	if api == "forgejo" {
		api = "gitea"
	}
	core.DB.Where("api = ? AND url LIKE ? AND state = ? AND user_id = ?", api, baseurl, "finished", b.Repository.UserID).First(&cred)
	if cred.ID <= 0 {
		return errors.New("No matching OAuth-Credentials found")
	}
	err := json.Unmarshal([]byte(cred.ClientToken), &tok)
	if err != nil {
		return err
	}

	// (1) connect to gitea with oauth token
	client := ConnectWithToken(cred.Url, cred.ClientId, cred.ClientSecret, []string{}, &tok, cred.RedirectUri)

	// (2) send request
	attachment_name := strings.ReplaceAll(b.Zip, "_", "-")
	var name string = b.Name
	if name == "" {
		var r core.Repository
		core.DB.Select("id, name").Where("id = ?", b.RepositoryID).First(&r)
		if r.ID == 0 {
			return errors.New("Failed to name the archive")
		}

		name = r.Name
	}
	if b.TagName != "" {
		name += "-" + b.TagName
	}

	attachment_name = name + ".tar." + compressionMethod
	attachment_name = strings.ReplaceAll(attachment_name, " ", "-")
	resp, err := PostFile(client, url+"/assets", map[string]string{}, "attachment", zip, attachment_name)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return errors.New("Statuscode does not indicate success: " + strconv.Itoa(resp.StatusCode))
	}

	// (3) close everything
	return nil
}

type GiteaPayloadCommitUser struct {
	Name     string
	Email    string
	Username string
}

type GiteaPayloadCommit struct {
	Id        string
	Message   string
	Url       string
	Author    GiteaPayloadCommitUser
	Committer GiteaPayloadCommitUser
	Timestamp time.Time
}

type GiteaPayloadUser struct {
	Id         int
	Login      string
	Full_Name  string
	Email      string
	Avatar_Url string
	Username   string
}

type GiteaPayloadRepository struct {
	Id                int
	Owner             GiteaPayloadUser
	Name              string
	Full_Name         string
	Description       string
	Private           bool
	Fork              bool
	Html_Url          string
	Ssh_Url           string
	Clone_Url         string
	Website           string
	Stars_Count       int
	Forks_Count       int
	Watcher_Count     int
	Open_Issues_Count int
	Default_Branch    string
	Created_At        time.Time
	Updated_at        time.Time
}

type GiteaPayloadRelease struct {
	Id              int
	TagName         string `json:"tag_name"`
	TargetCommitish string `json:"target_commitish"`
	Name            string
	Body            string
	Url             string
	HtmlUrl         string `json:"html_url"`
	Draft           bool
	Prerelease      bool
	Author          GiteaPayloadUser
}

type GiteaPayload struct {
	Secret      string
	Ref         string
	Before      string
	After       string
	Compare_Url string
	Commits     []GiteaPayloadCommit
	Repository  GiteaPayloadRepository
	Pusher      GiteaPayloadUser
	Sender      GiteaPayloadUser
	Release     GiteaPayloadRelease
}
