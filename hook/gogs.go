// this file mostly contains the object representation of the json request send
// by the webhook feature of gogs

package hook

import "time"

type GogsPayloadCommitUser struct {
	Name     string
	Email    string
	Username string
}

type GogsPayloadCommit struct {
	Id        string
	Message   string
	Url       string
	Author    GogsPayloadCommitUser
	Committer GogsPayloadCommitUser
	Timestamp time.Time
}

type GogsPayloadUser struct {
	Id         int
	Login      string
	Full_Name  string
	Email      string
	Avatar_Url string
	Username   string
}

type GogsPayloadRepository struct {
	Id                int
	Owner             GogsPayloadUser
	Name              string
	Full_Name         string
	Description       string
	Private           bool
	Fork              bool
	Html_Url          string
	Ssh_Url           string
	Clone_Url         string
	Website           string
	Stars_Count       int
	Forks_Count       int
	Watcher_Count     int
	Open_Issues_Count int
	Default_Branch    string
	Created_At        time.Time
	Updated_at        time.Time
}

type GogsPayload struct {
	Secret      string
	Ref         string
	Before      string
	After       string
	Compure_Url string
	Commits     []GogsPayloadCommit
	Repository  GogsPayloadRepository
	Pusher      GogsPayloadUser
	Sender      GogsPayloadUser
}
