// The hook package contains code for the build hoops of the different
// source code versioning systems, like gitea, gogs, gitlab, etc.
package hook

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"

	"golang.org/x/oauth2"
	//"codeberg.org/snaums/mvoCI/core"
)

// send a file through an http.Client via POST, e.g for posting a release to a server
func PostFile(client *http.Client, url string, formData map[string]string, formFilename string, filename string, attachment_name string) (*http.Response, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(formFilename, attachment_name)
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(part, file)
	if err != nil {
		return nil, err
	}
	for key, val := range formData {
		_ = writer.WriteField(key, val)
	}

	err = writer.Close()
	if err != nil {
		return nil, err
	}

	req, _ := http.NewRequest("POST", url, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	return client.Do(req)
}

// internal function to create the configuration for the OAuth2 library
func CreateConf(server string, clientID string, clientSecret string, scopes []string, redirectUri string) *oauth2.Config {
	return &oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Scopes:       scopes,
		RedirectURL:  redirectUri,
		Endpoint: oauth2.Endpoint{
			AuthURL:  server + "/login/oauth/authorize",
			TokenURL: server + "/login/oauth/access_token",
		},
	}
}

// returns the OAuth2 authentification url, the user has to visit
func AuthUrl(server string, clientID string, clientSecret string, scopes []string, redirectUri string) string {
	conf := CreateConf(server, clientID, clientSecret, scopes, redirectUri)
	url := conf.AuthCodeURL("", oauth2.AccessTypeOnline)
	return url
}

// create a client-connection with a (hopefully) valid OAuth2 Auth Token
func ConnectWithToken(server string, clientID string, clientSecret string, scopes []string, tok *oauth2.Token, redirectUri string) *http.Client {
	ctx := context.Background()
	conf := CreateConf(server, clientID, clientSecret, scopes, redirectUri)

	client := conf.Client(ctx, tok)
	return client
}

// create a client-connection with an auth-code from the user, to get a token
func Connect(server string, clientID string, clientSecret string, scopes []string, redirectUri string, authCode string) (*http.Client, *oauth2.Token, error) {
	ctx := context.Background()
	conf := CreateConf(server, clientID, clientSecret, scopes, redirectUri)

	tok, err := conf.Exchange(ctx, authCode)
	if err != nil {
		//die()
		fmt.Printf("Error: %+v\n", err)
		return nil, nil, err
	} else {
		client := conf.Client(ctx, tok)
		return client, tok, nil
	}
}
