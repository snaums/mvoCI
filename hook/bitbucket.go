// this file mostly contains the object representation of the json request send
// by the webhook feature of BitBucket

package hook

type BitBucketPayloadActor struct {
	Name         string
	EmailAddress string
	Id           int
	DisplayName  string
	Active       bool
	Slug         string
	Type         string
}

type BitBucketPayloadLinks struct {
	Self []struct {
		Href string
		Name string
	}
	Clone []struct {
		Href string
		Name string
	}
}

type BitBucketPayloadRepository struct {
	Slug          string
	Id            int
	Name          string
	HierarchyId   string
	ScmId         string
	State         string
	StatusMessage string
	Forkable      bool
	Public        bool
	Links         BitBucketPayloadLinks
}

type BitBucketPayloadChanges struct {
	Ref struct {
		Id        string
		DisplayId string
		Type      string
	}
	RefId    string
	FromHash string
	ToHash   string
	Type     string
}

type BitBucketPayload struct {
	EventKey   string
	Date       string
	Actor      BitBucketPayloadActor
	Repository BitBucketPayloadRepository
	Changes    []BitBucketPayloadChanges
}
