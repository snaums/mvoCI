slug: docs_testing
title: Testing and Test Results
date: 2024-04-02
author: snaums
order: 03-testing
category: doc

This article describes how to set-up automatic unit testing alongside builds and how to get test results to show on the build-view.

## Running Tests

When building a repository in mvoCI, a bash script is executed to build, maybe cleanup and prepare the package of a repository, given certain environment variables.

mvoCI is oblivious to what you are doing in the build script, so it will not interfere with your build and it will not actually run any tests, if your don't do it is the build script. However, after your build an additional step looks for certain files that mvoCI suspects to contain test results, linter results etc.

So, let's say, you are building a Java program with gradle. Gradle has built-in support for JUnit 4, which can be used to run your tests and generate a JUnit Reports. mvoCI expects a file `junit_report.xml` to contain the JUnit Report. This file will be interpreted if it is present and the test run is added in mvoCI, associated with the build.

This might be the build script:

```bash
# building
# ...

# running the test
gradle test

# copying the results-file
cp build/test-results/test/TEST-org.example.AppTest.xml junit_report.xml
```

After the build, `junit_report.xml` lays in the root of the build-directory, which will automatically be interpreted by mvoCI. The tests are added to the build/test run with their status and outputs.

## Test Reports

If any test-report file is present (like the ones below), a test run is added to the build with the test-results. The following files are supported:

  * `junit_report.xml` -- JUnit 4 format
  * `linter_report` -- output of a linter

All present test reports will be interpreted and tests will be added, so if your project creates JUnit reports and maybe some linter report, both will be added to the test run.

The JUnit report contains several tests. The linter report is a single file. The contents will be copied into the Log-field of the test. If the file is present but empty, the test succeeds, if it is not empty it will present as "with error". There is no additional parsing.

## Examples

### CMake / CTest (C/C++)

Assuming you are creating a C/C++ project and are using CMake, you can use the CTest integration and run your tests from your build system. CTest can output its results as junit compatible XML files.

Let's say you wanted to build and test a simple C program, contained in `main.c`. Then your CMakeLists.txt might already look like this:

```cmake
cmake_minimum_required(VERSION 3.18)
project(myProgram VERSION 1.0 LANGUAGES C)

add_executable(program src/main.c)
```

This will build your program. Now let's enable CTest testing. Add the following lines to the end of your CMakeLists.txt:

```
enable_testing()
add_test(NAME StartupTest COMMAND program)
add_Test(NAME ArgumentTest COMMAND program -x -e)
```

We've enabled testing and added two tests, one which only starts the program and another which also hands some arguments to the program. CTest will run the tests, when you run `make test`. It will check if the programs can be run and return 0 or some other return value. If the exit code is not 0, the test will be counted as failed.

Now to use this in mvoCI, add this to your build script:

```bash
# create a build-directory
mkdir build
cd build
# configuring the CMake project and generating a Makefile
cmake ..
# building the project
make

# you _could_ run make test, but it will not output a junit report
# make test
# better run ctest manually like so:
ctest --output-junit ../junit_report.xml --output-on-failure
```

