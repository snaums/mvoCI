slug: docs_ssh_keys
title: SSH-Authentication and Keys
date: 2024-04-02
author: snaums
order: 02-ssh-keys
category: doc

In some git software, non-public repositories one can either clone the repository with HTTP authentication or by authenticating with an SSH key. While HTTP authentication is quite simple, either enter your username and password interactively (which cannot be used for/with mvoCI) or provide both within the URL, which has its own problems, as it makes it more difficult to identify the correct repository when a webhook message arrives.

Authentication with SSH keys is more secure (if your don't loose the private key), and eliminates the problems mentioned above.

## SSH Keys

There are two main ways to enable SSH key authentication for mvoCI. You can either add them manually to `.ssh/config` or use the Integration -> SSH Keys Interface to add them with mvoCI. This will overwrite the current `.ssh/config`.

### SSH Key Interface

Using the mvoCI interface, go to Integration -> SSH Keys. You will see a list of the registered SSH keys. It will not scan your `~/.ssh/config`. Make sure to back-up any changes you made yourself to that file.

At first, this list will be empty. Click on "Add Key" in the toolbar (on the left or top, next to the headline). Here you can enter certain details about how the SSH keys needs to be used. You will have to input at least a host (that can double as a hostname), you can add a username, proxy information or a port.

Lastly you'll have to enter an SSH **private** key. You can either upload a key file, enter the key file in the test field or select an already present file in the servers `~/.ssh` folder by entering its name. **When transferring private keys, make sure the connection is encrypted (HTTPS) and that no one in the middle can decrypt the data stream.** Some company networks break SSL/TLS open "for security reasons". If unsure, upload a key (or generate one, see below), then add it's filename in this form.

### Manual Operation

If you have terminal access to the machine yourself, you can enter SSH keys manually. This method has the advantage that your keys will stay private (as long as the terminal connection is secure).

You can either copy already existing keys to the machine, or generate a new private/public key pair, enter them into the ssh-config and get the public key to enter into the VCS like codeberg.org or your own forgejo instance.

First I'll show how to copy SSH-keys from one to another machine. Then edit your ssh-config like shown below.

```bash
~> scp ~/.ssh/codeberg_rsa mvoci@server:.ssh/
~> scp ~/.ssh/codeberg_rsa mvoci@server:.ssh/
```

If you don't have ssh keys you'd want on your build machine create a new key-pair. Use `ssh-keygen`. You can tell it to use a certain algorithm, like rsa or ecdsa. If you are unsure, refer to the VCS and what algorithms are supported. RSA should be fine for most. It will ask for the target-file and for a passphrase.

```bash
~> ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/mvoci/.ssh/id_rsa): codeberg_rsa
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in new_file
Your public key has been saved in codeberg_rsa.pub
The key fingerprint is:
SHA256:oNNRtNEw9fnJECMWvkqyWVqBIpeDbNm1voOtJwlX/NQ mvoci@server
The key's randomart image is:
+---[RSA 3072]----+
|      ..*+=.o    |
| . + o + *.o +   |
|  * *.= o.. +    |
| . o *oo..E. + . |
|    o.+oS .   +  |
|  . .+ X..       |
|   o..B .        |
|    o...         |
|    .o           |
+----[SHA256]-----+
```

This will create the files `~/.ssh/codeberg_rsa` (private key) and `~/.ssh/codeberg_rsa.pub` (public key). **Do never show anyone your private key**.

We just now created them on our mvoCI machine. Let's alter the `~/.ssh/config` to use the key to authenticate with codeberg automatically:

```
Host codeberg.org
    User mvoci
    IdentityFile ~/.ssh/codeberg_rsa
```

Then open your public key, and enter it into your VCS, in codeberg: top right on your avatar -> Settings. On the left SSH- / GPG-Keys. Then copy the contents of your private key into the field.

```bash
~> cat codeberg_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCdfva2eog7C ...
```

If you've not entered a passphrase, this is enough to get authentication working, you can test it by manually building one of your repos on codeberg.

## Using SSH-Keys with Passphrase

If you've entered a passphrase, you will have to add the key into the users ssh-agent. First start the ssh-agent. It will output some bash environment variables, that you need to evaluate:

```bash
~> eval `ssh-agent`
```

Now you can add an ssh-key with, enter the passphrase. Now you can start mvoCI. It needs to know the environment variables, so git/ssh can connect to the ssh-agent to use the unlocked key.

```bash
~> ssh-add ~/.ssh/codeberg_rsa
Enter passphrase for /home/mvoci/.ssh/codeberg_rsa: 
```
