slug: docs_repository
title: Building a Repository
date: 2022-05-22
author: snaums
order: 02-repository
category: doc

A repository is the central concept of mvoCI. A repository in mvoCI represents a git-URL with a bash script on how to build the repository and some meta information, it can be built when certain events happen in the version control system like pushing into the repository (see Webhooks), with the API or on button press.

## Creating of a Repository

In the Repositories view, press "Add Repository". Give it a name, a repository URL, this can be either a https-link or a `git@...` URL, i.e. everything that `git clone` takes. You shouldn't add a username and a password here, as these will be stored plaintext in the database. Enter a default branch and a build script.

There a few flags you can activate: You can activate, that artifacts of the build are kept, i.e. everything in the folder after the build-script ran, you can activate the webhook, so the repository build can be triggered by a VCS like gitea.

If you entered a `git@...` link, you'll probably want to have a certificate, so mvoCI can authenticate with the VCS and access the repository. The simplest solution is, to add a new user to your VCS, upload an ssh-certificate and place the public+private side into `.ssh/id_rsa` or `.ssh/id_ecdsa`. These files will be automatically tried by git (or ssh to that matter) when trying to establish a connection to a remote server.

## Building the Repository

Building the repository is composed of these steps:

  1. git-clone and git-checkout of the repository, branch and/or hash,
  2. preparing the build-script and placing it into the cloned repository,
  3. executing the build-script and
  4. zipping (tar'ing) the folder and preserve it as artifacts if enabled

### Build Scripts

The build script is a bash-script, that is executed inside the cloned git-repository. There are some information passed to the build-script as environment variables to distinguish between certain events, like user-build events, push-events or release-events. The version, commit-hash, message and additional arguments are also passed in the environment, as `MVO_` variables.

These are the environment variables presented to the build script:

```bash
$ env
MVO_EVENT=push
MVO_REPO=mvoCI
MVO_BRANCH=master
MVO_ARG_id=1
MVO_ARG_editor=vim
MVO_ARG_test=VALUE
MVO_ARG_branch=master
MVO_ARG_event=push
MVO_ARG_hash=
VERSION=c347b488e989c076c4db2919657a34b28d58d41c
MVO_COMMIT_SHA=c347b488e989c076c4db2919657a34b28d58d41c
MVO_COMMIT_AUTHOR=Stefan Naumann
MVO_COMMIT_MESSAGE=Merge pull request 'fix issue #38 - build advanced dialog closes when clicking into branch input field.' (#39) from jenszahner/mvoCI:bugfix/advanced-build into master
```

### Webhooks

Webhooks are HTTP-GET Requests, sent from your VCS, like gitlab or gitea to a CI-software when certain events happen, like when a user pushes some commits, releases a release or creates a tag. Most VCS are supported only with push-events by mvoCI.

You'll need the following:

  * Secret Key of the repository
  * Webhook URL of mvoCI (Integration page)

Webhooks must be configured in your VCS. Oftentimes a secret-key is needed. This can either be found in mvoCI in the repository detail view in the meta-data section under the headline, or in the edit repository view.

You'll also the Webhook URL of mvoCI, which can be found in the Integration page. Please select the correct version for your VCS, i.e. for gitea choose the URL ending in `/gitea`. This is necessary, so the message sent be your VCS can be interpreted correctly.

In your VCS, for example in your gitea instance, choose the repository, Settings -> Webhook and enter both the webhook URL and the secret key. Save your configuration. You can also try the settings, by sending a dummy request. You should receive a HTTP response with the status 200 OK, and see a new build in mvoCI.

### Debugging Webhooks

If there is trouble concerning the webhooks, you can enable WebHookLogs in the `mvo.cfg` file. Then in mvoCI Admin -> Webhook Log you find all webhooks that were received by mvoCI together with their status code.

If the webhook you tried to send is missing, make sure, that the mvoCI server can be reached under the URL by your VCS. Make sure the URL is valid and `localhost` should only be used if the two servers are on the same machine (also on the same virtual machine!). Otherwise a public/private DNS name should be used. If you have a reverse proxy configured, have a look into the logs of your webserver.

If the webhook is showing up, but the status code is 404, the repository could not be identified. That more often than not means, that the repository URL in mvoCI and the URL of the VCS do not match. There is also a bug, that username:password in the URL break the correct recognition of the repository. Try without them. Also the secret key might be faulty, make sure you copied the secret key without any newlines or spaces at the end.
