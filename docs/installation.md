slug: docs_installation
title: Installation
date: 2022-05-22
author: snaums
order: 01-installation
category: doc

This page describes how to get mvoCI up and running, from obtaining mvoCI to setting it up on first run. Please note, due to a dependency to the POSIX threads mvoCI will only run on Unix operating systems.

## Obtaining mvoCI

mvoCI can be obtained through several ways:

  * Obtain a pre-built executable
  * Build from source
  * Run inside docker

### Obtaining a pre-build Executable

The easiest way to get mvoCI is to download a pre-build executable. You can either download a release from [Codeberg](https://codeberg.org/snaums/mvoCI/releases), which should be better tested, our you can download a nightly from [mvoCI](https://mvo.stefannaumann.de).

Eitherway you end up with a directory containing the `mvo` executable, as well as `static` and `views` folders. The go-binary can be executed as is as the go-toolchain builds all dependencies into the binary.

### Building from Source

To build mvoCI from source you will need the golang-compiler. Depending on the distribution you might need to install `golang` or `go` packages. You can also get the toolchain pre-built from [golang.org](golang.org) The go-toolchain will take care of dependencies. You'll need at least version 1.16.

If you don't want to develop mvoCI you can use `make dist` or `make distrelease` to build the executable and clean the directory from source code. Otherwise use `make mvobuild` or `make release` to build.

Just running mvoCI can be done with `make`, and starting mvoCI in install-mode is possible with `make install`.

### Running in docker

With the provided Dockerfile and `start.sh` script, you can also build an mvoCI docker container and run mvoCI in docker. This will take care of creating the container and a permanent volume, so the database and configuration file can be stored permanently.

`start.sh` tries to guess, whether mvoCI needs to start in installation mode or not. You can override it with the `-i` flag. Make sure to think about what compilers are needed in operation, and pass the corresponding Ubuntu packages with the `-c` flag (if there are several, make sure it is interpreted as string, i.e. put the whole string in quotes).

## Set-up

Depending on how you obtained mvoCI, you need to start mvoCI in installation mode to configure the database connection and the first user. The docker-way will already start in installation mode. In the other cases, start mvoCI with `make install` or `./mvo --install`.

Then open a web browser and connect to `http://localhost:4042`.

### Database Set-up

For the database backend you can choose between PostgreSQL, MySQL/MariaDB, SQLServer or SQLite3. Depending on your choice different parameters need to be given.

### First user

In the third step, create the first user, which is also the superuser of your instance. Make sure, to remember the password, otherwise you will need to delete the database-entry and restart in installation-mode.

### First Start

After the installation mvoCI must be restarted, either by killing the running instance and running it again in non-install mode (`make` or `./mvo`) or by restarting the docker-container.

Now you should be able to log in to your new instance. You will probably need to configure your webserver to serve mvoCI as reverse proxy.
