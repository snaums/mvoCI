# mvoCI

[![Build Status](https://mvo.stefannaumann.de/repo/state/2)](https://mvo.stefannaumann.de/public/2)
[![Go Docs](https://img.shields.io/badge/go-documentation-blue)](https://pkg.go.dev/codeberg.org/snaums/mvoCI)

My Very Own CI-server (Continuous Integration), a personal continuous integration system. mvoCI builds code from git repositories either on user-input, via an API call or on webhooks, i.e. whenever someone pushed commits to the git or when new releases are added.

mvoCI aims to be simple and do as much as is necessary and nothing more. It aims to have a small memory footprint, be easy to use and understand.

It is intended as a personal continuous integration server, useful for your private builds or builds of a team, who know and trust each other. It is not intended to be used for the public, for untrusted repositories or with untrusted co-users.

**Be advised, that mvoCI uses shell access for its building routines. It is your responsibility to secure your machine from damage or data leakage by mvoCI.**

## What it does

* organize Repositories and Builds of them
* build Repositories on webhook or on click "Build Now"
* build and publish release-artifacts to Gitea automatically
* bindings to Gogs, Gitea, Gitbucket, Gitlab, Bitbucket and Github
* group management
* ssh keys management
* simple test reporting

## What it does not

* Anything more, like
  * timed builds
  * after build scripts
  * SVN, Mercurial, VCS (although there should not be a real reason for this constraint)
  * internationalization (only English)

## Deprecation Notices

* Going forward the division between Release and Push Build script will be removed and only the push build script will be kept going forward. Make sure to combine your release and push build scripts and make use of the environment variables like *MVO_EVENT*.

## Building

Building mvoCI needs these packages as a requirement:
* go -- the go compiler
* gcc -- a C compiler for cgo (binding C code into go)
* bash/coreutils/make -- for running the Makefile

Go mod takes care of dependencies. Just run the following for building mvoCI, and place it together with the necessary assets inside the `dist`-folder.

```
make dist
```

### Windows

mvoCI works under Windows, but you will need a Bash and a view gnu-utils. I've tested it with software installed via chocolatey.

```
choco install git mingw32 go gnuwin32-utils.install
```

Then you can build and run mvoCI with `mingw32-make.exe` from the git-bash. I've had no luck with the `make`-package, as it was unable to find the mingw32 compilers.

```
mingw32-make.exe dist
```

When the compiler cannot be found, go defaults to `CGO_ENABLE=0`, which will break at least the SQLite-driver. If you get an error of that sort while installing or if mvoCI crashes on start in Database code, this might be the reason.

I suspect the git-bash to include gnu-tools. More work would be necessary to make mvoCI work on normal cmd.exe or PowerShell Shells. The build-scripts are still bash, so you'd have to have a bash shell anyways or patch the code before building.

## Requirements

### Database

You need a database server, or use SQLite as database backend. You may use one of the following:
* PostgreSQL
* MySQL / MariaDB
* SQLServer
* SQLite3

## Set up

* ``./mvo --install`` for the installation dialogues
* ``./mvo`` for production mode

## Configuration and Usage

### mvo.cfg

Config-file in JSON-format. See `core/config.go`.

### Emails

mvoCI can send out emails on finished builds. For this it needs swaks.

### Lock down

Do not execute mvoCI as root. It should be used with as little permissions as possible, but enough to be useful for your usecase. Most builds execute untrusted code like automake scripts, Makefiles or the like - make sure, that this untrusted code cannot leak information from your system.

### systemd

You may use this script to start mvoCI automatically as an own user (e.g. ``mvo``). Please make sure, that that user cannot do anything you wouldn't do yourself!

```
[Unit]
Description=mvoCI
After=syslog.target
After=network.target
After=mysqld.service
#After=postgresql.service

[Service]
RestartSec=2s
Type=simple
User=mvo
Group=mvo
WorkingDirectory=/home/mvo
ExecStart=/home/mvo/mvo
Restart=always
Environment=USER=mvo HOME=/home/mvo

[Install]
WantedBy=multi-user.target
```

## Contribute

Feel free to contribute to mvoCI if there are features missing for your use case. Either send me write-up for your use case, then I'll evaluate if it is inline with my goals of mvoCI. Feel also free to send pull-requests or patches my way.

If you want to start, try adding new routes in `web/routing.go`, then fill the new route with content and a template in `views/`. You can also have a look in the REST-API (`web/api.go`) code and add new functions there, which might be useful or add new hook-code for other git-version control systems (`hooks/`).

### Project Structure

* `main.go` - start point, reads config, starts builders, hands control to echo
* `core/` - database, configuration, logging. Fundamental stuff.
* `build/` - build workers and starting of builds from the web-side.
* `web/` - routing "table" for echo, handlers for requests, most of them called by echo
* `hook/` - webhook-request definitions for parsing the requests more or less generically (see `web/hook.go`)
* `auth/` - authentication code, like normal password check, TOTP module.
* `testing/` - code for parsing test reports like JUnit
